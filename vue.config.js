module.exports = {
  devServer:{
    proxy: 'http://localhost/contracts/api/'

  },
  
  transpileDependencies: [
    'vuetify',
    'vuex-persist'
  ],
  publicPath: process.env.NODE_ENV === 'production'
    ? '/'
    : '/',
}
