<?php
class OrganisationImagesController extends ImagesController{
	
	/*
	 * @method downloadAction()
	 * @desc download image content from database create a default image if none found
	 * @return $blob string field content echoed on browser as image
	 */
	public function downloadAction()
	{
		$organisation = Organisation::getInstance();
		$data=$this->model->filterOne([$organisation->primaryKey() => $this->_id, 'active'=>'Y']);		
		if($data)
		{
			$content=$data['content'];			
			$type=$data['mime_type'];
			$name=$data['file_name'];			
		}
		else
		{
			$content=file_get_contents(MAIN_ROOT.'/resources/img/logo-default.png');
			$type="image/png";
			$name="Image File";			
		}
		$this->download($content, $name, $type,false);
	}
	
	/*
	 * @method saveAvatarAction()
	 * @desc saves an icon/image received from post request
	 * @desc on successful save/update, the current image for a organisation id is set as the active
	 * @desc while other images for this organisation id are inactivated
	 * @return array $data which contains success and failure flags, last updated id and messages
	 */
	public function uploadAction()
	{
		if($this->hasPostedFiles())
		{
			$image = $this->model;
			$parent = Organisation::getInstance();
			$download_url = '/organisation-images/download/';
			$this->upload($image, $parent, $download_url);
		}
	}
}