<?php
class AccessLogsController extends CustomController
{
	private $colors = ['#84ba5b','#d35e60','#808585','#9067a7','#ab6857','#ccc210','#396ab1','#e1974c','#ff1a75','#804000'];
	protected $search_columns = ['resource','ip_address','host','browser','platform'];
	
	public function setTimeSpan($column)
	{
		//get proper time range to select records from
		$this->period_span = $this->getParams('period_span','D');
		$this->times = $this->getTimestampRange($this->period_span);
		$this->ts_begin = $this->times['ts_begin'];
		$this->ts_end = $this->times['ts_end'];
		//get proper time range to select records from
	
		return [new GreaterThanOrEqualTo($column,$this->ts_begin,'d1'),new LessThanOrEqualTo($column,$this->ts_end,'d2')];
	}
	
	public function filtersAction()
	{
		$data['browsers'] = $this->model->select([],[$this->model->uniqueColumn('browser')]);
		$data['platforms'] = $this->model->select([],[$this->model->uniqueColumn('platform')]);
		$data['hosts'] = $this->model->select([],[$this->model->uniqueColumn('host')]);
		$data['accounts'] = $this->model->select([],[$this->model->uniqueColumn('user_id')]);
		$this->data = $data;
	}
	
	public function usageAction()
	{
		$user_pk = User::getInstance()->primaryKey();
		$criteria = $this->setTimeSpan('audit_start');
		$columns = ['browser'=>'browser','platform'=>'platform','host'=>'host'];
		
		$data = [];
		foreach ($columns as $key=>$col)
		{
			$results = $this->model->select($criteria,[$this->model->countColumn($col,'value'),$col],$col);
			$chart = [];
			for($i=0;$i<count($results);$i++)
			{
				$row = $results[$i];
				$value = $row['value'];
				$label = $row[$col];
				$color = isset($this->colors[$i])? $this->colors[$i] : null;
				$chart[] = ['label'=>$label,'key'=>$label,'color'=>$color,'value'=>$value,'y'=>$value];
			}
			
			$data[$key] = ['chart'=>$chart];	
		}				
		
		$criteria = [new IsNotNull('session_token'),new NotEqual('suspended','N','suspended')];
		$data['account'] = User::getInstance()->select($criteria);;
		
		$this->data = $data;
	}
}