<?php
class PhoneMessagesController extends CustomController
{
	/**
	 * @api {get} /phone-messages/autocomplete autosearch existing processes
	 * @apiName autocomplete
	 * @apiParam {String} term Search criteria - sections of the process title or description
	 * @apiGroup PhoneMessages
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     		+254 789 090 900
	 *          +254 789 090 900
	 *     ]
	 */
	public function autocompleteAction(){
		//create some instances here and retrieve filter columns from post
		$search = '%'.$this->getParams('term').'%';
		$person = Person::getInstance();
		$organisation = Organisation::getInstance();
	
		//get persons sms
		$criteria = [new Like('phone',$search, 'phone1')];
		$person_sms = (array)$person->select($criteria,['phone'],null,null,' LIMIT 2 ');
	
		//get organisations sms
		$criteria = [new Like('phone',$search, 'phone')];
		$organisation_sms = (array)$organisation->select($criteria,['phone'],null,null,' LIMIT 2 ');
	
		//get recipients sms - in sent folders
		$criteria = [new Like('recipient',$search, 'phone')];
		$recipients = (array)$this->model->select($criteria,['DISTINCT(recipient) AS phone'],null,null,' LIMIT 5 ');
	
		//merge the two sms
		$sms = array_merge($person_sms,$organisation_sms,$recipients);
		$data = [];
		$unique = [];
		foreach ($sms as $e)
		{
			if(in_array($e['phone'],$unique)==false && strlen($e['phone'])>9){
				$this->model->setRecipient($e['phone']);
				$e['phone'] = $this->model->parsePhone();
				
				$data[] = $e;
				$unique[] = $e['phone'];
			}
		}
		$this->data = ['data' => $data];
	}
	
	public function pendingAction()
	{
		$setting = Setting::getInstance();
		
		$expiry_ts = intval($setting->fetchColumn('value',['code'=>'EXT']))*24*60*60;
		
		$box_type = MailBoxe::getInstance();
		
		$box_pk = $box_type->primaryKey();
		
		$criteria = [
				[$box_pk => $box_type->fetchColumn($box_pk,['code'=>'IB'])],
				new GreaterThanOrEqualTo('created_at',time()-$expiry_ts,'created_ata'),
				new LessThanOrEqualTo('created_at + '.$expiry_ts,time(),'created_atb')
		];
		
		$data = $this->model->select($criteria,null,null,$this->model->primarykey().' DESC');		
		$this->data = $data? $data : [];
	}
		
	private function setSmsOnPost()
	{
		$this->model->setRecipient($this->getPost('recipient'));
	}
	
	/**
	 * ---------------------------------- SEND PHONE SMS  --------------------------------
	 * @api {post} /phone-messages/send/ save phone short message (sms) as draft
	 * @apiName savePhoneSms
	 * @apiGroup PhoneMessages
	 *
	 * @apiParam {String} recipient List of One or Semi-colon Separated Phone Numbers
	 * @apiParam {String} message Message body usually 250 chars
	 * @apiParamExample {JSON}
	 *  {
	 *    "recipient": "+254667899900;+254987890900",
	 *    "message" : "This is a sample sms. Please dont reply"
	 *  }
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "status" : "info",
	 *       "message" : "Record succesfully saved"
	 *       "title" : "Record Operation"
	 *     }
	 */
	public function saveAction()
	{
		$box_type = MailBoxe::getInstance();
		
		$data[$box_type->primaryKey()] = $box_type->fetchColumn($box_type->primaryKey(),['code'=>'DF']);		
		
		$this->setSmsOnPost();
		$this->model->updateQueue(null,$this->getPost('message'),$data[$box_type->primaryKey()]);
	
		if($this->model->recordsAffected()>0)
		{
			//save to our database
			$this->model->update($data,[$this->model->primaryKey()=>$this->model->lastAffectedId()]);
			$data = ['message' => MESSAGE_SAVE_SUCCESS.' '.$this->getPost('recipient'),'status'=>'success','success'=>1];
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_SAVE_FAILURE;
			$data = ['message' => $message,'status'=>'error','success'=>0];
		}
		$this->data = array_merge($data,['title'=>TITLE_RECORD_OPERATION]);
	}
	
	
	/**
	 * ---------------------------------- SEND PHONE SMS  --------------------------------
	 * @api {post} /phone-messages/send/ send phone short message (sms)
	 * @apiName sendSms
	 * @apiGroup PhoneMessages
	 *
	 * @apiParam {String} recipient List of One or Semi-colon Separated Phone Numbers
	 * @apiParam {String} message Message body usually 250 chars
	 * @apiParamExample {JSON}
	 *  {
	 *    "recipient": "+254667899900;+254987890900",	 
	 *    "message" : "This is a sample sms. Please dont reply"
	 *  }
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "status" : "info",
	 *       "message" : "Sms succesfully sent to +254 733 808 099"
	 *       "title" : "Record Operation"
	 *     }
	 */
	public function sendAction()
	{		
		$this->setSmsOnPost();		
		$response = $this->model->sendSMS($this->getPost('message'),true);			
		
		if($response==true)
		{	
			$message = str_replace('Email','Sms',MESSAGE_SEND_SUCCESS).' '.$this->getPost('recipient');
			$data = ['message' => $message ,'status'=>'success','success'=>1];
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : str_replace('Email','Sms',MESSAGE_SEND_FAILURE);
			$data = ['message' => $message,'status'=>'error','success'=>0];
		}
		$this->data = array_merge($data,['title'=>TITLE_SMS_SEND]);
	}
	
	/**
	 * ---------------------------------- SEND PHONE SMS  --------------------------------
	 * @api {post} /phone-messages/send-selected/ send several queued sms
	 * @apiName sendSelectedSms
	 * @apiGroup PhoneMessages
	 *
	 * @apiParam {Array|Number} List of [sms id] Values For Queued Sms
	 * @apiParamExample {Array}
	 *  [
	 *  	4,5,6,7,8
	 *  ]
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "status" : "info",
	 *       "message" : "Sms succesfully sent to +254 733 808 099,+254 733 808 000"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
	public function sendSelectedAction()
	{
		$box_type = MailBoxe::getInstance();		
		$sms_pk = $this->model->primaryKey();
		$sms_ids = $this->getPost($sms_pk);
		$data = $this->model->select([$sms_pk=>$sms_ids]);
		$box_id = $box_type->fetchColumn($box_type->primaryKey(),['code'=>'ST']);
		$box_pk = $box_type->primaryKey();
		
		$count = 0;
		foreach ($data as $e)
		{
			$this->model->setRecipient($e['recipient']);			
			$response = $this->model->sendSms($e['message'],false);			
			$count = $count + intval($response);	

			if($response==true)
			{
				$this->model->update([$box_pk=>$box_id],[$sms_pk=>$e[$sms_pk]]);
			}
		}
		
		if($count>0)
		{
			$message = str_replace('Email','Sms',MESSAGE_SEND_SUCCESS).$count.' recipients';
			$status = STATUS_SUCCESS;
			$success = 1;
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : str_replace('Email','Sms',MESSAGE_SEND_FAILURE);
			$status = STATUS_ERROR;
			$success = 0;
		}
		
		$this->data = ['message'=>$message,'status'=>$status,'title'=>TITLE_SMS_SEND,$success=>$success];
	}
	
	/**
	 * --------------------- DELETE PHONE SMS ----------------------------
	 * @api {post} /phone-messages/delete/{id} delete existing
	 * @apiName deletePhoneSms
	 * @apiGroup PhoneMessages
	 *
	 * @apiParam {Integer}  id Unique Id Of A Sms
	 *
	 * @apiSuccess {Integer} count No. Of Records Successfully Deleted
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {Integer} id Unique ID Of The Just Deleted Record
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Record deleted successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
}