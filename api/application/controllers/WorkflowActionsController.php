<?php
class WorkflowActionsController extends CustomController
{
	/**
	 * --------------------- LIST POSSIBLE ACTIONS ----------------------------
	 * @api {get} /workflow-actions/list list actions
	 * @apiName ListActions
	 * @apiGroup WorkflowActions
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     	{
	 *     		action_id : 1,
	 *     		action_title: "Reject" //Action Name
	 *     		code: "REJ" //Short Code For Action
	 *     		action_effect: "Rejected" //Result Of Performing This Action
	 *     		color_hexa: "#000" //Hexadecimal Color Code For This Action
	 *     	},
	 *      ...
	 *     ]
	 */
}