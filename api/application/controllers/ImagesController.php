<?php
class ImagesController extends CustomController
{
	protected  $image_sizes = [
			'banner'=>['height'=>600,'width'=>1000],			
			'large'=>['height'=>300,'width'=>300],
			'medium'=>['height'=>200,'width'=>200],
			'thumb'=>['height'=>120,'width'=>120],
			'small'=>['height'=>80,'width'=>80],
	];
	
	public function download($content,$name,$type,$resize=true,$aspect=false)
	{	
		//set memory limits here		
		$this->model->setMemoryLimit($content,false);
		
		/* obtain dimensions here */
		$dimension = $this->getParams('dimension',null);
		
		/*
		 * check image then resize accordingly
		 * else don't resize just display as required
		 */
		if($resize==true && $dimension)
		{
			/* retrieve image sizes */
			$size = $this->image_sizes[$dimension];
			$height = $size['height'];
			$width = $size['width'];
			
			/*create image from bob string and based on supplied height and width*/
			$im = imagecreatefromstring($content);
			$x = imagesx($im);
			$y = imagesy($im);
				
			/*check if aspect ratio allowed and set destination width accordingly*/
			$ratio = ($aspect==true)? ($x/$y) : 1;
			$width = $ratio*$width;
				
			/*create a new true color image based on the new height and width*/
			$new = imagecreatetruecolor($width, $height);
				
			//used for alpha blending
			imagealphablending($new, false);
			imagesavealpha($new,true);
			$transparent = imagecolorallocatealpha($new, 255, 255, 255, 127);
			imagefilledrectangle($new, 0, 0, $width, $height, $transparent);
			//used for alpha blending
								
			imagecopyresampled($new, $im, 0, 0, 0, 0, $width, $height, $x, $y);
			imagedestroy($im);
			
			header('Content-type:'.$type);		
				
			/*check mime type before echoing*/
			switch ($type){
				case image_type_to_mime_type(IMAGETYPE_JPEG): imagejpeg($new,null,75); break;
				case image_type_to_mime_type(IMAGETYPE_PNG): imagepng($new,null,8); break;
				case image_type_to_mime_type(IMAGETYPE_GIF): imagegif($new,null); break;
				case image_type_to_mime_type(IMAGETYPE_BMP): imagewbmp($new,null); break;
			}
			
		}
		else{ 
			$size=strlen($content);			
			header("Content-length: $size");
			header("Content-type: $type");
			header("Content-Disposition: inline; filename=$name");
			echo $content;
		}
		exit();
	}
	
	public function upload($image,$parent,$download_url)
	{
		if($this->hasPostedFiles())
		{
			//retrieve posted image detail
			$post = $this->getPost();
			$selection = explode(',',$post['selection']);
			$this->_id = $post[$parent->primaryKey()];
			$file = $this->getPostedFiles('file');
			$tmp_path = $file['tmp_name'];
			$extension = strtolower(substr($file['name'],strrpos($file['name'], '.')));
			move_uploaded_file($tmp_path, $post['name'].$extension);
			$tmp_path = $post['name'].$extension;
		
			/* resize image here */
			$resized = $image->resizeImage($file, $selection, $tmp_path);
			$file = $resized['file'];
			$content = $resized['content'];
		
			//assemble record details to save
			$data=[
					'content' => $content,
					'file_name' => $post['name'],
					'file_size' => $file['size'],
					$parent->primaryKey() => $this->_id,
					'mime_type' => $file['type'],
					'effective_from' => time()
			];
		
			//start transaction
			$image->startTransaction();
		
			//update existing flags
			$image->update(['effective_to'=>time(),'active'=>'N'],[$parent->primaryKey() => $this->_id]);
		
			//save image information
			$image->save($data);
		
			//end transaction;
			$image->endTransaction();
		
			if($image->isError()==true)
			{
				$this->data = ['message'=>MESSAGE_IMAGE_UPLOAD_FAILURE.$image->message(),'status'=>STATUS_ERROR,'success'=>0];
			}
			else
			{
				$icon = API_ROOT.$download_url.$this->_id.'t=?'.time();
				$data = ['icon'=>$icon];
				$this->data = ['data'=>$data,'message'=>MESSAGE_IMAGE_UPLOAD_SUCCESS,'status'=>STATUS_SUCCESS,'success'=>1];
			}
		}
		else
		{
			$this->data = ['message'=>MESSAGE_IMAGE_UPLOAD_FAILURE,'status'=>STATUS_ERROR,'success'=>0];
		}
		$this->data['title'] = TITLE_UPLOAD_OPERATION;
		
	}
}