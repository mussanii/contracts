<?php
class WorkflowProcessStepsController extends CustomController
{	
	
	/**
	 * --------------------- DELETE STEP ----------------------------
	 * @api {get} /workflow-process-steps/delete/{id} delete existing step
	 * @apiName DeleteStep
	 * @apiGroup WorkflowStep
	 *
	 * @apiParam {Integer}  id Unique Id Of A Step
	 *
	 * @apiSuccess {Integer} count No. Of Records Successfully Deleted
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message For The Record Operation
	 * @apiSuccess {Integer} id Unique ID Of The Just Deleted Step
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Record deleted successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
}