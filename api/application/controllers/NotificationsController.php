<?php
class NotificationsController extends CustomController
{
	public function pendingAction()
	{
		$setting = Setting::getInstance();
		
		$expiry_ts = intval($setting->fetchColumn('value',['code'=>'NET']))*24*60*60;
		
		$criteria = [
				new GreaterThanOrEqualTo('created_at',time()-$expiry_ts,'expires_ata'),
				new GreaterThanOrEqualTo('expires_at',time(),'expires_atb')
		];
		
		$data = $this->model->select($criteria,null,null,$this->model->primarykey().' DESC');
		$this->data = $data? $data : [];
	}
	
	/**
	 * ---------------------------------- PUSH NOTIFICATION  --------------------------------
	 * @api {post} /notifications/push/ push a notification to a mobile device.
	 * @apiName pushNotification
	 * @apiGroup Notifications
	 *
	 * @apiParam {recipient} an optional semi colon separated list of recipients bearing person's id and name
	 * @apiParam {criteria} an optional array of person_ids/email_ids/phones etc used to select user devices
	 * @apiParam {String} message Message body 
	 * @apiParam {String} title Message title
	 * 
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "status" : "info",
	 *       "message" : "Notifications successfully sent"
	 *       "title" : "Push Notification"
	 *     }
	 */
	public function pushAction()
	{
		/* obtain requisite instances and primary keys */
		$user_role = UserRole::getInstance();	
		$user = User::getInstance();	
		
		$person_pk = Person::getInstance()->primaryKey();
		$role_pk = Role::getInstance()->primaryKey();
		$user_pk = $user->primaryKey();
		
		/* extract person id from 'recipient' key or 'person_id' key */
		$person_id = $this->model->extractRecipient($this->getPost('recipient'),true);
		$person_id = $this->getPost($person_pk)? $this->getPost($person_pk) : $person_id;
		
		/* extract person ids for any roles that have been selected */
		if($this->getPost($role_pk))
		{
			$user_id = $user_role->fetchColumn($user_pk,[$role_pk=>$this->getPost($role_pk),'active'=>'Y'],true);
			$person_id = array_merge($person_id,$user->fetchColumn($person_pk,[$user_pk => $user_id],true));
		}		
		
		/* assemble criteria */
		$recipient = PersonDevice::getInstance()->fetchColumn('device_sno',[$person_pk=>$person_id],true);
		$criteria = (array)$this->query_array;	
		$criteria = count($criteria)>0? $criteria : array_merge($criteria,[$person_pk=>$person_id]);			
		
		/* assemble data */
		$data = ['message' => $this->getPost('message'),'title' => $this->getPost('title'),'recipient'=>implode(';',$recipient)];
		
		$rp = 'Push Notification';
		$response = $this->model->pushNotification($criteria,$data);
		
		if(isset($response['success']) && $response['success']>0)
		{
			if($this->hasPostedFiles())
			{
				$this->model->saveAttachments($response['id'],$this->getPostedFiles(),$this->getPost('mime_type'),$this->getPost('file_type'));
			}
			$message = str_replace('Email',$rp,MESSAGE_SEND_SUCCESS).' to device(s)';
			$data = ['message' => $message ,'status'=>'success','success'=>1];
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : str_replace('Email',$rp,MESSAGE_SEND_FAILURE);
			$data = ['message' => $message,'status'=>'error','success'=>0];
		}
		$this->data = array_merge($data,['title'=>str_replace('Email',$rp,TITLE_EMAIL_SEND)]);
	}
	
	/**
	 * ---------------------------------- SEND MULTIPLE NOTIFICATIONS  --------------------------------
	 * @api {post} /notifications/push-selected/ send several queued notifications
	 * @apiName sendSelectedNotifications
	 * @apiGroup Notifications
	 *
	 * @apiParam {Array|Number} List of [notification id] Values For Queued notifications
	 * @apiParamExample {Array}
	 *  [
	 *  	4,5,6,7,8
	 *  ]
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "status" : "info",
	 *       "message" : "Notifications succesfully sent to Leiney,James & Kereka"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
	public function pushSelectedAction()
	{
		$box_type = MailBoxe::getInstance();
		$notification_pk = $this->model->primaryKey();
		$notification_ids = $this->getPost($notification_pk);
		$data = $this->model->select([$notification_pk=>$notification_ids]);
		$box_id = $box_type->fetchColumn($box_type->primaryKey(),['code'=>'ST']);
		$box_pk = $box_type->primaryKey();
		$rp = 'Push Notification';
	
		$count = 0;
		foreach ($data as $e)
		{
			$criteria = ['device_sno'=>explode(';', $e['recipient'])]; 
			$message = ['message'=>$e['message'],'title'=>$e['title']];
			$response = $this->model->pushNotification($criteria,$message,false);
			$count = $count + ((isset($response['success']) && $response['success']>0)? 1 : 0);
			if($response==true)
			{
				$this->model->update([$box_pk=>$box_id],[$notification_pk=>$e[$notification_pk]]);
			}
		}
	
		if($count>0)
		{
			$message = MESSAGE_SEND_SUCCESS.$count.' recipients';
			$status = STATUS_SUCCESS;
			$success = 1;
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_SEND_FAILURE;
			$status = STATUS_ERROR;
			$success = 0;
		}
		
		$message = str_replace('Email',$rp,$message);
		$title = str_replace('Sms',$rp,TITLE_SMS_SEND);
		$this->data = ['message'=>$message,'status'=>$status,'title'=>$title,$success=>$success];
	}
	
	
	/**
	 * @api {get} /notifications/register register a mobile device 
	 * @apiName registerDevice
	 * @apiParam {String}  platform os platform supporting push notifications i.e. ANDROID/IOS/WINDOWS
	 * @apiParam {String} device_sno unique device no i.e. IMEI or Serial No
	 * @apiParam {String} push_device_id unique device id as recorded in the mobile/tablet etc
	 * @apiParam {String} description optional description about device e.g. name,manufacturer etc
	 * @apiGroup Notifications
	 * 
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "status" : "info",
	 *       "message" : "Device successfully registered"
	 *       "title" : "Device Registration"
	 *     }
	 */
	public function registerAction()
	{	
		$this->model->registerPushDevice($this->getPost(),$this->identity->getPersonId());
		
		if($this->model->recordsAffected()>0)
		{
			$data = ['message' => MESSAGE_DEVICE_REGISTER_SUCCESS,'status'=>'success','success'=>1];
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_DEVICE_REGISTER_FAILURE;
			$data = ['message' => $message,'status'=>'error','success'=>0];
		}
		$this->data = array_merge($data,['title'=>TITLE_RECORD_OPERATION]);
	}
}