<?php
class WorkflowCommentsController extends CustomController
{
	/**
	 * --------------------- LIST POSSIBLE commentS ----------------------------
	 * @api {get} /workflow-Comments/list list Comments
	 * @apiName ListComments
	 * @apiGroup WorkflowComment
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     	{
	 *     		comment_id : 1,
	 *     		action_id : 2,
	 *          step_id : 2,
	 *          comments: 'Approved for next step',
	 *     		code: "REJ" //Short Code For Comment
	 *     		color_hexa: "#000" //Hexadecimal Color Code For This Comment
	 *     	},
	 *      ...
	 *     ]
	 */
}