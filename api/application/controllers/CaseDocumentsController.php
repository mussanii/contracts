<?php
class CaseDocumentsController extends DocumentsController
{
	public function __construct($url_array,$query_array)
	{
		parent::__construct($url_array,$query_array);
		$this->fk_table = CaseLog::getInstance();
	}

	/**
	 * --------------------- UPLOAD CASES DOCUMENT ----------------------------
	 * @api {post} /case-documents/upload upload a document at a step
	 * @apiName UploadDocument
	 * @apiGroup CaseDocument
	 *
	 * @apiParam {Integer}  id Unique Id For A Comment
	 * @apiParam {Object} file Name Of Uploaded File Object.
	 *
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "answer" : "File transfer completed"
	 *     }
	 *
	 * @apiErrorExample {Json} Success-Response:
	 *     HTTP/1.1 400 Error
	 *     "No Files"
	 */
	public function uploadAction()
	{
		
	    parent::uploadAction();
	    $count = $this->model->recordsAffected();
	    if($count>0)
    	{
    	    $this->getSaveResponse($count,'File upload completed with success');
    	}
    	else
    	{
    	    $this->data = ['message' => 'No file(s) uploaded. Try again.','success'=>0,'status'=>STATUS_ERROR];
    	}
    	$this->data['title'] = TITLE_UPLOAD_OPERATION; 
	}
	
	/**
	 * --------------------- DOWNLOAD/VIEW CASE DOCUMENT -------------
	 * @api {get} /case-documents/download/{id} download a document
	 * @apiName DownloadDocument
	 * @apiGroup CaseDocument
	 *
	 * @apiParam {Integer}  id Unique Id Of A document
	 *
	 */	
	
	
	/**
	 * --------------------- DELETE DOCUMENT ----------------------------
	 * @api {get} /case-documents/delete/{id} delete a document
	 * @apiName Deletedocument
	 * @apiGroup ContractDocument
	 *
	 * @apiParam {Integer}  id Unique Id Of A document
	 *
	 * @apiSuccess {Integer} count No. Of Records Successfully Deleted
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message For The Record Operation
	 * @apiSuccess {Integer} id Unique ID Of The Just Deleted Record
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Record deleted successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
}