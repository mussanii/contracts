<?php
class WorkflowProcessesController extends CustomController
{
	protected $sanitize_data=false;
	
	/**
	 * ---------------------------------- CREATE NEW WORKFLOW --------------------------------
	 * @api {post} /workflow-processes/save/ create a new process
	 * @apiName CreateProcess
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {String} process_title A Summary About This Workflow
	 * @apiParam {Number} organisation_id (Optional) Unique Id Of Organisation That Owns This Process
	 * @apiParam {String} process_description  A Detailed Description About This Workflow
	 * @apiParam {Array} steps A List Of Json Objects Containing Information About Each Step
	 * @apiParamExample {Array} steps array 
	 *     [
	 *     	{
	 *       "step_no": 1,	 
	 *       "step_url" : "http://worflows.com/list",   //path to html that contains view page
	 *       "step_name":"Set Profile Detail",
	 *       "role_id" : 2,
	 *       "min_approvers" : "3",
	 *       "is_mandatory" : "Y",   //determine if current step must be complete to proceed next
	 *       "due_date" : "24556320028"  //time stamp
	 *     	},
	 *     ...
	 *    ] 
	 * @apiSuccess {Number}  id  Record ID Of The Just Created Process
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Workflow created successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
	
	/**
	 * ------------------------------- UPDATE WORKFLOW DETAIL ---------------------------------
	 * @api {post} /workflow-processes/save/ update process detail
	 * @apiName UpdateProcess
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {Integer} process_id Process unique ID
	 * @apiParam {String} process_title A Summary About This Workflow
	 * @apiParam {String} process_description  A Detailed Description About This Workflow
	 * @apiParam {Array} steps A List Of Json Objects Containing Information About Each Step
	 * @apiParamExample {Array} steps array
	 *     [
	 *     	{
	 *       "step_no": 1,
	 *       "step_url" : "http://worflows.com/list",   //path to html that contains view page
	 *       "step_name":"Set Profile Detail",
	 *       "role_id" : 2,
	 *       "min_approvers" : "3",
	 *       "is_mandatory" : "Y",   //determine if a step is required to be complete to proceed next
	 *       "due_date" : "24556320028"  //time stamp
	 *     	},
	 *     ...
	 *    ]
	 * @apiSuccess {Number}  id  Unique ID Of The Just Updated Process
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Workflow updated successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
	
	
	
	/**
	 * --------------------- LIST WORKFLOWS ----------------------------
	 * @api {get} /workflow-processes/list list existing processes
	 * @apiName ListProcesses
	 * @apiGroup WorkflowProcess
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [	
	 *     	{
	 *     		process_id : 1,
	 *     		process_title: 'User Account Setup For Kimwanda Kennedy',
	 *     		status_id: 1,
	 *     		id_parent: null, //Id of process where this workflow was cloned(null means it is an original copy)
	 *     		submitted_on: 3673783839, //timestamp
	 *     		status: { "status_id": 1,"status_title": "Not Started","code": "NST" },
	 *     		steps : [
	 *     		 {
	 *      			"step_no": 1,
	 *      			"step_url" : "http://worflows.com/list", //path html view page
	 *       			"step_name":"Set Profile Detail",
	 *       			"role_id" : 2,
	 *       			"min_approvers" : "3",
	 *       			"is_mandatory" : "Y",   //determine if a step is required to be complete to proceed next
	 *       			"due_date" : "24556320028"  //time stamp
	 *     		 }
	 *     		 ...
	 *     		]
	 *     	},
	 *     	...
	 *     ]
	 *     
	 */
	public function listAction()
	{
		$criteria = $this->query_array;
		$type = WorkflowProcessType::getInstance();
		$type_key = $type->primaryKey();
		$code = ['CM','ML','OB','TS'];
		$type_id = (array)$this->getParams($type_key,$type->fetchColumn($type_key,[new Equal('code',$code, 'code')]));
		
		if(array_key_exists('id_parent', $this->query_array)){
			$id_parent = $this->getParams('id_parent');
			$criteria[] = $id_parent? new Equal('id_parent',$id_parent,'k1') : new IsNull('id_parent');
			unset($criteria['id_parent']);
		}	
		
		if ($this->after > 0) {
			$criteria [] = new LessThan ( $this->model->primaryKey (), $this->after, $this->model->primaryKey () );
		}
		if($type_id){
			$criteria [] = new NotEqual($type_key,$type_id,$type_key);
		}
		
		$data = $this->model->select ($criteria, null, null, null, $this->limit);
		$count = intval($this->model->count($criteria, null, null, null, null));
		$this->data = ($this->infinite!='Y')? $data : ['count'=>$count,'items'=>$data];	
	}
	
	
	/**
	 * --------------------- DELETE WORKFLOW ----------------------------
	 * @api {get} /workflow-processes/delete/{id} delete existing process
	 * @apiName DeleteProcess
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {Integer}  id Unique Id Of A Process
	 *
	 * @apiSuccess {Integer} count No. Of Records Successfully Deleted
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {Integer} id Unique ID Of The Just Deleted Record
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Record deleted successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
	/**
	 * --------------------- CLONE EXISTING WORKFLOW ----------------------------
	 * @api {post} /workflow-processes/clone/{id} clone existing process
	 * @apiName CloneProcess	
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {Integer}  id Process Id Of Existing Process (To Be Used As Clone Source)
	 * @apiParam {Number} organisation_id (Optional) Unique Id Of Organisation That Owns This Process
	 * @apiParam {String} process_title (Optional) Preferred Short Name For The Clone
	 * @apiParam {String} process_description  (Optional) A Detailed Description About The Clone
	 *
	 * @apiSuccess {Number}  id  Unique ID Of The Clone
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {Object}  data  Details About The Just Created Clone (If Any)
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "data" : {
	 *     		process_id : 1,
	 *     		process_title: 'User Account Setup For Kimwanda Kennedy',
	 *     		status_id: 1,
	 *     		id_parent: null, //Id of process where this workflow was cloned(null means it is an original copy)
	 *     		submitted_on: 3673783839, //timestamp
	 *     		status: { "status_id": 1,"status_title": "Not Started","code": "NST" },
	 *     		steps : [
	 *     		 {
	 *      			"step_no": 1,
	 *      			"step_url" : "http://worflows.com/list", //path html view page
	 *       			"step_name":"Set Profile Detail",
	 *       			"role_id" : 2,
	 *       			"min_approvers" : "3",
	 *       			"is_mandatory" : "Y",   //determine if a step is required to be complete to proceed next
	 *       			"due_date" : "24556320028"  //time stamp
	 *     		 }
	 *     		 ...
	 *     		]
	 *     	 },
	 *       "status" : "info",
	 *       "message" : "Workflow created successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */	
	public function cloneAction()
	{
		$clone = $this->model->cloneProcess($this->_id);
		$this->addPost($clone); 
		$this->saveAction();
		$id = isset($this->data['id'])? $this->data['id'] : null;
		$this->data['data'] = $this->model->selectOne($id);		
	}
	
	/**	 
	 * @api {get} /workflow-processes/next/{id} move to next step
	 * @apiName ProgressNext
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {Integer}  id Unique ID of An Existing Process
	 * @apiParam {Char}  send_email A Flag  (Optional) (Y/N) That Indicates If An Email Should Be Sent
	 * @apiParam {Char}  send_sms A Flag  (Optional) (Y/N) That Indicates If An SMS Should Be Sent
	 * @apiParam {Char}  push_notification (Optional) A Flag (Y/N) That Indicates If Notification Should Be Sent
	 * 
	 * @apiSuccess {Number}  id  Process Id
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	*/
	public function nextAction()
	{
		$count = $this->model->move($this->_id,true);
		$this->getSaveResponse($count);
	}
	
	/**
	 * @api {get} /workflow-processes/previous/{id} move to previous step
	 * @apiName ProgressPrevious
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {Integer}  id Unique ID of An Existing Process
	 * @apiParam {Char}  send_email A Flag  (Optional) (Y/N) That Indicates If An Email Should Be Sent
	 * @apiParam {Char}  send_sms A Flag  (Optional) (Y/N) That Indicates If An SMS Should Be Sent
	 * @apiParam {Char}  push_notification (Optional) A Flag (Y/N) That Indicates If Notification Should Be Sent
	 *
	 * @apiSuccess {Number}  id  Process Id
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 */
	public function previousAction()
	{
		$count = $this->model->move($this->_id,false);
		$this->getSaveResponse($count);
	}
	
	/**
	 * @api {get} /workflow-processes/change-status/{id} change process status
	 * @apiName ProgressChangeStatus
	 * @apiGroup WorkflowProcess
	 *
	 * @apiParam {Integer}  id Unique ID of An Existing Process
	 * @apiParam {Integer}  status_id An Id Denoting Status N/B Check Available Statuses
	 * @apiParam {Char}  send_email A Flag  (Optional) (Y/N) That Indicates If An Email Should Be Sent
	 * @apiParam {Char}  send_sms A Flag  (Optional) (Y/N) That Indicates If An SMS Should Be Sent
	 * @apiParam {Char}  push_notification (Optional) A Flag (Y/N) That Indicates If Notification Should Be Sent
	 *
	 * @apiSuccess {Number}  id  Process Id
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 */
	public function changeStatusAction()
	{
		$status = WorkflowStatuse::getInstance();
		$status_id = $this->getPost($status->primaryKey());
		$_POST = [$this->model->primaryKey()=>$this->_id,$status->primaryKey()=>$status_id];
		$this->saveAction();
	}
	

	/**
	 * @api {get} /workflow-processes/autocomplete autosearch existing processes
	 * @apiName autocomplete
	 * @apiParam {String} term Search criteria - sections of the process title or description
	 * @apiGroup WorkflowProcess
	 */
	public function autocompleteAction(){
		$search = '%'.$this->getParams('term').'%';		
		$id_parent = intval($this->getParams('id_parent',0));		
		$criteria = [new Like('process_title',$search, 'process_title')];
		if($id_parent>0)
		{
			$criteria['id_parent'] = $id_parent;
		}		
		$columns = [$this->model->primaryKey(),'process_title'];
		$this->data = ['data' => $this->model->select($criteria,$columns,null,null,' LIMIT 5 ')];
	}
	

	/**
	 * @api {get} /workflow-processes/change-status/{id} copy details info from process
	 * @apiName Copify
	 * @apiGroup WorkflowProcess
	 */
	public function copifyAction(){
		$data = $this->model->cloneProcess($this->_id);
		$this->data['data'] = $data;
		$this->data['success'] = count($data)>0? 1 : 0;
	}
	
}