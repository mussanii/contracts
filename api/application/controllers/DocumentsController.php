<?php
use Dompdf\Dompdf;
class DocumentsController extends CustomController
{	
	protected $fk_table;
	
	public function uploadAction()
	{
		if($this->hasPostedFiles())
		{
			$document = $this->getPostedFiles('file');
			$content = file_get_contents($document[ 'tmp_name' ]);
			
			$data=[
					'content' => $content,
					'file_name' => $document['name'],
					'file_size' => $document['size'],
					$this->fk_table->primaryKey() => $this->getPost('id'),
					'file_type' => $this->getPost('file_type'),
					'mime_type' => $document['type'],
					'effective_from' => time()
			];
			// die(var_dump($document['size']));
			$this->model->save($data);
			$answer = ['answer' => 'document transfer completed'];
			$this->data = $answer;
		}
		else
		{
			$this->data = 'No files';
		}
	}
	
	public function downloadAction()
	{
		$data= $this->model->selectOne($this->_id);
		if($data)
		{
			$content=$data['content'];
			$type=$data['mime_type'];
			$name=$data['file_name'];
			$this->download($content, $name, $type);
		}
	}
	
	public function download($content,$name,$type)
	{	
		global $global;
		
		$size=strlen($content);
		
		header("Content-length: $size");
		header("Content-type: $type");
		header("Content-Disposition: inline; filename=$name");
				
		$global->sendResponse($content,false);
	}
	
	public function saveFile($content,$file_path)
	{
		$fp = fopen($file_path, 'w');
		fwrite($fp, $content);
		fclose($fp); 
	}
	
	/**
	 * @param $post variables from post
	 * @desc export html content to pdf
	 * @method exportAction()
	 */
	public function exportAction()
	{
		global $global;
		
		/*retrieve important content post variable and replace <title> with file name */
		$html = $this->getPost('content');
		$name = $this->getPost('file_name');
		$match = '/(<title>)(.*?)(<\/title>)/i';
		$content = preg_replace($match, '$1' . $name . '$3', $html);
				
		/*initialize dompd and set important options e.g. layout before going*/
		$dompdf = new Dompdf();
		$dompdf->set_option('isHtml5ParserEnabled', true);
		$dompdf->set_option('defaultFont', $this->getParams('font','Courier'));
		$dompdf->setPaper($this->getParams('page_size','A4'), $this->getParams('layout','portrait'));
		
		/* load html to $dompdf object , render and output pdf as blob string */
		$dompdf->loadHtml($content);		
		$dompdf->render(); 		
		$content = $dompdf->output();
		
		/* gather important table field values and alter $_POST content */
		$type = 'application/pdf';
		$file = ['content'=>$content,'file_size'=>strlen($content),'mime_type'=>$type];
		$_POST = array_merge($this->getPost(),$file);
		$json = ['status'=>STATUS_SUCCESS,'title'=>TITLE_UPLOAD_OPERATION,'message'=>MESSAGE_EXPORT_SUCCESS,'type'=>$type];
		
		/*Render document or save to file system or database as specified. Default[Database]*/
		switch ($this->getParams('method','blob')){
			case 'json': $this->renderJSON($json); break;
			case 'string': $global->sendResponse($content); break;
			case 'blob': $this->saveAction(); $this->data = $json; break;
			case 'file': $this->saveFile($content, $name.'.pdf');  $this->renderJSON($json); break;
			case 'browser': $this->download($content, $name, $type); break;
		}		
	}
}