<?php
class CaseLogsController extends CustomController
{
    protected  $sanitize_data = false;
    protected $search_columns = ['case_no','description','parties'];
    public function autocompleteAction(){
		$search = '%'.$this->getParams('term').'%';
		$criteria = [new Like($this->model->concat(['description','parties']),$search, 'description')];		
		$this->data = ['data' => $this->model->select($criteria,null,null,null,' LIMIT 5 ')];
	}
}