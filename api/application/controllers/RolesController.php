<?php
/**
 * @author Leiney M. Ogeto
 * @copyright Copyright 2017
 * @license Commercial
 * @version 1
 */

class RolesController extends Controller{
	
	private $actions;
	private $objects;
	private $o_types;
	
	/**
	 * ---------------------------------- CREATE NEW ROLES --------------------------------
	 * @api {post} /roles/save/ create a new roles
	 * @apiName CreateRoles
	 * @apiGroup Roles
	 *
	 *
	 * @apiParam {int{11}} role_id   Primary Key
	 * @apiParam {int{11}} [id_parent]   Index
	 * @apiParam {char{2}} [code]
	 * @apiParam {varchar{50}} role_name
	 * @apiParam {char{1}} permanent = N
	 * @apiParam {char{1}} active = Y
	 *
	 * @apiParamExample {json} Request-Example:
	 *     {
	 *
	 *     }
	 *
	 * @apiSuccess {Number}  id  Record ID Of The Just Created Roles
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *
	 *     }
	 */
	
	
	/**
	 * ------------------------------- UPDATE ROLES DETAIL ---------------------------------
	 * @api {post} /roles/save/ update roles detail
	 * @apiName UpdateRoles
	 * @apiGroup Roles
	 *
	 *
	 * @apiParam {int{11}} role_id   Primary Key
	 * @apiParam {int{11}} [id_parent]   Index
	 * @apiParam {char{2}} [code]
	 * @apiParam {varchar{50}} role_name
	 * @apiParam {char{1}} permanent = N
	 * @apiParam {char{1}} active = Y
	 *
	 * @apiParamExample {json} Request-Example:
	 *     {
	 *
	 *     }
	 * @apiSuccess {Number}  id  Unique ID Of The Just Updated Process
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Record Operation
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *
	 *     }
	 */
	
	
	
	
	/**
	 * --------------------- LIST ROLES ----------------------------
	 * @api {get} /roles/list list existing roles
	 * @apiName ListRoles
	 * @apiGroup Roles
	 *
	 * @apiParamExample {json} Request-Example:
	 *     {
	 *
	 *     }
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     {
	 *
	 *     }
	 *
	 */
	
	
	/**
	 * --------------------- DELETE ROLES ----------------------------
	 * @api {get} /roles/delete/{id} delete existing roles
	 * @apiName DeleteRoles
	 * @apiGroup Roles
	 *
	 * @apiParam {Integer}  id Unique Id Of A Roles
	 *
	 * @apiSuccess {Integer} count No. Of Records Successfully Deleted
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Record Operation
	 * @apiSuccess {Integer} id Unique ID Of The Just Deleted Record
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *
	 *     }
	 */
	
	
	/**
	 * --------------------- GET ROLE HIERARCHY AS TREE ----------------------------
	 * @api {get} roles/tree Get roles hierarchy as a tree
	 * @apiName RoleTree
	 * @apiGroup Roles
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     	{
	 *          role_id : 1,
	 *          role_name: 'admin',	 
	 *          ...
	 *          children: [
	 *             {
	 *                 role_id : 2,
	 *                 role_name: 'junior admin',	
	 *             }
	 *             ....
	 *          ]
	 *     	},
	 *     	...
	 *     ]
	 *
	 */
	public function treeAction()
	{
		$roles_criteria = ($this->getParams('all')=='Y')? [] : [new NotEqual('code', 'AM', 'code')];
		
		if($this->getParams('simple')=='Y')
		{
			$pk = $this->model->primaryKey();
			$columns = [$pk.' AS id', 'COALESCE(id_parent,0) AS parent', 'role_name AS text', $pk.' AS value'];
			$items = $this->model->select($roles_criteria,$columns);		
			foreach ($items as &$item)
			{
				$item['open'] = true;
			}	
			$items = Helpers::getPaths($items,'parent','id','text',' &#9679 ');
			$this->data = $this->getTree($items, 'parent','id','text');			
			return;
		}
		
		$object = Object::getInstance();
		$role_object = RoleObject::getInstance();
		$action = ObjectAction::getInstance();
		$role_permission = RolePermission::getInstance();
		$object_type = ObjectType::getInstance();
		
		$role_pk = $this->model->primarykey();
		$object_pk = $object->primaryKey();
		$action_pk = $action->primaryKey();
		$o_type_pk = $object_type->primaryKey();
		
		$criteria = [new NotEqual('code', 'ACT', 'code')];
		$this->objects = (array)$object->select($criteria);
		$this->actions = (array)$action->select();
		$this->o_types = (array)$object_type->select();
				
		$roles = (array)$this->model->select($roles_criteria); 
		
		for($r=0;$r<count($roles);$r++)
		{
			$rle = $roles[$r];
						
			//for each role inject object types
			$object_types = [];
			for ($k=0;$k<count($this->o_types);$k++)
			{
				$o_type = $this->o_types[$k];
				//for each object type retrieve objects				
				$objects = [];	
				for ($m=0;$m<count($this->objects);$m++)
				{
					$obj = $this->objects[$m];
					
					//if object type is same as one in outer loop
					if($o_type[$o_type_pk]==$obj[$o_type_pk])
					{
						$obj['id_parent'] = (int)$obj['id_parent'];
						$criteria = [$object_pk=>$obj[$object_pk],$role_pk=>$rle[$role_pk],'active'=>'Y'];
						
						$blank = $role_object->sanitize([$role_pk=>$rle[$role_pk],$object_pk=>$obj[$object_pk]],false);
						$rec = $role_object->filterOne($criteria);
						$obj = $rec? array_merge($rec,$obj) : array_merge($blank,$obj);
						
						//for each object retrieve permissions	
						$permissions = [];
						for ($n=0;$n<count($this->actions);$n++)
						{
							$prm = $this->actions[$n];
							$criteria = [$action_pk=>$prm[$action_pk],$role_pk=>$rle[$role_pk],'active'=>'Y'];
							$rpm = $role_permission->filterOne($criteria);
							if($obj[$object_pk]==$prm[$object_pk])
							{
								$blank = $role_permission->sanitize([$role_pk=>$rle[$role_pk],$action_pk=>$prm[$action_pk]],false);								
								$permissions[] = $rpm? array_merge($rpm,$prm) : array_merge($blank,$prm);
							}					
						}
						
						$obj['permissions'] = $permissions;
						$objects[] = $obj;
					}
				}
				
				$objects = Helpers::getPaths($objects,'id_parent',$object_pk,'title');
				$objects = Helpers::getTree($objects, 'id_parent',$object_pk,'title');
				
				//exclude dashboard items
				if($o_type['code']!='DB')
				{
					$o_type['objects'] = $objects;
					$object_types[] = $o_type;
				}							
			}
			
			$rle['object_types'] = $object_types;			
			$roles[$r] = $rle;
		}
		
		$this->data = $roles;
	}
		
	/**
	 * --------------------- ROLE USERS FOR A GIVEN ROLE CODE ----------------------------
	 * @api {get} roles/contacts Get user list for a role code
	 * @apiName RoleUsers
	 * @apiGroup Roles
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     	{
	 *          user_id : 12,
	 *          username: 'kimwa377393',
	 *     		person_id : 1,
	 *     		full_name: 'Kimwanda Kennedy',
	 *     		primary_email: 'leinsghsh@gama.com',
	 *          phone: '276232222029',
	 *          active: 'Y'
	 *          ...
	 *     	},
	 *     	...
	 *     ]
	 *
	 */
	public function usersAction()
	{
		$user = User::getInstance();
		$role = Role::getInstance();
		$user_role = UserRole::getInstance();
		
		$role_id = $role->fetchColumn($role->primaryKey(),['code'=>explode(',',$this->getParams('code'))]);
		$role_id = $role_id>0? $role_id : $this->_id;
		
		$user_id = $user_role->fetchColumn($user->primaryKey(),[$this->model->primaryKey()=>$role_id,'active'=>'Y']);
		$data = $user->select([$user->primaryKey()=>$user_id]);
		$this->data= $data? $data : [];
	}
	
	/**
	 * --------------------- PHONE AND EMAIL FOR ROLE USERS ----------------------------
	 * @api {get} roles/contacts Get phones & emails for role users
	 * @apiName RoleContacts
	 * @apiGroup Roles
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     	{
	 *     		person_id : 1,
	 *     		full_name: 'Kimwanda Kennedy',
	 *     		primary_email: 'leinsghsh@gama.com',
	 *          phone: '276232222029'
	 *     	},
	 *     	...
	 *     ]
	 *
	 */
	public function contactsAction()
	{
	    $user = User::getInstance();
	    $role = Role::getInstance();
	    $user_role = UserRole::getInstance();
	    $role_pk = $role->primaryKey();
	    
	    $columns = ['person_id','full_name','primary_email','phone'];
	    $role_id = $this->getPost($role_pk)? $this->getPost($role_pk) : $this->_id;
	    
	    $user_id = $user_role->fetchColumn($user->primaryKey(),[$role_pk=>$role_id,'active'=>'Y']);
	    $data = $user->select([$user->primaryKey()=>$user_id]);
	    $data = $user->arraySubset($data,$columns,true);
	    $this->data= $data? $data : [];
	}
	
}