<?php
class AnalyticsController extends ContractsController
{
	
	public function setTimeSpan($column)
	{
		//get proper time range to select records from
		$this->period_span = $this->getParams('period_span','D');
		$this->times = $this->getTimestampRange($this->period_span);
		$this->ts_begin = $this->times['ts_begin'];
		$this->ts_end = $this->times['ts_end'];
		//get proper time range to select records from
	
		return [new GreaterThanOrEqualTo($column,$this->ts_begin,'d1'),new LessThanOrEqualTo($column,$this->ts_end,'d2')];
	}
	
	/**
	 * @desc get quick stats for contracts based on status
	 */
	public function quickStatsAction()
	{
		$criteria = []; 
		$model = $this->model;
		$status = ContractStatuse::getInstance();		
		$pk = $model->primaryKey();
		$status_pk = $status->primaryKey();
		
		$data = [];
		
		//fetch contract statuses
		foreach ((array)$status->select() as $row)
		{			
			//count contracts in this status
			$code = strtolower($row['code']);
			$row['value'] = intval($model->count(array_merge($criteria,[$status_pk=>$row[$status_pk]])));
			$row['href'] = WEB_ROOT.'/#/contracts/list?'.$status_pk.'='.$row[$status_pk];
			$data[$code] = $row;			
		}
		
		$this->data = $data;
	}
	
	/**
	 * @desc get short descriptive pie graph data based on particular field
	 */
	public function pieStatsAction()
	{
		$criteria = []; 
		$analytic = $this->getParams('analytic');
		
		switch ($analytic)
		{
			case 'status' : $data = $this->model->simpleGraph(ContractStatuse::getInstance(),$criteria,false); break;
			case 'department' : $data = $this->model->simpleGraph(OrganisationUnit::getInstance(),$criteria,false); break;
			case 'area' : $data = $this->model->simpleGraph(ContractArea::getInstance(),$criteria,false); break;
			case 'type' : $data = $this->model->simpleGraph(ContractType::getInstance(),$criteria,false); break;
			default : $data = []; break;
		}				
		$this->data = [$analytic=>$data];
	}
	
	/**
	 * @desc get short descriptive bar graph data based on particular field
	 */
	public function barStatsAction()
	{
		$status = ContractStatuse::getInstance();
		$status_pk = $status->primaryKey();	
		
		$criteria = [$status_pk=>$status->fetchColumn($status_pk,['code'=> $this->getParams('code')])];		
		$analytic = $this->getParams('analytic');
	
		switch ($analytic)
		{
			case 'status' : $data = $this->model->simpleGraph(ContractStatuse::getInstance(),$criteria); break;
			case 'department' : $data = $this->model->simpleGraph(OrganisationUnit::getInstance(),$criteria); break;
			case 'area' : $data = $this->model->simpleGraph(ContractArea::getInstance(),$criteria); break;
			case 'type' : $data = $this->model->simpleGraph(ContractType::getInstance(),$criteria); break;
			default : $data = []; break;
		}
		
		$this->data = [$analytic=>[['values'=>$data]]];
	}
}