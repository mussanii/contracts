<?php
class UserSettingsController extends CustomController
{
	public function resetAction()
	{
		$this->model->reset();
		$this->data = ['message'=>MESSAGE_SETTINGS_RESET,'title'=>TITLE_SETTINGS_RESET];
	}
	
	public function mapAction()
	{
		$results = (array)$this->model->select();
		$data = [];
		foreach ($results as $row)
		{
			$data[strtolower($row['code'])] = $row;
		}
		$this->data = $data;
	}
}