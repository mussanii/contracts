<?php
class CustomersController extends CustomController{
	
	protected $sanitize_data=false;
	
	public function autocompleteAction(){
		$search = '%'.$this->getParams('term').'%';	
		$party = Party::getInstance();
		$criteria = [new Like($party->concat(['full_name','nick_name','kra_pin']),$search, 'full_name')];
		$party_id = $party->fetchColumn($party->primaryKey(),$criteria);		
		$criteria = [$party->primaryKey()=>$party_id]; 
		$this->data = ['data' => $this->model->select($criteria,null,null,null,' LIMIT 5 ')];
	}
	
	public function listAction()
	{
	    if($this->identity->isAdmin()!=true){
	        $this->query_array['owner_id'] = $this->identity->getId();
	    }	    	
		return parent::listAction();
	}
	
	public function registerAction()
	{
		$this->saveAction();
	}
}