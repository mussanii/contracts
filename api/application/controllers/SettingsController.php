<?php
class SettingsController extends CustomController{
	
	public function mineAction()
	{
		$user_setting = UserSetting::getInstance();		
		$data = $user_setting->select([$this->identity->primaryKey()=>$this->identity->getId()]);
		$this->data = $data? $data : [];
	}
	
	public function oneAction()
	{
		$code = $this->getParams('code',null);		
		$user_setting = UserSetting::getInstance();				
		$setting_id = $this->model->fetchColumn($this->model->primaryKey(),['code'=>$code]);		
		$data = $user_setting->filterOne([$this->model->primaryKey()=>$setting_id]);		
		$this->data = $data? $data : null;
	}
	
}