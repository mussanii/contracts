<?php
class WorkflowStatusesController extends CustomController
{
	/**
	 * --------------------- LIST POSSIBLE statusS ----------------------------
	 * @api {get} /workflow-statuses/list list statuses
	 * @apiName ListStatuses
	 * @apiGroup WorkflowStatus
	 *
	 * @apiSuccessExample {Json} Success-Response Example:
	 *     HTTP/1.1 200 OK
	 *     [
	 *     	{
	 *     		status_id : 1,
	 *     		status_title: "Reject" //Status Name
	 *     		code: "REJ" //Short Code For Status
	 *     		color_hexa: "#000" //Hexadecimal Color Code For This Status
	 *     	},
	 *      ...
	 *     ]
	 */
}