<?php
class MailersController extends CustomController
{
	/**
	 * @desc autocomplete for emails begins here
	 * @method autocompleteAction()
	 *
	 */
	public function autocompleteAction(){
		//create some instances here and retrieve filter columns from post
		$search = '%'.$this->getParams('term').'%';
		$person = Person::getInstance();
		$organisation = Organisation::getInstance();
	
		//get persons emails
		$criteria = [new Like('primary_email',$search, 'email')];
		$person_emails = (array)$person->select($criteria,['primary_email'],null,null,' LIMIT 2 ');
	
		//get organisations emails
		$criteria = [new Like('primary_email',$search, 'email')];
		$organisation_emails = (array)$organisation->select($criteria,['primary_email'],null,null,' LIMIT 2 ');
	
		//get recipients emails - in sent folders
		$criteria = [new Like('recipient',$search, 'email')];
		$recipients = (array)$this->model->select($criteria,['DISTINCT(recipient) AS primary_email'],null,null,' LIMIT 5 ');
	
		//merge the two emails
		$emails = array_merge($person_emails,$organisation_emails,$recipients);
		$data = [];
		$unique = [];
		foreach ($emails as $e)
		{
			if(in_array($e['primary_email'],$unique)==false){
				$data[] = $e;
				$unique[] = $e['primary_email'];
			}
		}
		$this->data = ['data' => $data];
	}
	
	public function pendingAction()
	{
		$setting = Setting::getInstance();
		
		$expiry_ts = intval($setting->fetchColumn('value',['code'=>'EXT']))*24*60*60;
		
		$box_type = MailBoxe::getInstance();
		
		$box_pk = $box_type->primaryKey();
		
		$criteria = [
				[$box_pk => $box_type->fetchColumn($box_pk,['code'=>'IB'])],
				new GreaterThanOrEqualTo('created_at',time()-$expiry_ts,'created_ata'),
				new LessThanOrEqualTo('created_at + '.$expiry_ts,time(),'created_atb')
		];
		
		$data = $this->model->select($criteria,null,null,$this->model->primarykey().' DESC');		
		$this->data = $data? $data : [];
	}
		
	private function setMailerOnPost()
	{
		$recipient = array_filter(explode(';',$this->getPost('recipient')));
		$cc = array_filter(explode(';',$this->getPost('cc')));
		$bcc = array_filter(explode(';',$this->getPost('bcc')));
		
		$this->model->setAttachments($this->getPostedFiles('attatchments'));
		$this->model->setSender();
		$this->model->setRecipient([array_pop($recipient)]);
		$this->model->setCc(array_merge($cc,$recipient));
		$this->model->setBcc($bcc);
	}
	
	public function saveAction()
	{
		$box_type = MailBoxe::getInstance();
		
		$data[$box_type->primaryKey()] = $box_type->fetchColumn($box_type->primaryKey(),['code'=>'DF']);		
		$subject = $this->getPost('subject');
		
		$this->setMailerOnPost();
		$this->model->updateQueue(null,$subject, $this->getPost('body'),$data[$box_type->primaryKey()]);
	
		if($this->model->recordsAffected()>0)
		{
			//save to our database
			$this->model->update($data,[$this->model->primaryKey()=>$this->model->lastAffectedId()]);
			$data = ['message' => MESSAGE_SAVE_SUCCESS.' '.$this->getPost('recipient'),'status'=>'success','success'=>1];
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_SAVE_FAILURE;
			$data = ['message' => $message,'status'=>'error','success'=>0];
		}
		$this->data = array_merge($data,['title'=>TITLE_RECORD_OPERATION]);
	}
	
	public function sendAction()
	{
		$subject = $this->getPost('subject');		
		$this->setMailerOnPost();		
		$response = $this->model->sendEmail($this->getPost('body'),$subject,true);			
		
		if($response==true)
		{	
			$data = ['message' => MESSAGE_SEND_SUCCESS.' '.$this->getPost('recipient'),'status'=>'success','success'=>1];
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_SEND_FAILURE;
			$data = ['message' => $message,'status'=>'error','success'=>0];
		}
		$this->data = array_merge($data,['title'=>TITLE_EMAIL_SEND]);
	}
	
	public function sendSelectedAction()
	{
		$attachment = MailAttachment::getInstance();
		$box_type = MailBoxe::getInstance();		
		$email_pk = $this->model->primaryKey();
		$email_ids = $this->getPost($email_pk);
		$data = (array)$this->model->select([$email_pk=>$email_ids]);
		$box_id = $box_type->fetchColumn($box_type->primaryKey(),['code'=>'ST']);
		$box_pk = $box_type->primaryKey();
		
		$count = 0;
		foreach ($data as $e)
		{
			$sender = $this->model->splitEmail($e['sender']);
			$this->model->setAttachments($attachment->select([$email_pk=>$e[$email_pk]]));
			$this->model->setSender($sender['email'],$sender['name']);
			$this->model->setRecipient(explode(';',$e['recipient']));
			$this->model->setCc(explode(';',$e['cc']));
			$this->model->setBcc(explode(';',$e['bcc']));
			
			$response = $this->model->sendEmail($e['body'],$e['subject'],false);			
			$count = $count + intval($response);	

			if($response==true)
			{
				$this->model->update([$box_pk=>$box_id],[$email_pk=>$e[$email_pk]]);
			}
		}
		
		if($count>0)
		{
			$message = MESSAGE_SEND_SUCCESS.$count.' recipients';
			$status = STATUS_SUCCESS;
			$success = 1;
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_SEND_FAILURE;
			$status = STATUS_ERROR;
			$success = 0;
		}
		
		$this->data = ['message'=>$message,'status'=>$status,'title'=>TITLE_EMAIL_SEND,$success=>$success];
	}
		
	public function readAction()
	{
		global $smtp_config;		 
		$SMTP_USERNAME = $smtp_config['SMTP_USERNAME'];
		
		$box_type = MailBoxe::getInstance();
		
		$reader = ImapReader::getInstance();
		
		try
		{
			$emails = $reader->getMessages($this->getParams('retrieve_attatchments','N'));
		}
		catch (Exception $ex)
		{
			
		}
		
		$count = 0;
		
		foreach ($emails as $msg)
		{
			//save to our database
			$box_pk = $box_type->fetchColumn($box_type->primaryKey(),['code'=>'IB']);
			
			$sender = $this->model->splitEmail($msg['sender']);	
			$recipient = $msg['recipient'];
			
			$this->model->setSender($sender['email'],$sender['name']);			
			$this->model->setRecipient(explode(';',$recipient));
			$this->model->setCc(explode(';',$msg['cc']));
			$this->model->setBcc(explode(';',$msg['bcc']));
			
			//retrieve bcc data here to save alongside the mail
			$subject = $msg['subject'];
							
			$body = '<span><b>From: </b></span> '.$sender['email'].' [mailto:'.$sender['name'].'] <br/>
					<span><b>Sent: </b></span> '.date('D, M d, Y h:i:s a',$msg['time']).' <br/>
					<span><b>To: </b></span> \''.$msg['recipient'].'\'<br/>'.
					($msg['cc']? '<span><b>Cc: </b></span> '.$msg['cc'].' <br/>' : '').
					'<span><b>Subject: </b></span> '.$subject.'<br/>'.$msg['body'];
					
			$this->model->updateQueue(null,$subject, $body,$box_pk);	
			
			$count = $count + $this->model->recordsAffected();
		}
		
		if($count>0)
		{
			$message = $count.' '.MESSAGE_MAIL_READ_SUCCESS.' at '.$SMTP_USERNAME;
			$status = STATUS_SUCCESS;
			$success = 1;
		}
		else
		{
			$message = $this->model->isError()==true? $this->model->message() : MESSAGE_MAIL_READ_NONE.' at '.$SMTP_USERNAME;
			$status = STATUS_ERROR;
			$success = 0;
		}
		ob_clean();
		$this->data = ['message'=>$message,'status'=>$status,'title'=>TITLE_EMAIL_READ,$success=>$success,'count'=>$count];
	}
	
}