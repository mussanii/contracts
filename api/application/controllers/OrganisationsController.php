<?php
class OrganisationsController extends CustomController
{
	public function profileAction()
	{
	    $payload = $this->identity->getPayload();
	    $this->_id = @$payload['organisation']['organisation_id'];
	    parent::detailsAction();
	}
}