<?php
class AddressesController extends CustomController{
	
	protected $sanitize_data=false;	
	public function autocompleteAction(){
		$term = $this->getParams('term');		
		$this->data = ['data' => $this->model->select(['address'=>$term])];
	}
}