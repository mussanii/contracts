<?php
class PersonImagesController extends ImagesController{
	
	/*
	 * @method downloadAction()
	 * @desc download image content from database create a default image if none found
	 * @return $blob string field content echoed on browser as image
	 */
	public function downloadAction()
	{
		$person = Person::getInstance();
		$data=$this->model->filterOne([$person->primaryKey() => $this->_id, 'active'=>'Y']);		
		if($data)
		{
			$content=$data['content'];			
			$type=$data['mime_type'];
			$name=$data['file_name'];			
		}
		else
		{
			$content=file_get_contents(MAIN_ROOT.'/resources/img/avatar-default.png');
			$type="image/png";
			$name="Image File";			
		}
		$this->download($content, $name, $type);
	}
	
	/*
	 * @method saveAvatarAction()
	 * @desc saves an avatar/image received from post request
	 * @desc on successful save/update, the current image for a person id is set as the active
	 * @desc while other images for this person id are inactivated
	 * @return array $data which contains success and failure flags, last updated id and messages
	 */
	public function uploadAction()
	{
		if($this->hasPostedFiles())
		{
			$image = PersonImage::getInstance();
			$parent = Person::getInstance();
			$download_url = '/person-images/download/';
			$this->upload($image, $parent, $download_url);
		}
	}
}