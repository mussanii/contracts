 <?php
class UsersController extends CustomController
{		
    protected $search_columns = ['username'];
	public function __construct($url_array, $query_array)
	{
		parent::__construct($url_array, $query_array);
		$this->import = UserImport::getInstance();
	}
	
	/**
	 * ------------------------------- LOGIN A USER ---------------------------------
	 * @api {post} /users/login/ authenticate user into api
	 * @apiName LoginUser
	 * @apiGroup Users
	 *
	 * @apiParam {String} username A Unique Name For A User
	 * @apiParam {String} password  A Password For The Above username
	 * 
	 * @apiSuccess {Number}  success  Status Of The Login Operation (0-failure, 1-success).
	 * @apiSuccess {String}  status  Status of Login Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Login Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Login Operation
	 * @apiSuccess {String}  access_token  An JWT Token To Authenticate Other APIs After (Appended As GET parameter)
	 * @apiSuccessExample {Json} Success-Response Sample:
	 *     	{
	 *       "success": 1,
	 *       "status" : "success",
	 *       "message" : "You Have Been Logged In Successfully",
	 *       "title" : "Login Results",
	 *       "access_token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2Vy........" //It Could Be Very Long Sometimes
	 *     	}
	 */
	public function loginAction()
	{

		/*  ---Try to login using an access token. a token has to be valid and unique per user ---- */
		$access_token = $this->model->accessToken();
		$payload = $this->model->globalVar()->getTokenPayLoad($access_token);		
		if (isset($payload ['username']) && $this->model->exists(['session_token'=>$access_token]))
		{
			$username = $this->model->getUsernameByEmail($payload ['username']);
			$this->model->setPayload($username); 
			$objects = $this->model->getAuthorisation($this->identity->getDefaultRole());						
			$this->data = ['title'=>TITLE_LOGIN_RESPONSE,'status'=>STATUS_SUCCESS,'success'=>1,'message'=>MESSAGE_LOGIN_SUCCESS,'token'=>$access_token,'objects'=>$objects];
			return;
		}
		/*  ---Try to login using an access token. a token has to be valid and unique per user ---- */
		
		/*proceed to login the normal way*/
		$user = $this->getPost();		
		$username = isset($user['username'])? $user['username'] : null;
		$username = $this->model->getUsernameByEmail($username);
		$password = isset($user['password'])? $user['password'] : null;
		$password = $this->model->getPasswordSalt($password);		
		$criteria = ['username'=>$username,'password'=>$password];
	
		$response=[];
		
		/* Test if both username and password not blank */
		/* Send Error & Flag 0 If either or both username and password blank */
		/* Otherwise continue with authentication process */
		if($username=='' || $password=='')
		{
			$response = ['status'=>STATUS_INFO,'success'=>0,'message'=>MESSAGE_MISSING_CREDENTIALS];
		}
		else
		{
			/*Determine if an account has been suspended then abort login action*/
			/*Else continue the other login operations*/
			if($this->model->exists(['username'=>$username,'suspended'=>'Y'])==true)
			{
				$response = ['status'=>STATUS_ERROR,'success'=>0,'message'=>MESSAGE_USER_SUSPENDED];
			}
			else
			{
				/* Test if this user account with given password exists */
				/* Continue with authentication if username and Password correct*/
				/* Otherwise Send Error Message & Flag 0 on wrong username or password*/
				if($this->model->exists($criteria)==true)
				{
					/* reset number of password attempts to '0' after successful login */
					$this->model->update(['password_attempts'=>0],['username'=>$username]);
					
					/* Check if a password confirmed [done for new accounts] */
					/* Continue with authentication if password already confirmed */
					/* Otherwise Send Error Message and Flag 0 on unconfirmed account*/
					$criteria['confirm_password']=$password;
					
					if($this->model->exists($criteria)==true)
					{
						$this->model->setPayload($username);
						$objects = $this->model->getAuthorisation($this->identity->getDefaultRole());
						$response = ['status'=>STATUS_SUCCESS,'success'=>1,'message'=>MESSAGE_LOGIN_SUCCESS,'token'=>$this->model->generateToken(),'objects'=>$objects];
					}				
					else 
					{
						$response = ['status'=>STATUS_INFO,'success'=>0,'message'=>MESSAGE_UNCONFIRMED_PASSWORD];
					}
				}
				else
				{
					/* record number of failed password attempts on login failure */
					$m = Setting::getInstance()->fetchColumn('value',['code'=>'MLA']);
					$n = intval($this->model->fetchColumn('password_attempts',['username'=>$username])) + 1;
					$data = ($n>=$m)? ['password_attempts'=>$n,'session_token'=>null,'suspended'=>'Y'] : ['password_attempts'=>$n];
					$this->model->update($data,['username'=>$username]);								
					$response = ['status'=>STATUS_ERROR,'success'=>0,'message'=>MESSAGE_WRONG_CREDENTIALS];
				}
			}
		}		
		$response['title'] = TITLE_LOGIN_RESPONSE;		
		$this->data = $response;
	}
		
	public function recoverPasswordAction()
	{
		$person = Person::getInstance();
		$person_id = $person->fetchColumn($person->primaryKey(),['primary_email'=>$this->getPost('username')]);
		$criteria = [$person->primaryKey()=>$person_id];
		
		if($this->model->exists($criteria)==true)
		{
			$recipient=$this->model->filterOne($criteria);			
			
			$subject = "Your reset password request at ".SITE_NAME;
			$email=$recipient['primary_email'];
			$name = $recipient['first_name'].' '.$recipient['last_name'];
			$reset_password=$this->getResetPasswordCode ($email.time());
			
			$this->model->update(['reset_password'=>$this->model->getPasswordSalt($reset_password)],$criteria);
			$message = "There was a request to reset your password at ";
			$body = $this->model->getEmailNewUserAccount($email, $name, $reset_password,$message);
			$this->sendEmail ( $email, $name, $body, $subject );
			
			$response = ['status'=>STATUS_SUCCESS,'success'=>1,'message'=>MESSAGE_PASSWORD_SENT.$email];
		}
		else
		{
			$response = ['status'=>STATUS_ERROR,'success'=>0,'message'=>MESSAGE_EMAIL_INVALID];
		}
		
		$response['title'] = TITLE_PASSWORD_RECOVERY;
		$this->data = $response;
	}
	
	/*
	 * @method changePasswordAction()
	 * @desc confirm new password 
	 * @param string $code hashed code to validate email
	 * @param string $email email account to validate against
	 * @param string $old_password value of old password before change
	 * @param string $new_password value of new password on change request
	 */
	public function changePasswordAction()
	{
		$person = Person::getInstance();
		
		$token = $this->model->getTokenPayLoad($this->getPost('reset_token'));		
		$email = isset($token['email'])? trim($token['email']) : null;
		$code = isset($token['code'])? trim($token['code']) : null;
		$code = $this->model->getPasswordSalt($code);
		$password = $this->getPost('password');
		$old_password = $this->model->getPasswordSalt($this->getPost('current_password'));
		
		$person_id = $person->fetchColumn($person->primaryKey(),['primary_email'=>$email]);
		$person_pk = $person->primaryKey();
		
		//check scenario if it is about resetting/changing password while logged in
		$change_old = ($old_password && $this->_id)? true : null;
        $criteria = ($change_old && !$email)? [$this->model->primaryKey()=>$this->_id] : [$person_pk=>$person_id];
        $criteria['reset_password'] = $code;
		$reset_pass = ($email && $password && $code && $this->model->exists($criteria)==true)? true : null;
		
		//set data selection/update criteria as accordingly
		$criteria = ($reset_pass)? [$person_pk=>$person_id] : [$this->model->primaryKey()=>$this->_id] ;
		
		/* Check for either scenario **/
		/* on missing throw message and error ELSE proceed **/
		if($change_old || $reset_pass)
		{
			$passw_criteria=$criteria;
			$passw_criteria['password'] = $old_password;
			if($change_old && ($this->model->exists($passw_criteria)!==true))
			{
				$response=['message' => MESSAGE_WRONG_OLD_PASSWORD, 'success' => 0 , 'status' => STATUS_ERROR];
			}
			else
			{
				$password = $this->model->getPasswordSalt($password);
				$data = ['tnc'=>'Y','active'=>'Y','password'=> $password,'confirm_password'=>$password,'reset_password'=>null];				
				$data = array_merge($data,['suspended'=>'N','password_attempts'=>0]);
				$this->model->update($data,$criteria);
				$response=['message' => MESSAGE_PASSWORD_CHANGED, 'success' => 1 , 'status' => STATUS_SUCCESS];
			}			
		}
		else
		{
			$response=['message' => MESSAGE_USER_IDENTITY_MISSING, 'success' => 0 , 'status' => STATUS_INFO];
		}	
		
		$response['title'] = TITLE_PASSWORD_CONFIRM;		
		$this->data = $response;
	}
	
	/*
	 * @method rolesAction()
	 * @desc retrieve all active user roles
	 * @param int $id user id
	 * @return array $data 2-d list of user roles
	 */
	public function rolesAction()
	{
		$user_role = UserRole::getInstance();
		$role = Role::getInstance();
		$data = $user_role->fetchColumn($role->primaryKey(),[$this->model->primaryKey() => $this->_id,'active'=>'Y'],true);
		$this->data = $data;
	}
	
	/*
	 * @method saveAction()
	 * @desc saves a user record either new or updates to the same record
	 * @overrides super class 'saveAction()' method
	 * @starts by saving personal details of user
	 */
	public function saveAction()
	{
		$response=[];
		$account = $this->getPost();
		$role_ids_posted = $this->getPost('role_id');
				
		$person = Person::getInstance();
		
		//extract data from post
		$person_data = $person->sanitize($account);		
		$user_detail = $this->model->removePasswords($this->model->sanitize($account));
		
		/* Check if posted username exists */
		/* If username exists, stop operation and return error message*/
		/* If username doesnt exist, save person/profile, user and billing details */
		/* on successful user creation, create and send token along */
		$user_id = $user_detail[$this->model->primaryKey()];
		$username = $user_detail['username'];
		$primary_email = $person_data['primary_email'];
		
		$test_a = $this->model->exists(['username'=>$username]);
		$test_b = $this->model->exists(['username'=>$username,$this->model->primaryKey()=>$user_id]);
				
		$user_role_records_affected = 0;
		
		if((!$user_id && $test_a==false) || ($test_b==true) || ($user_id && $test_a==false))
		{
			//start a new transaction
			$person->startTransaction();	

			//collect more person details b4 saving
			$person_data['last_updated'] = time();
			
			//save person data here
			$person->save($person_data);			
			
			if((int)$person->recordsAffected()>0 && $person->isError()!==true)
			{				
				//collect more user detail here
				if(!$user_id)
				{
					$user_detail['effective_from']= time();
					$user_detail['id_parent']= $this->identity->getId();
				}
				
				$user_detail[$person->primaryKey()]=$person->lastAffectedId();
				
				//save user details			
				$this->model->save($user_detail);
				
				$last_affected_id = (int)$this->model->lastAffectedId();
				
				//check if any user record saved, then save other secondary, role records
				if((int)$this->model->recordsAffected()>0 || $this->model->isError()!=true)
				{
					//save/update user roles 
					$role_ids_posted = is_array($role_ids_posted)==true? array_filter($role_ids_posted) : [];
					if(count($role_ids_posted)>0)
					{
						$user_role_records_affected = $this->model->saveRoles($role_ids_posted,$this->model->lastAffectedId());
					}
				}				
			}
			else{
				$this->model->isError($person->isError());
				$this->model->message($person->message());
			}
	
			$count = $person->recordsAffected() + $this->model->recordsAffected() + $user_role_records_affected;
			if($count>0 && $this->model->isError()!==true)
			{
				//commit transaction here
				$person->endTransaction();
			
				//send email on new account created
				$criteria = ['username'=>$user_detail['username']];
				$recipient=$this->model->filterOne($criteria);
				$updated_subject = "Changes made at ".SITE_NAME."'".$criteria['username']."'";
				$added_subject = "New user account at ".SITE_NAME;				
				$subject = ($user_id)? $updated_subject : $added_subject;
				$email=$recipient['primary_email'];
				$name = $recipient['first_name'].' '.$recipient['last_name'];
			
				//save password reset code
				if(!$user_id)
				{
					//send email on new account created
					$reset_code = $this->getResetPasswordCode($email);
					$this->model->update(['reset_password'=>$this->model->getPasswordSalt($reset_code)],$criteria);						
					$added_message = $this->model->getEmailNewUserAccount($email,$name,$reset_code);					
					$body = ($user_id)? $update_message : $added_message;						
					$this->sendEmail ( $email, $name, $body, $subject, true );
					//send email on new account created
					
					$message = MESSAGE_REGISTRATION_SUCCESS.$person_data['primary_email'];
				}				
				else
				{
					//audit this message
					$update_message="Some changes were made on user account '" .$criteria['username']."' at ".SITE_NAME.".<br/><br/>If this account doesnt belong to you please ignore this message.";

					$message = MESSAGE_USER_UPDATED.$person_data['primary_email'];
					$subject = $message;
					$body = ($user_id)? $update_message : $added_message;
					$this->sendEmail( $email, $name, $body, $subject, true);
				}				
						
				$response=['message' => $message, 'success' => 1 , 'status' => STATUS_SUCCESS,'count'=>$count,'id'=>$last_affected_id];				
				
			}
			else
			{
				$message = $this->model->message();
				$message = strpos($message,$primary_email)>0 && strpos($message,'unique')>0? $primary_email.MESSAGE_ALREADY_TAKEN :  $message;
				$response=['message' =>  $message, 'success' => 0 , 'status' => STATUS_ERROR];
			}
		}
		else
		{
			$response=['message' => MESSAGE_USER_ALREADY_EXISTS, 'success' => 0 , 'status' => STATUS_INFO];
		}
		
		$response['title'] = TITLE_USER_REGISTRATION;
		
		$this->data = $response;
	}
		
	/*
	 * @method exists()
	 * @desc check if username exists
	 * @param string $username 
	 * @return boolean $exists
	 */
	public function existsAction()
	{
		$this->data = ['exists'=>$this->model->exists(['username'=>$this->getPost('username')])];
	}
	
	/*
	 * @method autocompleteAction()
	 * @desc auto search through many records using a search term
	 * @param string $term
	 * @return array $data 
	 */
	public function autocompleteAction(){
		$person = Person::getInstance();
		$search = '%'.$this->getParams('term').'%';
		
		$criteria1 = [new Like($this->model->concat(['username']),$search, 'username'),new NotEqual('username','sa', 'username')];		
		$person_id1 =  $this->model->fetchColumn($person->primaryKey(),$criteria1,true);
		
		$criteria2 = [new Like($person->concat(['first_name','last_name']),$search, 'full_name')];
		$person_id2 =  $person->fetchColumn($person->primaryKey(),$criteria2,true);
		
		$person_id = array_merge($person_id1,$person_id2);
		$this->data = ['data' => $this->model->select([$person->primaryKey()=>$person_id],null,null,null,' LIMIT 5')];
	}
	

	/*
	 * @method namesAction()
	 * @desc fetch names of users with their ids as keys
	 */
	public function namesAction()
	{
		$model = $this->model; 
		$pk = $model->primaryKey();
		$this->data = $model->getArrayAssoc($model->select([],[$pk,'username']),$pk,'username');
	}
	
	/**
	 * ------------------------------- GUEST INVITE/LINK SHARE ---------------------------------
	 * @api {post} /users/collaborate/ share invite or document link 
	 * @apiName GuestInvite
	 * @apiGroup Users
	 *
	 * @apiParam {String} user_id (Optional) The user's unique id
	 * @apiParam {String} first_name The guest's first name
	 * @apiParam {String} last_name The guest's last name
	 * @apiParam {String} primary_email The guest's email address
	 * 
	 * @apiSuccess {Number}  success  Status Of The Login Operation (0-failure, 1-success).
	 * @apiSuccess {String}  status  Status of Login Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message Resulting From The Login Operation
	 * @apiSuccess {String}  title  Short/Summarized Error Or Success Message Resulting From The Login Operation
	 * @apiSuccessExample {Json} Success-Response Sample:
	 *     	{
	 *       "success": 1,
	 *       "status" : "success",
	 *       "message" : "Collaboration link sent successfully to xxx@gmail.com",
	 *       "title" : "Guest Invitation",
	 *     	}
	 */
	public function collaborateAction()
	{
		global $COMPANY_CODE;
		global $global;
	
		//Gather some post variables and create instances
		$role = Role::getInstance();
		$person = Person::getInstance();
		$person_pk = $person->primaryKey();
		
		$user_id = (int)$this->getPost($this->model->primaryKey());
		
		if($user_id==0)
		{			
    		$role_id = $role->fetchColumn($role->primaryKey(),['code'=>'CB']);
    		$email = $this->getPost('primary_email');
    		$password_plain = $this->model->getResetPasswordCode($email.time());
    		$password = $this->model->getPasswordSalt($password_plain);
    		$data = array_merge($this->getPost(),['active'=>'Y','is_super'=>'N','is_admin'=>'N','password'=>$password]);
    		
    		$username = $this->model->getUsernameByEmail($email);
    		$amend_criteria = ['username'=>$username];
    		if(($amending = $this->model->exists($amend_criteria))==true)
    		{
    		    $old_password = $this->model->fetchColumn('password',$amend_criteria);
    		    $this->model->update(['password'=>$password,'confirm_password'=>$password],$amend_criteria);
    		}
    		else
    		{		    
                $this->model->cloneRecord(['is_super'=>'Y'],$data,$role_id,true);
    		}
		}
		else
		{
		    $amending = true;
		    $user = $this->model->selectOne($user_id);
		    $email = @$user['primary_email'];
		    
		    $amend_criteria = [$this->model->primaryKey()=>$user_id];
		    $old_password = $this->model->fetchColumn('password',$amend_criteria);
		    
		    $password_plain = $this->model->getResetPasswordCode($email.time());
		    $password = $this->model->getPasswordSalt($password_plain);
		    
		    $this->model->update(['password'=>$password,'confirm_password'=>$password],$amend_criteria);
		}
	
		//compose response
		$message = $this->model->isError()==true? $this->model->message() : MESSAGE_GUEST_COLLABORATE_SUCCESS.$email;
		$title = TITLE_GUEST_COLLABORATION;
		$success = $this->model->isError()==true? 0 : 1;
		$status = $this->model->isError()==true? STATUS_ERROR : STATUS_SUCCESS;
	
		//send email if a user by this email exists
		if($success==1 && ($person_id = $person->fetchColumn($person->primaryKey(),['primary_email'=>$email])))
		{
			//Generate token by curl request and set to original user
			$request = ['username'=>$email,'password'=>$password_plain,'company_code'=>$COMPANY_CODE];
			$body = $this->model->globalVar()->arrayToQuery($request);
			$response = $this->model->globalVar()->sendCurlRequest(SITE_URL.'/api/users/login',$body);
			
			//update old password if in amending mode
			if($amending==true)
			{
			    $this->model->update(['password'=>$old_password,'confirm_password'=>$old_password],$amend_criteria);
			}
				
			//compose email and send
			$recipient = $this->model->filterOne([$person->primaryKey()=>$person_id]);
			$user_id = $recipient[$this->model->primaryKey()];
			$name = $recipient['full_name'];
			$subject = TITLE_GUEST_COLLABORATION.' at '.SITE_NAME;
			$link_label = SITE_URL.'/#'.$this->getPost('link');
			 
			
			$link = $global->addUriQueryParam(SITE_URL.'/#'.$this->getPost('link'),'access_token',@$response['token']);
				
			$body = "Hi ".$name.",<br/><br/>You have been invited to collaborate on document at ". SITE_NAME.
			" on <b>\"".$this->getPost("subject")."\"</b><br/>".
			"<br/>Please click on the link below to open document.<br/>".
			"<a href='".$link."'>".$link_label."</a><br/>Regards<br/>".
			$this->model->identity()->getFullName().",".SITE_NAME;
				
			$this->sendEmail( $email, $name, $body, $subject, true);
		}
	
		$this->data = ['id'=>$user_id,'message'=>$message,'success'=>$success,'status'=>$status,'title'=>$title];
	}
	
}