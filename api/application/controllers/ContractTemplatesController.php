<?php
class ContractTemplatesController extends CustomController
{
	protected  $sanitize_data = false;

	public function autocompleteAction(){
		$search = '%'.$this->getParams('term').'%';
		$criteria = [new Like('template_title',$search, 'template_title')];		
		$this->data = ['data' => $this->model->select($criteria,null,null,null,' LIMIT 5 ')];
	}
	
	public function namesAction()
	{
		$data = $this->model->select([],[$this->model->primaryKey(),'template_title']);
		$this->data = $data? $data : [];
	}
}