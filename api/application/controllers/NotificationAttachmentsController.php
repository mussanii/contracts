<?php
class NotificationAttachmentsController extends DocumentsController
{
	public function __construct($url_array,$query_array)
	{
		parent::__construct($url_array,$query_array);
		$this->fk_table = Notification::getInstance();
	}

	/**
	 * --------------------- UPLOAD NOTIFICATION ATTACHMENT ----------------------------
	 * @api {post} /notification-attachments/upload upload a attachment at a step
	 * @apiName UploadAttachment
	 * @apiGroup NotificationAttachment
	 *
	 * @apiParam {Integer}  id Unique Id For A Notification
	 * @apiParam {Object} file Name Of Uploaded File Object.
	 *
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "answer" : "File transfer completed"
	 *     }
	 *
	 * @apiErrorExample {Json} Success-Response:
	 *     HTTP/1.1 400 Error
	 *     "No Files"
	 */
	
	
	/**
	 * --------------------- DOWNLOAD/VIEW NOTIFICATION ATTACHMENT -------------
	 * @api {get} /notification-attachments/download/{id} download a attachment
	 * @apiName DownloadAttachment
	 * @apiGroup NotificationAttachment
	 *
	 * @apiParam {Integer}  id Unique Id Of A attachment
	 *
	 */	
	
	
	/**
	 * --------------------- DELETE DOCUMENT ----------------------------
	 * @api {get} /notification-attachments/delete/{id} delete a attachment
	 * @apiName Deleteattachment
	 * @apiGroup NotificationAttachment
	 *
	 * @apiParam {Integer}  id Unique Id Of A attachment
	 *
	 * @apiSuccess {Integer} count No. Of Records Successfully Deleted
	 * @apiSuccess {Number}  success  Status Of The Record Operation (0-failure, 1-success,2-No effect).
	 * @apiSuccess {String}  status  Status of Record Operation ; Possible Values (info,warning,success,error)
	 * @apiSuccess {String}  message  Detailed Error Or Success Message For The Record Operation
	 * @apiSuccess {Integer} id Unique ID Of The Just Deleted Record
	 * @apiSuccessExample {Json} Success-Response:
	 *     HTTP/1.1 200 OK
	 *     {
	 *       "success" : "1",
	 *       "id" : "1",
	 *       "status" : "info",
	 *       "message" : "Record deleted successfully"
	 *       "title" : "Record Operation"
	 *     }
	 */
	
}