<?php
class ContractsController extends CustomController
{
	protected  $sanitize_data = false;
	protected $search_columns = ['contract_no','contract_title','description'];

	public function autocompleteAction(){
		$search = '%'.$this->getParams('term').'%';
		$criteria = [new Like($this->model->concat(['contract_title','contract_no']),$search, 'contract_title')];		
		$this->data = ['data' => $this->model->select($criteria,null,null,null,' LIMIT 5 ')];
	}
	
	/**
	 * --------------------- LIST POSSIBLE CONTRACT PROCESSES ----------------------------
	 * @api {get} /contracts/process get contract processes/flows
	 * @apiName ContractProcesses
	 * @apiGroup Contracts
	 *
	 */
	public function processesAction()
	{
		//obtain required table instances
		$type = WorkflowProcessType::getInstance();
		$contract_process = ContractProcesse::getInstance();
		$process = WorkflowProcesse::getInstance();
		$status = WorkflowStatuse::getInstance();
		$data = (array)$type->select();
		
		//primary keys shorthand
		$type_key = $type->primaryKey();
		$process_key = $process->primaryKey();
		$model_pk = $this->model->primaryKey();
		
		//obtain all process details for this contract
		$criteria = $this->_id? [$model_pk=>$this->_id] : null;
		$cp = (array)$contract_process->select($criteria);
		$process_id = $process->getArrayMap($cp,$process_key);
		$columns =  null;
		$processes = (array)$process->select([$process_key=>$process_id],$columns);
		
		//add detailed process information to contract processes
		$processes = $process->join2DArrays($processes,$cp,$process_key);
		
		$calendar = [];
		
		$path = ObjectAction::getInstance()->fetchColumn('path_ref',['code'=>'WFEXC']);
		
		//loop to create tree with process types
		foreach($data as &$rowt)
		{
			//combine with process information if match found among types
			$rowt['processes'] = $process->join2DArrays($processes,[$rowt],$type_key);
			
			foreach ($rowt['processes'] as $evt)
			{
				$color = $rowt['color_hexa'];
				$id = $evt[$process_key];
				$calendar[] = [						
						'calendarEventId' => $id,
						'path' => $path.'/'.$id,
						'title' => $evt['process_title'],
						'color' => ['primary'=>$color,'secondary'=>$color],
						'startsAt' => $evt['submitted_on'],
						'endsAt' => $evt['due_date'],
						'draggable' => true,
						'resizable' => true,
						'actions' => [
							"label: '<i class=\'glyphicon glyphicon-pencil\'></i>'",
							
						  
							"label: '<i class=\'glyphicon glyphicon-remove\'></i>'",
						],	
						
						
				];
			}
		}
		
		$this->data = ['tabular'=>$data,'calendar'=>$calendar];
	}
	
	/**
	 * --------------------- CALENDAR ----------------------------
	 * @api {get} /contracts/calendar obtain calendar events
	 * @apiName ContractCalendar
	 * @apiGroup Contracts
	 *
	 */
	public function calendarAction()
	{
		$this->processesAction();	
	}
	
	/**
	 * --------------------- IMPORT CONTRACTS ----------------------------
	 * @api {get} /contracts/import import contracts from excel
	 * @apiName ImportContracts
	 * @apiGroup Contracts
	 *
	 */
	public function importAction()
	{
		$excel = ContractImport::getInstance();
		$data = $excel->importWorkBook();
		$this->data = $data;
	}
	
	/**
	 * --------------------- EXPORT CONTRACTS ----------------------------
	 * @api {get} /contracts/export export contracts to excel
	 * @apiName ExportContracts
	 * @apiGroup Contracts
	 *
	 */
	public function exportAction()
	{
		$excel = ContractImport::getInstance();
		$data = $excel->exportWorkBook($this->getParams('type',null));
		extract($data);
		$this->download($content, $type, $name);
	}
	
	public function listAction()
	{
		$organisation = Organisation::getInstance();
		$contract_party = ContractParty::getInstance();
		
		$organisation_pk = $organisation->primaryKey();		
		$contract_party_pk = $organisation->primaryKey();
		
		if(isset($this->advanced[$organisation_pk]))
		{
			$organisation_id = $this->advanced[$organisation_pk];
			unset($this->advanced[$organisation_pk]);
			$id = $contract_party->fetchColumn($this->model->primaryKey(),[$organisation_pk=>$organisation_id]);
			$this->query_array['contract_id'] = $id;			
		}		
		return parent::listAction();
	}
}