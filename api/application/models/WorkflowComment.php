<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a workflow comment
 */
class WorkflowComment extends CustomModel
{
	private static $_instance=null;
	
	//Object Associations & Compositions
	private $wf_document;
	private $wf_action;
	private $user;
	
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='workflow_comments';
		parent::__construct();
				
		//capture instances of object associations
		$this->wf_document = WorkflowDocument::getInstance();	
		$this->wf_action = WorkflowAction::getInstance();
		$this->user = User::getInstance();
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$user_pk = $this->user->primaryKey();
		$action_pk = $this->wf_action->primaryKey();
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		
		//get original array source of parent
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
	
		/* populate additional fields */
		if(!$columns || count($columns)>1)
		{
			for ($i=0;$i<count($data);$i++)
			{
				$row = $data[$i];
				
				/* populate documents info for each record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['documents'] = (array)$this->wf_document->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
				}
				/* populate status info for each record */
				if(array_key_exists($user_pk, $row)==true){
					$row['user'] = $this->user->filterOne([$user_pk=>$row[$user_pk]]);
				}
				
				/* populate action info for each record */
				if(array_key_exists($action_pk, $row)==true){
					$row['action'] = (array)$this->wf_action->filterOne([$action_pk=>$row[$action_pk]]);
				}
				
				$data[$i] = $row;
			}
		}
				
		return $data;
	}
	
	public function save($data)
	{
		global $identity;
		if(array_key_exists($this->primaryKey(), $data)==false || (int)$data[$this->primaryKey()]==0)
		{
			$data[$identity->primaryKey()] = $identity->getId();
			$data['submitted_on'] = time();
		}
		$response = parent::save($data);
		
		if ($this->recordsAffected()>0)
		{
			$this->sendMessages($data);
			
			$process = WorkflowProcesse::getInstance();			
			$step = WorkflowProcessStep::getInstance();
			$process_pk = $process->primaryKey();
			$step_pk = $step->primaryKey();			
			
			$step_id = $this->fetchColumn($step_pk,[$this->primaryKey()=>$this->lastAffectedId()]);
			$process_id = $step->fetchColumn($process_pk,[$step_pk=>$step_id]);
			
			$process->reportOnCompletion($process->selectOne($process_id));
		}
		return $response;
	}
	
	/**
	 * @desc sendMessages()
	 * @desc send mails to people at different steps 
	 * @desc if rejected send rejection message to person previous step
	 * @desc if approved send message to person in the next step
	 * 
	 */
	public function sendMessages($comment)
	{
		global $smtp_config;
		
		//table instances that we may need for later record modifications and queries
		$process_tb = WorkflowProcesse::getInstance();
		$step_tb = WorkflowProcessStep::getInstance();
		$action_tb = WorkflowAction::getInstance();
		$role_tb = Role::getInstance();
		$user_tb = User::getInstance();
		$user_role_tb = UserRole::getInstance();
		$person_tb = Person::getInstance();
		$notification_tb = Notification::getInstance();
		
		//primary keys in short
		$process_pk = $process_tb->primaryKey();
		$role_pk = $role_tb->primaryKey();
		$user_pk = $user_tb->primaryKey();
		
		//retrieve current step details and sender roles
		$step = $step_tb->selectOne($comment[$step_tb->primaryKey()]);
		$action = $action_tb->selectOne($comment[$action_tb->primaryKey()]);
		$process = $process_tb->filterOne([$process_pk=>$step[$process_pk]],[$process_pk,'process_title']);
		$steps = $step_tb->select([$process_pk=>$process[$process_pk]]);
		$sender_role = $role_tb->selectOne($step[$role_pk]);
		$subject = $process['process_title'];
		
		//fetch approver detail
		$approver = (array)$user_tb->selectOne($comment[$user_pk]);
		
		//determine current step, retrieve its role, retrieve all user detail  with that role
		$cur_step = $step_tb->filterOne([$process_pk=>$process[$process_pk],'is_current'=>'Y']);
		$user_id = $user_role_tb->fetchColumn($user_pk,[$role_pk=>$cur_step[$role_pk]]);
		$recipient_role = $role_tb->selectOne($cur_step[$role_pk]);
		$recipients = (array)$user_tb->select([$user_pk => $user_id]);
		
		//looop through recipients as we save notifications and send mails
		$recipient_emails = [];
		$recipient_phones = [];
		foreach($recipients as $recipient)
		{
			//retrieve some additional recipient details
			$name = $recipient['full_name'];
			$email = $recipient['primary_email'];
			
			$recipient_emails[] = $email;
			$recipient_phones[] = $recipient['phone'];	
					
			$person_id = $recipient[$person_tb->primaryKey()];
		
			//compose the message body and notifications body 
			$action_style = 'background-color:'.$action['color_hexa'].';color:#fff;padding:5px;';
					
			if(trim($step['completion_msg'])=="")
			{			
				$message = '"'.$process['process_title'].'" has been '.
						'<span style="'.$action_style.'">'.$action['action_effect'].'</span><br/>'.
						'By '.$approver['full_name'].', '.ucwords($sender_role['role_name']).'<br/><br/>'.
						'This Action Was Taken At Step <b>'.$step['step_no'].' of '. count($steps).'</b><br/><br/>'.
						'The Approver Made Some Comments Below: <br/><br/>"<i>'.$comment['comment_title'].'</i>"<br/><br/>'.
						'Please login to complete your role of "'. ucwords($recipient_role['role_name']).'" at step '.
						'<b>'.$cur_step['step_no'].' of '. count($steps).'</b> '.$cur_step['step_title'].'<br/><br/>';
			
				$body = "Hello,<br/><br/>".$message."<br/><br/><br/>Regards.";
				$sms = $process['process_title'].' has been '.$action['action_effect'].' By '.$approver['full_name'];
			}
			else
			{
				$body = $step['completion_msg'];
				$sms = $step['completion_sms'];
			}
			
			/* send notifications to all recipients in this loop */
			$notification = ['title'=>$subject,'message'=>$message,'person_id'=>$person_id,'color_class'=>'label-success'];
			$notification_tb->save($notification);
		}		
		
		//create one recipient and add the rest of the emails as CCs
		$recipient_emails[] = $smtp_config['ADMIN_EMAIL'];
		$primary_recipient = array_shift($recipient_emails);
		
		/* send email messages to all recipients*/
		$this->mailer()->setSender();
		$this->mailer()->setRecipient($primary_recipient);
		$this->mailer()->setCc($recipient_emails);
		
		if($step['email_enabled']=='Y' && count($recipients)>0) $this->mailer()->sendEmail($body, $subject,true);	
		if($step['sms_enabled']=='Y' && count($recipient_phones)>0) $this->sms()->sendSMS($sms,true,implode(';',$recipient_phones));
		
	}		
}