<?php
class WorkflowWeeklyReminder implements WorkflowWeeklyReminderRange  {
    protected $data;
    protected $date_created;
    protected $process;
    public function __construct($data,$process){
        $this->process=$process;
        $this->data=$data;
       // $data["steps"][0]['due_date'];

    }
    function seventhRange($id){
        $date_created =date("Y-m-d", strtotime("-7 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);

    }

    protected  function storeReminders($id,$date_created){
        $reminders =array($this->data);
        
        foreach($reminders as $row){
            $row = [$this->process->primaryKey()=>$id];
            $row = $this->process->getReminder()->sanitize($row);
            $row['reminder_date']=$date_created;
                
        //save the reminders
        $this->process->reminder->save($row);
        //if error encountered break and throw error
        if($this->process->reminder->isError()==true){
            $this->message($this->process->getReminder()->message());
            $this->RollBack();
            break;
        }

       }
    }
    protected function resolveDueDate(){
        return $this->data["steps"][0]['due_date'];
    }
}
