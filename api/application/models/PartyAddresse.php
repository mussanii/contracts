<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a party addresse
 */
class PartyAddresse extends CustomModel
{	
	private $address_type;
	private $addresse; 
	private $party;
	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='parties_addresses';
		parent::__construct();		
		$this->address_type = AddressType::getInstance();
		$this->addresse = Addresse::getInstance();
	}		
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);	
		$addresse_key = $this->addresse->primaryKey();
		$address_type_key = $this->address_type->primaryKey();
				
		for($i=0;$i<count($data);$i++)
		{
			$row=$data[$i];
			if(array_key_exists($addresse_key, $row)==true)
			{
				$row=array_merge($row,$this->addresse->selectOne($row[$addresse_key]));
			}
			if(array_key_exists($address_type_key, $row)==true)
			{
				$row=array_merge($row,$this->address_type->selectOne($row[$address_type_key]));
			}
			$data[$i]=$row;
		}
		
		return $data;
	}
	
	public function getAddressList($criteria)
	{
		$results = $this->select($criteria);
		$results = $results? $results : [];
		$data = "";
		foreach ($results as $row)
		{
			$data[strtolower($row['code'])] = $row;
		}
		return $data;
	}
	
	public function save($data)
	{
		$address_types = $this->address_type->select();
		$addresses = (isset($data['address']) && is_array($data['address'])==true)? $data['address'] : [];
			
		foreach ($address_types as $add)
		{
			$code = strtolower($add['code']);
			if(array_key_exists($code, $addresses)==true)
			{
				$adt_id = $add['adt_id'];
				$address = $addresses[$code];
				if(isset($address['address_text']))
				{
					$address_text = $address['address_text'];					
					$add_id = (int)$this->addresse->fetchColumn($this->addresse->primaryKey(), ['address_text'=>$address_text]);					
					$rec_affected = ($add_id==0)? $this->addresse->save(['address_text'=>$address_text]) : 0;
					$add_id = ((int)$this->addresse->lastAffectedId()>0)? $this->addresse->lastAffectedId() : $add_id;
					$data = array_merge($data,['adt_id'=>$adt_id,'add_id'=>$add_id,'active'=>'Y','effective_from'=>time()]);
					$data = $this->sanitize($data);
					parent::save($data);
				}
			}
		}	
	}
}
