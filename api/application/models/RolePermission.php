<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a role permission
 */
class RolePermission extends Toggler
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='roles_permissions';
		parent::__construct();				
	}	
	
	public function save($data)
	{
		$action = ObjectAction::getInstance();
		$role = Role::getInstance();
		return $this->toggle($data,[$action->primaryKey(),$role->primaryKey()]);	
	}
	
}
