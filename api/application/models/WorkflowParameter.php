<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a workflow parameter
 */
class WorkflowParameter extends CustomModel
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='workflow_parameters';
		parent::__construct();	
	}
	
	public function save($data){
		if(isset($data['parameter_value']) && isset($data['data_type'])  && $data['data_type']=='datetime')
		{
			$data['parameter_value'] = $this->time->dateToTimestamp($data['parameter_value']);
		}
		return parent::save($data);
	}	
	
}
