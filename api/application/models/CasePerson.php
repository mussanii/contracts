<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a contract type
 */
class CasePerson extends Toggler
{		
    private $person;
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
	    $this->resetTable();
		$this->person = Person::getInstance();
		parent::__construct();				
	}		
	
	private function resetTable()
	{
	    $this->table_name = 'cases_persons';
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
	    $person_table=$this->person->table();
	    $person_key=$this->person->primaryKey();
	    
	    $table = array_filter(explode(' ',trim($this->table_name)));
	    $table = (count($table)>0)? $table[0] : $this->table_name;
	    
	    $this->table("$table a");
	    $this->join("$person_table b"," a.$person_key = b.$person_key", " INNER ");
	    
	    $data = parent::select($criteria,$columns,$group_by,$order_by,$limit);
	    $this->table("$table");
	    return $data;
	}
	
	public function save($data)
	{   
	    $table = $this->person;
	    $pk = $this->person->primaryKey();	    
	    $data['last_updated'] = time();
	    
	    //save person or organization data here please
	    if(($records_affected = $table->save($table->sanitize($data)))>0 && (int)$table->lastAffectedId()>0)
	    {
	        //collect customer data and add other data along
	        $data[$pk] = $table->lastAffectedId();
	        $data['effective_from'] = time();
	        parent::save($this->sanitize($data));
	    }
	    
	    if($table->isError()==true)
	    {
	        $this->message($table->message());
	        $this->isError($table->isError());
	    }
	    
	    return (!$this->isError() && $records_affected>0 && (int)$this->recordsAffected()>0)? $this->recordsAffected() : 0;
	}	
	
	
}
