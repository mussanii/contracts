<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract class for a Yearly Reminder 
 */

class YearlyReminder extends MonthlyReminder implements YearlyReminderRange{

     public function __construct($data,$contract){
      parent::__construct($data,$contract);

    }

    public function sixthiethRange($id){
        if(array_key_exists('effective_to', $this->data)){
            $date_created =date("Y-m-d", strtotime("-60 days",strtotime($this->data["effective_to"])));
             $this->storeReminders($id,$date_created);

        }
        

    }
     public function fourtyFiveRange($id){
         if(array_key_exists('effective_to', $this->data)){
            $date_created =date("Y-m-d", strtotime("-45 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);
         }
        

     }
     //adapt the monthly reminder dates calculation functions here
    protected function escalateMonthly($id){
        $this->thirthiethRange($id);
        $this->fiftheenthRange($id);
        $this->tenthRange($id);
        $this->fifthRange($id);
        $this->zeroRange($id);
        $this->afterFiveRange($id);

    }
    //save all  reminder dates
    public function saveYearlyReminder($id){
        $this->sixthiethRange($id);
        $this->fourtyFiveRange($id);
        $this->escalateMonthly($id);

    }
}