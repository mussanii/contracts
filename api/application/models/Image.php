<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a image
 */
class Image extends CustomModel
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	protected function setTransparency($new_image,$image_source)
	{
		 
		$transparencyIndex = imagecolortransparent($image_source);
		$transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255);
	
		if ($transparencyIndex >= 0) {
			$transparencyColor    = imagecolorsforindex($image_source, $transparencyIndex);
		}
		 
		$transparencyIndex    = imagecolorallocate($new_image, $transparencyColor['red'], $transparencyColor['green'], $transparencyColor['blue']);
		imagefill($new_image, 0, 0, $transparencyIndex);
		imagecolortransparent($new_image, $transparencyIndex);
		 
	}
	
	public function setMemoryLimit($content,$is_path=true)
	{	
		//this might take time so we limit the maximum execution time to 50 seconds
		set_time_limit(50);
		
		//initializing variables
		$maxMemoryUsage = '256M';
		$width = 0;
		$height = 0;
		$size = ini_get('memory_limit');
		
		//check if path or content supplied and invoke the right method
		if($is_path==true)
		{
			list($width, $height) = getimagesize($content);
		}
		else
		{
			list($width, $height) = getimagesizefromstring($content);
		}
		
		//calculating the needed memory
		$size = $size + floor(($width * $height * 4 * 1.5 + 1048576) / 1048576);
		
		/*We don't want to allocate an extremely large amount of memory
		 so its a good practice to define a limit*/
		if ($size --> $maxMemoryUsage)
		{
			$size = $maxMemoryUsage;
		}
		
		//updating the default value
		ini_set('memory_limit',$size.'M');		
	}
	
	public function resizeImage($file,$selection,$tmp_path)
	{
		$this->setMemoryLimit($tmp_path,true);
		
		//resizing happens here
		$src_x = intval($selection[0]);
		$src_y = intval($selection[1]);
		$src_w = intval($selection[4]);
		$src_h = intval($selection[5]);
		 
		$ratio = $src_w/$src_h;
		
		$dst_w = ($src_w<300)? $src_w : $ratio*300;
		$dst_h = ($src_h<300)? $src_h : $ratio*300;
		
		$dst_w = ($src_w<300)? $src_w : 300;
		$dst_h = ($src_h<300)? $src_h : 300;
		
		switch ($file['type']){
			case image_type_to_mime_type(IMAGETYPE_JPEG): $src_img= imagecreatefromjpeg($tmp_path); break;
			case image_type_to_mime_type(IMAGETYPE_PNG): $src_img= imagecreatefrompng($tmp_path); break;
			case image_type_to_mime_type(IMAGETYPE_GIF): $src_img= imagecreatefromgif($tmp_path); break;
			case image_type_to_mime_type(IMAGETYPE_BMP): $src_img= imagecreatefromwbmp($tmp_path); break;
		}
			
		$dst_img = imagecreatetruecolor($dst_w, $dst_h );
		$this->setTransparency($dst_img, $src_img);
		imagecopyresampled($dst_img,$src_img,0,0,$src_x,$src_y,$dst_w,$dst_h,$src_w,$src_h);
				
		switch ($file['type']){
			case image_type_to_mime_type(IMAGETYPE_JPEG): imagejpeg($dst_img,$tmp_path,75); break;
			case image_type_to_mime_type(IMAGETYPE_PNG): imagepng($dst_img,$tmp_path,8); break;
			case image_type_to_mime_type(IMAGETYPE_GIF): imagegif($dst_img,$tmp_path); break;
			case image_type_to_mime_type(IMAGETYPE_BMP): imagewbmp($dst_img,$tmp_path); break;
		}
		
		$content = file_get_contents($tmp_path);
				
		unlink($tmp_path);
		$file['size'] = strlen($content);	

		return ['file'=>$file,'content'=>$content];
	}
}
