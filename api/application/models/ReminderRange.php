<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @ Interface for a monthly Reminder Range
 */

 interface ReminderRange{
 function thirthiethRange($id);
 function fiftheenthRange($id);
 function tenthRange($id);
 function fifthRange($id);
 function zeroRange($id);
 function afterFiveRange($id);
}