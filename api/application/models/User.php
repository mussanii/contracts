<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial
 * @version 1.0.0 
 * @abstract base class for a user
 */
class User extends BaseUser
{		

	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	/**
	 * @method isCustomer()
	 * @desc check if user is an customer/client and active
	 * @return boolean
	 */
	public function isCustomer()
	{
		$identity = $this->identity();
		$role_ids = $this->getMyRoles($identity->getId(),true);
		$exists = $this->role->exists([$this->role->primaryKey()=>$role_ids,'code'=>'CS']);
		return $exists;
	}
	
	public function getLoggedInEntity($person_id=null,$owner_id=null)
	{
		$person = Person::getInstance();
		$party = Party::getInstance();
		$customer = Customer::getInstance();
		$customer_pk = $customer->primaryKey();
	
		if($person_id)
		{
			//find if contact party by stated person id exists, then find its customer parent record
			$contact_party_id = $party->fetchColumn($party->primaryKey(),['a.'.$this->person->primaryKey()=>$person_id]);
			$customer_id = $this->filterOne([$person->primaryKey()=>$person_id],[$customer_pk]);
						
			if($contact_party_id || $customer_id)
			{
				$criteria = $contact_party_id?  ['contact_party_id'=>$contact_party_id] : [$customer_pk=>$customer_id];
				$organization = $customer->filterOne($criteria);
				if(!$organization){
					$organization = $this->organisation->selectOne($owner_id);
				}
			}
			else
			{
				$organization = $this->organisation->selectOne($owner_id);
			}
		}
		else
		{
			$organization = $this->organisation->selectOne($owner_id);
		}
	
		//if no contact person specify administrator of organisation
		if(!isset($organization['person']))
		{
			$person_id = $this->fetchColumn($this->person->primaryKey(),['is_admin'=>'Y']);
			$organization['person'] = $this->person->selectOne($person_id);
		}
		return $organization;
	}

	/**
	 * @method generateToken()
	 * @desc override of generic method
	 * @desc create token from logged user and organisation entities
	 * @return string token
	 */
	public function generateToken()
	{
		global $COMPANY_CODE;
		$user = $this->selectOne($this->getId()); 
		$owner_id = $this->organisation->fetchcolumn($this->organisation->primaryKey(),['is_owner'=>'Y']);
	
		$payload = [
				'user_id'=>$this->getId(),
				'is_super'=>$user['is_super'],
				'username'=>$user['username'],
				'full_name'=>$user['full_name'],
				'user_role'=>$this->getRoles(),
				'icon' => $user['icon'],
				'first_name' => $user['first_name'],
				'last_name' => $user['last_name'],
				'phone' => $user['phone'],				
				'primary_email' => $user['primary_email'],
				'dob' => $user['dob'],
				'gender' => $user['gender'],
				'initials' => $user['initials'],
				'city_town' => $user['city_town'],
				'country' => $user['country'],
				'role' => $this->getDefaultRole(),
				'dashboard' => $this->getDashboard($this->getDefaultRole()),
				'person_id'=>$this->getPersonId(),
				'organisation' => $this->getLoggedInEntity($this->getPersonId(),$owner_id),
				'owner' => $this->organisation->selectOne($owner_id),
				'company_code'=>$COMPANY_CODE,
				'company_id' =>$user['company_id'],
		];
		
		//Feed token Header with some expiry dates and other things
		$exp = ((time()*1000) + 10000)/1000;
		$head = ['iat'  => time(),'iss'  => $_SERVER['HTTP_HOST'],'nbf'  => time(),'exp'  => $exp];
	
		//generate token
		$token = JWT::encode($payload, $this->getTokenKey(),'HS256',md5($this->getId().time()),$head);
		
		//save token for this user
		parent::update(['session_token'=>$token],['user_id'=>$this->getId()]);
	
		return $token;
	}	
	
	public function cloneRecord($criteria,$data,$role_id,$save=true)
	{
		//get user detail
		$user_detail = $this->filterOne($criteria);
		
		unset($user_detail['id_parent']);
		unset($user_detail['active']);		
		unset($user_detail['session_token']);
		unset($user_detail['organisation']);
		unset($user_detail['small_url']);
		unset($user_detail['thumb_url']);
		unset($user_detail['large_url']);
		unset($user_detail['icon']);
		unset($user_detail['signature']);
		unset($user_detail['full_name']);
		unset($user_detail['username']);
		unset($user_detail['primary_email']);
		unset($user_detail['suspended']);
		
		$user_detail[$this->person->primaryKey()] = null;
		$user_detail[$this->primaryKey()] = null;
		$user_detail['last_affected'] = time();
		$user_detail['last_updated'] = time();
		$user_detail['effective_from'] = time();

		//suppose saving enabled, then save clone with the additional data
		if($save==true)
		{
			$user_detail = array_merge($user_detail,$data);
			$this->person->save($this->person->sanitize($user_detail));
			
			if($this->isError()==true || intval($this->person->recordsAffected())==0)
			{
				$this->isError($this->person->isError());
				$this->message($this->person->message());
			}
			else 
			{
				//save user detail -- add person id on the go
				//$user_detail['username'] = (isset($data['username']) && trim($data['username'])!='')? $data['username'] : $user_detail['first_name'].$this->randomize(5);
				$firstCharacter = substr($user_detail['firstname'], 0, 1);
				
				$user_detail['username'] = (isset($data['username']) && trim($data['username'])!='')? $data['username'] : ucfirst($firstCharacter).$user_detail['last_name'];

				$user_detail[$this->person->primaryKey()] = $this->person->lastAffectedId();
				$password = $this->getPasswordSalt($this->getResetPasswordCode($user_detail['primary_email'].time()));
				$user_detail['password'] = isset($user_detail['password'])? $user_detail['password'] : $password; 
				$user_detail['confirm_password'] = isset($user_detail['password'])? $user_detail['password'] : null;
				$this->save($this->sanitize($user_detail));
				
				//save roles for this user
				if(intval($this->recordsAffected())>0)
				{
					$user_detail[$this->primaryKey()] = $this->lastAffectedId();					
					$this->saveRoles($role_id, $this->lastAffectedId());
				}
			}
		}		
		return $user_detail;
	}
	
	public function sendResetEmail($user_id,$username)
	{
		//send email on new account created
		$u_criteria = ['username'=>$username];
		$recipient=$this->selectOne($user_id);
		$subject = "New user account at ".SITE_NAME;
		$email=$recipient['primary_email'];
		$name = $recipient['first_name'].' '.$recipient['last_name'];
	
		//save password reset code
		$reset_code = $this->getResetPasswordCode($email);
		$this->update(['reset_password'=>$this->getPasswordSalt($reset_code)],$u_criteria);
		$body = $this->getEmailNewUserAccount($email, $name, $reset_code);
	
		//send to contact person
		$this->mailer()->setSender();
		$this->mailer()->setRecipient($email);
		$this->mailer()->sendEmail($body, $subject,true);
	}
	
}
