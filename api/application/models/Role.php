<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a role
 */
class Role extends CustomModel
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='roles';
		parent::__construct();				
	}		
	
	public function save($data){
		$success = parent::save($data);		
		$object = Object::getInstance();
		$pk = $this->primaryKey();
		
		/* we do this for new records. check if new role then also link with dashboard object */
		if((array_key_exists($pk, $data)==false || (int)$data[$pk]==0) && (int)$this->lastAffectedId()>0)
		{
			$success = $object->addDashboard($this->lastAffectedId());
			if($object->isError()==true)
			{
				$this->isError($object->isError());
				$this->message($object->message());	
			}
		}
		
		return $success;
	}
}
