<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a contract cron job
 */
class ContractCron extends CustomModel
{			
	private static $_instance=null;
		
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
		
	public function __construct()
	{
		global $DB_SOURCE;
		global $DB_USER;
		global $DB_PASSWORD;
				
		$this->adapter = new Adapter($DB_SOURCE, $DB_USER, $DB_PASSWORD);
		$this->adapter->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->adapter->exec("SET GLOBAL event_scheduler = ON");
	}
	
	private function cleanPeriodType($period_type)
	{
		$period_type = strtolower($period_type);
		$pos = strrpos($period_type, 's');
		$period_type = (substr_compare($period_type, 's', -strlen('s'))===0)? substr($period_type, 0,$pos) : $period_type;
		$period_type = strtoupper(($period_type));
		return $period_type;
	}
	
	public function executeStatus($event_name,$code,$cycle,$criteria=[], $period_type='DAY',$period_length='1')
	{	
		global $identity;
		
		$period_type = $this->cleanPeriodType($period_type);
		$effective_date = " UNIX_TIMESTAMP(DATE_ADD(NOW(), INTERVAL $period_length $period_type)) ";
		
		$criteria = count($criteria)>0? " AND ".implode('AND', $criteria) : "";
		$organisation = Organisation::getInstance();
		$oid = $identity->getPayload()['organisation'][$organisation->primaryKey()];
		
		$sql = " CREATE EVENT 
				`$event_name` 
				ON SCHEDULE EVERY  $period_length $period_type
				DO
				 BEGIN
				 	/*-- Get Status Id */
					SET @status_id = (SELECT status_id FROM contracts_status WHERE code='$code' AND organisation_id=$oid);
					
					/*-- Get Status Id For Expired Contract */
					SET @status_id_expired = (SELECT status_id FROM contracts_status WHERE code='EX' AND organisation_id=$oid);
					
					/*-- Set contract flags as per required status */
					UPDATE contracts SET status_id = @status_id, cycle_no = $cycle 
					WHERE effective_to IS NOT NULL 
					AND $effective_date>=effective_to 
					$criteria;
					
					
					/*-- Remember to flag expired contracts */
					UPDATE contracts SET status_id = @status_id_expired, cycle_no = 7 
					WHERE effective_to IS NOT NULL 
					AND UNIX_TIMESTAMP()>effective_to
					$criteria;
										
		         END;";		
		
		$this->adapter->exec("DROP EVENT IF EXISTS `$event_name`");
		$this->adapter->exec($sql);
	}
}
