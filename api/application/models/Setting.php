<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a setting
 */
class Setting extends CustomModel
{		
	//associations/compositions
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
			
	public function __construct()
	{		
		$this->table_name='settings';
		parent::__construct();				
	}	
	
	public function save($data)
	{
		$result = parent::save($data);
		
		//configure cron jobs to execute after a given units of time
		if(intval($this->recordsAffected())>0 && trim($data['units'])!='')
		{
			$cron = ContractCron::getInstance();
			$units = $data['units'];
			$value = $data['value'];
			
			switch ($data['code'])
			{
				case 'CCE' : $cron->executeStatus('Check_Pending_Expiry','PE','6',["is_renewable='N'"],$units,$value); break;
				case 'CCR' : $cron->executeStatus('Check_Pending_Renewal','RN','6',["is_renewable='Y'"],$units,$value); break;
			}
			
		}
		
		return $result;
	}
	
}
