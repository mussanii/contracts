<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @base class for a monthly Reminder 
 */



class WorkflowMonthlyReminder implements WorkflowReminderRange  {
    protected $data;
    protected $date_created;
    protected $process;
    public function __construct($data,$process){
        $this->process=$process;
        $this->data=$data;
       //$due_date= $data["steps"][0]['due_date'];

    }

    function thirthiethRange($id){
        $date_created =date("Y-m-d", strtotime("-30 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);

    }
    function fiftheenthRange($id){
        $date_created =date("Y-m-d", strtotime("-15 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);

    }
    function tenthRange($id){
        $date_created = date("Y-m-d", strtotime("-10 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);
    }
    function fifthRange($id){
        $date_created = date("Y-m-d", strtotime("-5 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);
    }
    function zeroRange($id){
        $date_created =date("Y-m-d", strtotime("-0 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);
    }
    function afterFiveRange($id){
        $date_created = date("Y-m-d", strtotime("+5 days",strtotime($this->resolveDueDate())));
        $this->storeReminders($id,$date_created);
    }

  protected  function storeReminders($id,$date_created){
        $reminders =array($this->data);
        
        foreach($reminders as $row){
            $row = [$this->process->primaryKey()=>$id];
            $row = $this->process->getReminder()->sanitize($row);
            $row['reminder_date']=$date_created;
                
        //save the reminders
        $this->process->reminder->save($row);
        //if error encountered break and throw error
        if($this->process->reminder->isError()==true){
            $this->message($this->process->getReminder()->message());
            $this->RollBack();
            break;
        }

       }
    }
    protected function resolveDueDate(){
       return $this->data["steps"][0]['due_date'];
     

    }
}