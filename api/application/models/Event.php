<?php
/*
 * 
 * @author Kevin Musanii
 * @copyright 2021 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for an event
 */
class Event extends OrganisationObject
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='events';
		parent::__construct();				
	}		
	
	public function formatDates($data)
	{
	    if(isset($data['effective_from']))
	    {
	        $data['effective_from'] =  strtotime($data['effective_from']);
	    }
	    if(isset($data['effective_to']))
	    {
	        $data['effective_to'] =  strtotime($data['effective_to']);
	    }
		
	    return $data;
	}
	
	public function update($data,$criteria=null)
	{
	    return parent::update($this->formatDates($data),$criteria);
	}	

	/*
	 * @method save()
	 * @desc save event detail
	 * @param array $data contains event details
	 */
	public function save($data)
	{
		// die(var_dump($data));
	    $identity = $this->identity();
	    if(array_key_exists($this->primaryKey(), $data)==false || (int)$data[$this->primaryKey()]==0)
	    {
	        $data['owner_id'] = $identity->getId();
	        $data['color'] = isset($data['color']) && $data['color']? $data['color'] : $this->randomColor();
	        $data = $this->formatDates($data);
	    }
	    return parent::save($data);
	}
	
}
