<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for emails
 */
class Mailer extends CustomModel
{	
	private static $_instance=null;
	private $box_type;
	private $mail_attachment;
	
	private $sender;	
	private $recipient;
	private $cc = [];
	private $bcc = [];
	
	private $files = [];
	
	/*Singlerecipientn Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function setAttachments($files=[])
	{
		return $this->files = is_array($files)==true? $files : [];
	}
	
	public function setSender($email=null,$name=null)
	{
		global $smtp_config;
		
		$email = $email? $email : $smtp_config['FROM_EMAIL'];
		$name = $name? $name : $smtp_config['FROM_NAME'];
		
		$this->sender = ['email'=>$email,'name'=>$name];	
		return $this->sender;
	}
	
	public function setRecipient($recipient=[])
	{
		$this->recipient = (is_array($recipient)==true)? array_filter($recipient) : (($recipient)? [$recipient] : []);
		return $this->recipient;
	}
	
	public function setCc($cc=[])
	{
		$this->cc = (is_array($cc)==true)? array_filter($cc) : (($cc)? [$cc] : []);
		return $this->cc;
	}
	
	public function setBcc($bcc=[])
	{
		$this->bcc = (is_array($bcc)==true)? array_filter($bcc) : (($bcc)? [$bcc] : []);	
		return $this->bcc;
	}
	
	public function __construct()
	{
		$this->box_type = MailBoxe::getInstance();
		$this->mail_attachment = MailAttachment::getInstance();
		$this->table_name = 'mail_messages';
		parent::__construct();
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$identity = $this->identity();
		$person_pk = Person::getInstance()->primaryKey();
		$person_id = $identity? $identity->getPersonId() : null;
		
		/* Fetch emails which apply to this user. If admin then fetch evrything */
		if($identity && $identity->isAdmin()!==true)
		{
			$email = Person::getInstance()->fetchColumn('primary_email',[$person_pk=>$person_id]);
				
			$or_predicates = [
					new Equal('creator_usr_id', $this->identity()->getId(),'key1'), //user is creator of mail
					new Equal('recipient',$email, 'key2'),     // user found in recipient column
					new Like('sender','%'.$email.'%', 'key3'), // user found in sender column
					new Like('cc','%'.$email.'%', 'key4'),     // user found in cc column
					new Like('bcc','%'.$email.'%', 'key5'),    // user found in bcc column
			];
			
			$criteria[] = new NestedOr($or_predicates);
		}
		
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
		$box_pk = $this->box_type->primaryKey();
		
		for($i=0;$i<count($data);$i++)
		{
			$row=$data[$i];
			if(array_key_exists('body', $row)==true)
			{
				$row['body'] = $row['body'];
			}
			if(array_key_exists('subject', $row)==true)
			{
				$row['subject'] = $row['subject'];
			}
			if(array_key_exists('sender', $row)==true)
			{
				$row['from'] = $this->splitEmail($row['sender']);
			}
			if(array_key_exists('recipient', $row)==true)
			{
				$row['to'] = $this->splitEmail($row['recipient']);
			}
			if(array_key_exists('action_time', $row)==true)
			{
				$row['time_ago'] = $this->time->timeAgo($row['action_time']);
			}
			if(array_key_exists($box_pk, $row)==true)
			{
				$row = array_merge($row,$this->box_type->selectOne($row[$box_pk]));
			}
			$data[$i] = $row;
		}
	
		return $data;
	}
	
	public function updateQueue($email_id=null,$subject,$body,$box_type=null)
	{
		global $identity;
		
		if($this->exists([$this->primaryKey()=>$email_id])==false)
		{
			$owner_id = is_object($identity)==true? $identity->getId() : 1;
			$data = [
					'subject'=>$subject,
					'body'=>$body,
					'sender'=> $this->mergeEmail($this->sender),
					'recipient'=> implode(';',(array)$this->recipient),
					'cc'=> implode(';',(array)$this->cc),
					'bcc'=> implode(';',(array)$this->bcc),
					'sender_usr_id' => $owner_id,
					'action_time' => time(),
					'creator_usr_id' => $owner_id,
					'owner_id' => $owner_id,
					'created_at' => time(),
					$this->box_type->primaryKey() => $box_type
			];
				
			if(PRIORITY_QUEUING==true) $data['priority_level'] = 1;	
			parent::save($data);
			
			if($this->recordsAffected()){
				foreach($this->files as $attach)
				{
					$mail_data = $this->mail_attachment->sanitize($attach);
					$mail_data[$this->primaryKey()] = $this->lastAffectedId();
					$mail_data[$this->mail_attachment->primaryKey()] = null;
					$this->mail_attachment->save($mail_data);		
				}			
			}
		}
		else
		{
			$data = ['sender_usr_id' => $this->identity()->getId(),'action_time' => time()];
			parent::update($data,[$this->primaryKey()=>$email_id]);
		}
	}
	
	private function addAttachments($mailer)
	{
		foreach ($this->files as $row)
		{
			$mailer->AddStringAttachment($row['content'],$row['file_name'],'base64', $row['mime_type']);
		}	
		return $mailer;	
	}
	
	private function setServer(PHPMailer $mailer)
	{
		global $smtp_config;
		
		/*Set up Smtp Server here */
		$mailer->Port = SMTP_PORT;
			
		$mailer->Host = $smtp_config['SMTP_SERVER'];
			
		$mailer->SMTPAuth = SMTP_AUTHED; 
		
		$mailer->Timeout = 60;
		
		$mailer->Username = $smtp_config['SMTP_USERNAME'];
			
		$mailer->Password = $smtp_config['SMTP_PASSWORD'];
			
		$mailer->CharSet = 'utf-8';		

		/* set mailer recipient use smtp by default */
		$mailer->IsSMTP();			
	}
	
	private function setRecipients(PHPMailer $mailer)
	{
		/*Set recipients here*/		
		foreach ((array)$this->recipient as $recipient)
		{
			$a = $this->splitEmail($recipient);
			$mailer->AddAddress($a['email'],$a['name']);
		}
		//add cc here
		foreach ((array)$this->cc as $recipient)
		{
			$a = $this->splitEmail($recipient);
			$mailer->AddCC($a['email'],$a['name']);
		}
		//add bcc here
		foreach ((array)$this->bcc as $recipient)
		{
			$a = $this->splitEmail($recipient);
			$mailer->AddBCC($a['email'],$a['name']);
		}
		/*Set recipients here*/
	}
	
	private function setBody(PHPMailer $mailer,$body)
	{	
		$mailer->IsHTML(true);
		
		$mailer->AddEmbeddedImage(MAIN_ROOT.PRODUCT_LOGO, 'logoProduct','ProductLogo.png','base64','image/png');
		$mailer->AddEmbeddedImage(MAIN_ROOT.COMPANY_LOGO, 'logoCompany','VendorLogo.png','base64','image/png');
			
		$mailer->AltBody = $body;
		
		$mailer->Body = '<table width="100%">
				        <tr>
    					   <td width="100%"><img src="cid:logoProduct" height="60" /></td>    					    					
    					</tr>
    					<tr><td width="100%">'.$body.'</td></tr>
    					<tr>
    					   <td width="100%"><h3> Powered By </h3></td>
    					<tr/>
    					<tr>
    					   <td width="100%"><img src="cid:logoCompany" height="60" /></td>
    					<tr/>
    					</table>';
		
	}
	
	public function sendEmail($body,$subject,$save_copy=false){
		
		ini_set('max_execution_time', 0);
		
		$mailer = new PHPMailer();	

		//set options
		$mailer->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );
		
		$mailer->Subject = $subject;
		$mailer->From = $this->sender['email'];
		$mailer->FromName = $this->sender['name'];
		
		$this->setServer($mailer);		 
		$this->setRecipients($mailer);
		$this->setBody($mailer, $body);
		$this->addAttachments($mailer);	
			
		$is_sent = (PRIORITY_QUEUING!==true || $this->identity==null)? ($mailer->Send()==true? 'Y' : 'N') : 'N';
		
		if($mailer->IsError()==true)
		{
			$this->isError(true);
			$this->message($mailer->ErrorInfo);
		}
		
		/*if copies can be saved, then call this method recipient add an email recipient queue */
		if($save_copy==true)
		{
			$code = ($is_sent=='Y')? 'ST' : 'OB';
			$box_pk = $this->box_type->fetchColumn($this->box_type->primaryKey(),['code'=>$code]);			
			$this->updateQueue(null,$subject, $body,$box_pk);
		}
		return ($is_sent=='Y')? true : false;
	}
	
	public function splitEmail($source)
	{
		$source = trim($source);
			
		$pos1 = strpos($source, '<');
		$pos2 = strrpos($source, '>');
			
		$name = ($pos1>0)? trim(substr($source,0,$pos1)) : $source;
		$email = ($pos1>1 && $pos2>2)? trim(substr($source,$pos1+1,$pos2-$pos1-1))  : $source;
			
		return ['name'=>$name,'email'=>$email];
	}
	
	public function mergeEmail($source)
	{
		$name = isset($source['name'])? $source['name'] : '';
		$email = isset($source['email'])? $source['email'] : '';
		return $name.'<'.$email.'>';
	}
}
