<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a contract document
 */
class ContractDocument extends Document
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='contracts_documents';
		parent::__construct();	
	}
	
	public function save($data)
	{
		

		$contract = Contract::getInstance();
		$this->startTransaction();
		
		/* if new record, then add some keys */
		if(array_key_exists($this->primaryKey(), $data)==false || intval($data[$this->primaryKey()])==0){
			$data['active'] = 'Y';
			$data['effective_from'] = time();
		}
		
		/* flag other documents to 'N' since we can only maintain one main document */
		if(isset($data[$contract->primaryKey()]) && isset($data['main']) && $data['main']=='Y')
		{
			parent::update(['main'=>'N'],[$contract->primaryKey()=>$data[$contract->primaryKey()]]);
		}
		
		$results = parent::save($data);
		$this->endTransaction();
		return $results;
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		global $access_token;
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
		$data = $data? $data : [];
		for($i=0;$i<count($data);$i++)
		{
			$row = $data[$i];
			if(array_key_exists($this->primaryKey(), $row)==true)
			{
				$row['download_path'] = API_ROOT.'/contract-documents/download/'.$row[$this->primaryKey()].'?access_token='.$access_token;
			}
			$data[$i] = $row;
		}
		return $data;
	}
}
