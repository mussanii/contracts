<?php
class Notification extends CustomModel
{

	private $box_type;
	private $attachment;
	protected $settings;
	protected $person;
	private static $_instance=null;

	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}

	public function __construct()
	{
		$this->table_name = 'notifications';
		parent::__construct();
		$this->box_type = MailBoxe::getInstance();
		$this->setting = Setting::getInstance();
		$this->person = Person::getInstance();
		$this->role = Role::getInstance();
		$this->attachment = NotificationAttachment::getInstance();
	}

	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$user = User::getInstance();
		$device = PersonDevice::getInstance();		
		$identity = $this->identity();
		
		$role_id = $identity->getMyRoles($identity->getId(),true);
		$criteria = $criteria? $criteria : [];

		/* if not an administrator query all notifications only where this user is a sender/recipient */
		if($this->identity()->isAdmin()!==true)
		{
			$device_snos = $device->fetchColumn('device_sno',[$this->person->primaryKey()=>$identity->getPersonId()],true);
			
			$or_predicates = [new Equal('creator_usr_id', $this->identity()->getId(),'creator_usr_id')];
			foreach ($device_snos as $key=>$device_sno){
				$or_predicates[] = new Like('recipient','%'.$device_sno.'%', 'recipient'.$key);
			}
				
			$criteria[] = new NestedOr($or_predicates);					
		}
		
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$data = parent::select($criteria,$columns,$group_by,$order_by,$limit,true);
		$box_pk = $this->box_type->primaryKey();
		$person_pk = $this->person->primaryKey();
		$attachment_pk = $this->attachment->primaryKey();
		
		if(($columns && count($columns)>=2) || !$columns)
		{
			for($i=0;$i<count($data);$i++)
			{
				$row=$data[$i];
				
				if(array_key_exists('created_at', $row)==true)
				{
					$row['time_ago'] = $this->time()->timeAgo($row['created_at']);
					$row['title'] = $row['title'];
					$row['message'] = $row['message'];
				}
				
				if(array_key_exists($box_pk, $row)==true)
				{
					$row = array_merge($row,$this->box_type->selectOne($row[$box_pk]));
				}
				
				/* populate documents for each notification record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$columns=[$attachment_pk,'file_type','file_name','file_size','mime_type'];	
					$row['attachments'] = (array)$this->attachment->select([$this->primaryKey()=>$row[$this->primaryKey()]],$columns);
				}
				
				if(array_key_exists('recipient', $row)==true){
					$to = isset($row['recipient'])? explode(';',$row['recipient']) : null;
					$person_id = $device->fetchColumn($person_pk,[new Equal('device_sno',$to,'device_sno')]);
					
					$column = $this->concat(['first_name','last_name'],' ').' AS full_name';
					$names = $this->person->fetchColumn($column,[$person_pk=>$person_id],true,'full_name');
					
					$row['to'] = (count($names)>0? implode(',',$names) : 'System Admin');
				}
				
				if(array_key_exists('creator_usr_id', $row)==true){
					$person_id = $user->fetchColumn($person_pk,[$user->primaryKey()=>$row['creator_usr_id']]);
					$row['from'] = $this->person->fetchColumn($column,[$person_pk=>$person_id],false,'full_name');			
					$row['person_id'] = $person_id;
				}
				$data[$i] = $row;
			}
		}
		return $data;
	}

	public function save($data)
	{
		$identity = $this->identity();
		$box_pk = $this->box_type->primaryKey();
		
		if(array_key_exists($this->prkey, $data)==false || intval($data[$this->prkey])==0)
		{
			$data['creator_usr_id'] = $identity->getId();
			$expiry_ts = intval($this->setting->fetchColumn('value',['code'=>'NET']))*24*60*60;
			$data['created_at'] = time();
			$data['title'] = htmlentities($data['title']);
			$data['message'] = htmlentities($data['message']);
			$data['expires_at'] = $expiry_ts>0? $data['created_at'] + $expiry_ts : null;
			
			/* save notification as draft in case no storage box defined */
			$box_id = $this->box_type->fetchColumn($box_pk,['code'=>'DF']);
			$data[$box_pk] = (!isset($data[$box_pk]) || intval($data[$box_pk])==0)? $box_id: $data[$box_pk];
			
		}
		return parent::save($data);
	}
	
	public function saveAttachments($id,$files=[],$mime=null,$type=null)
	{
		$files = is_array($files)? $files : [];
		
		foreach ($files as $key=>$file)
		{
			if(!isset($file['tmp_name']) || trim($file['tmp_name'])=='') continue;
			
			$content = file_get_contents($file[ 'tmp_name' ]);
			
			$data= [
					'content' => $content,
					'file_name' => $file['name'],
					'file_size' => $file['size'],
					$this->primaryKey() => $id,
					'file_type' => $type,
					'mime_type' => $mime? $mime : $file['type'],
					'effective_from' => time()
			];
			
			NotificationAttachment::getInstance()->save($data);
		}
		
	}
	
	/**
	 * @method registerPushDevice()
	 * @desc register device against a user/person
	 * @param string $push_device_id
	 * @param integer $person_id
	 */
	public function registerPushDevice($data,$person_id)
	{
		$person = Person::getInstance();
		$device = PersonDevice::getInstance();
		
		/* prepare data and sanitize criteria */
		$device_sno = isset($data['device_sno'])? $data['device_sno'] : null;
		$criteria = [$person->primaryKey()=>$person_id,'device_sno'=>$device_sno];
		$data = $device->sanitize(array_merge($data,$criteria));
		unset($data[$device->primaryKey()]);
		
		/* check if device exists update it else add as new device */
		if($device->exists($criteria))
		{			
			$device->update($data,$criteria);			
		}
		else
		{
			$device->save($data);
		}
		
		/* update messaage and error flags acccordingly using $device instance */
		$this->message($device->message());
		$this->isError($device->isError());
		$this->lastAffectedId($device->lastAffectedId());
		$this->recordsAffected($device->recordsAffected());
	}
	
	/**
	 * @method pushNotification()
	 * @desc push a notification to a mobile device. It fetches gcm device id for this person and 
	 * sends a push notification to the device
	 * @param integer|array $person_id
	 * @param array $data data options containing message,title,icon e.g
	 * 	[
			'message' 	=> 'here is a message. message',
			'title'		=> 'This is a title. title',
			'icon'	=> 'myicon', //e.g. http://wasiliana.stl-horizon.com/resources/company/logo-64.png
			'sound'		=> 'mysound', //e.g. 
	 	]
	 */
	public function pushNotification($criteria,$data,$auto_save=true)
	{
		global $global;
		global $push_notifier;
		
		/* gather device gcm registration ids */
		$device = PersonDevice::getInstance();
		$person = Person::getInstance();
		$person_pk = $person->primaryKey();
		if(!array_key_exists('device_sno',$criteria) && count($criteria)==1)
		{
			$criteria = [$person_pk=>(array)$person->fetchColumn($person_pk,$criteria)];
		}
		$push_device_id = array_unique((array)$device->fetchColumn('push_device_id',$criteria));
		
		/* send push notification here */
		$headers = [
			'Authorization: key=' . $push_notifier['PUSH_APIKEY'],
			'Content-Type: application/json'				
		];
		
		/* add default icons and sounds */
		$data['icon'] = isset($data['icon'])? $data['icon'] : WEB_ROOT.PRODUCT_LOGO;
		$data['sound'] = isset($data['sound'])? $data['sound'] : 'default';		
		$data['person_id'] = $this->identity()->getPersonId();
				
		/* compose response body and send */
		$body = json_encode(['registration_ids' => array_values(array_filter($push_device_id)),'data' => $data]);		
		$response = $global->sendCurlRequest($push_notifier['PUSH_ENDPOINT'],$body,$headers);
		
		/* save notification message */
		if($auto_save==true)
		{
			$code = (isset($response['success']) && $response['success']==1)?  'ST' : 'OB';
			$box_id = $this->box_type->fetchColumn($this->box_type->primaryKey(),['code'=>$code]);
			$data = $this->sanitize(array_merge($data,[$this->box_type->primaryKey()=>$box_id]));
			$this->save($data);
			$response['id'] = $this->lastAffectedId();
		}
				
		return $response;
	}
	
	/**
	 * @desc split recipient string to clearly show ids and names
	 * @param string $recipients 
	 * @param boolean $ids_only flag to allow whole 2-d resultset or just ids
	 * @return array 1 or 2 array depending on whether we allow ids only 
	 */
	public function extractRecipient($recipients,$ids_only=false)
	{
		$results = [];
		$recipients = array_filter(explode(';', $recipients));
		
		foreach($recipients as $recipient)
		{
			$source = trim($recipient);
				
			$pos1 = strpos($source, '<');
			$pos2 = strrpos($source, '>');
				
			$id = ($pos1>0)? trim(substr($source,0,$pos1)) : $source;
			$name = ($pos1>1 && $pos2>2)? trim(substr($source,$pos1+1,$pos2-$pos1-1))  : $source;
				
			$results[] = ['id'=>$id,'name'=>$name];
		}		
		
		$results = ($ids_only==true)? $this->getArrayMap($results,'id',true) : $results;
		return $results;
	}
}
