<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for an organisation
 */
class Organisation extends CustomModel
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	//associations/compositions		
	public function __construct()
	{		
		$this->table_name='organisations';
		parent::__construct();		
	}		
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
			
		//columns exit and at least 2 or not specified at all
		if(($columns && count($columns)>=2) || !$columns)
		{
			for($i=0;$i<count($data);$i++)
			{
				$row=$data[$i];
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['icon'] = API_ROOT.'/organisation-images/download/'.$row[$this->primaryKey()].'?access_token=';
				}
				$data[$i] = $row;
			}
		}
		return $data;
	}
}
