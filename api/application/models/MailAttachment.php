<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a mail box type
 */
class MailAttachment extends CustomModel
{		
	//associations/compositions
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
			
	public function __construct()
	{		
		$this->table_name='mail_attachments';
		parent::__construct();				
	}	
}
