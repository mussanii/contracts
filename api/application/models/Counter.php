<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @final base class for a counter
 */
class Counter extends CustomModel
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='counters';
		parent::__construct();		
	}	

	public function nextCounter($type,$permanent=true)
	{
		$counter = $this->filterOne(['code'=>$type]);
		$last_count = $counter['last_count'] + 1;
		$formatted_count = $counter['prefix'].str_pad($last_count, $counter['min_length'],$counter['pad_string'],STR_PAD_LEFT);		
		if($permanent==true){
			$this->update(['last_count'=>$last_count],['code'=>$type]);
		}
		return $formatted_count;
	}
	
}
