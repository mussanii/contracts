<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @co-author  Kevin K. Musanii
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a contract
 */
class Contract extends OrganisationObject
{
	private static $_instance=null;
	private $role;
	private $user_role;
	private $user;
	private $contract_status;
	private $contract_type;
	private $contract_area;
	private $contract_party;
	private $contract_user;
	private $contract_location;
	private $reminder_frequency;
	private $contract_reminder;
	private $organisation;
	private $organisation_unit;
	private $person;
	private $counter;
	private $document;
	private $experience;
	private $enable_mail = true;
	private $frequency_unit;
	public $reminder;
	private $date_created;
	private $companie;

	public function __construct()
	{		
		$this->table_name='contracts';
		parent::__construct();
		
		//capture instances of object associations
		$this->contract_area = ContractArea::getInstance();
		$this->contract_type = ContractType::getInstance();		
		$this->contract_status = ContractStatuse::getInstance();		
		$this->organisation_unit = OrganisationUnit::getInstance();
		$this->document = ContractDocument::getInstance();
		$this->organisation = Organisation::getInstance();
		$this->contract_party = ContractParty::getInstance();
		$this->contract_user = ContractUser::getInstance();
		$this->contract_location = ContractLocation::getInstance();
		$this->reminder_frequency = ReminderFrequency::getInstance();
		$this->frequency_unit = FrequencyUnit::getInstance();
		$this->reminder = Reminder::getInstance();
		$this->companie=Companie::getInstance();
		

		
		$this->counter = Counter::getInstance();
		$this->user = User::getInstance();
		$this->role = Role::getInstance();
		$this->user_role = UserRole::getInstance();
		$this->person = Person::getInstance();
		$this->experience = ContractExperience::getInstance();
		
	}


	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}

	/**
	 * 
	 * Disable the cloning of this class
	 * 
	 * @return void
	 */

	final public function __clone()
	{
		throw new Exception('Feature Disabled');
	}


	/**
	 * 
	 * Disable the wake up of this class
	 * 
	 * @return void
	 * 
	 */

	final public function __wakeup()
	{
		throw new Exception('Feature Disabled.');
	}


	

	
	public function enableMailer($enable_mail=true)
	{
		$this->enable_mail = $enable_mail;
		return $this->enable_mail;


	}
	
	/*
	 * @method select()
	 * @desc fetch detailed contracts including status, department, contract type and contract area
	 * @param array|NULL $criteria 
	 * @param array|NULL $columns 
	 * @return array $data;
	 */
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		//capture foreign keys of each object
		$unit_pk = $this->organisation_unit->primaryKey();
		$status_pk = $this->contract_status->primaryKey();
		$location_pk = $this->contract_location->primaryKey();
        $frequency_pk = $this->reminder_frequency->primaryKey();
		$type_pk = $this->contract_type->primaryKey();
		$area_pk = $this->contract_area->primaryKey();
		$experience_pk = $this->experience->primaryKey();
		$document_pk = $this->document->primaryKey();
		$user_pk = $this->user->primaryKey();
		$user_id = $this->identity()->getId();

	
		
		//if not an administrator query contracts only where this party exists
		if($this->identity()->isAdmin()!==true )
		{
			
			$contract_id = (array)$this->contract_user->fetchColumn($this->primaryKey(),[$user_pk=>$user_id]);
			$criteria[] = new Equal($this->primaryKey(),$contract_id,$this->primaryKey());
		;
			
		}
		
		//get original array source of parent
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
		
		//columns exists and at least 2 or not specified at all
		/* populate additional fields */
		if(!$columns || count($columns)>2)
		{
		
			for ($i=0;$i<count($data);$i++)
			{
				$row = $data[$i];
				
				
				
				/* populate contract duration here */
				if(array_key_exists('effective_from', $row)==true && array_key_exists('effective_to', $row)==true){
					$row['duration'] = $this->time()->timesAgo($row['effective_to'],$row['effective_from'],'');
					
					
				}
				if( array_key_exists('effective_to', $row)==true){
					$row['date_expiry'] = date('d/M/Y',$row['effective_to']);
					
					
				}
				
				/* populate functional unit info for each record */
				if(array_key_exists($unit_pk, $row)==true){
					$row['unit'] = (array)$this->organisation_unit->filterOne([$unit_pk=>$row[$unit_pk]]);
				}
				
				/* populate contract status info for each record */
				if(array_key_exists($status_pk, $row)==true){
					$row['status'] = (array)$this->contract_status->filterOne([$status_pk=>$row[$status_pk]]);
				}
				
				/* populate contract status info for each record */
				if(array_key_exists($location_pk, $row)==true){
				    $row['location'] = (array)$this->contract_location->filterOne([$location_pk=>$row[$location_pk]]);
				}
				
                /* populate reminder frequencies info for each record */
				if(array_key_exists($frequency_pk, $row)==true){
				    $row['frequency'] = (array)$this->reminder_frequency->filterOne([$frequency_pk=>$row[$frequency_pk]]);
				}
				
				/* populate contract type info for each record */
				if(array_key_exists($type_pk, $row)==true){
					$row['type'] = (array)$this->contract_type->filterOne([$type_pk=>$row[$type_pk]]);
				}
				
				/* populate contract area info for each record */
				if(array_key_exists($area_pk, $row)==true){
					$row['area'] = (array)$this->contract_area->filterOne([$area_pk=>$row[$area_pk]]);
				}
				
				/* populate contract experience info for each record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['experience'] = (array)$this->experience->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
				}
				
				/* populate documents for each contract record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$columns=[$document_pk,'file_type','file_name','file_size','mime_type','main','effective_from'];
					$document_criteria = ['main'=>'Y',$this->primaryKey()=>$row[$this->primaryKey()]];
					$document_id = $this->document->fetchColumn($this->document->primaryKey(),$document_criteria);
					$row['document_url'] = API_ROOT.'/contract-documents/download/'.$document_id.'?access_token=';
					$row['documents'] = (array)$this->document->select([$this->primaryKey()=>$row[$this->primaryKey()]],$columns);
					
				}
				
				/* populate contract parties info for each record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['parties'] = (array)$this->contract_party->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
					$parties_names = $this->getArrayMap($row['parties'], 'full_name');
					$row['parties_names'] = implode(',', $parties_names);
					
				}
				
				/* populate contract persons info for each record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['users'] = (array)$this->contract_user->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
				}
				
				$data[$i] = $row;				
			}
		}
		
		
		return $data;
	}
	
	public function update($data,$criteria=null)
	{
		if(isset($data['effective_from']))
		{
			$data['effective_from'] = $this->time->dateToTimestamp($data['effective_from']);
		}
		if(isset($data['effective_to']))
		{
			$data['effective_to'] = $this->time->dateToTimestamp($data['effective_to']);
		}
		return parent::update($data,$criteria);	
	}	
		
	/*
	 * @method save()
	 * @desc save workflow details and their steps
	 * @param array $data contains workflow details and details for step
	 */
	public function save($data)
	{
		$identity = $this->identity();
		
		//capture some keys here
		$contract_user_pk = $this->contract_user->primaryKey();
		
		
		//start transaction
		$this->startTransaction();
	
		//clean parent data
		$parent = $this->sanitize($data);
		
		/* we do this for new records */
		if(array_key_exists($this->primaryKey(), $parent)==false || (int)$parent[$this->primaryKey()]==0){
			//obtain status id for new record
			$status_key = $this->contract_status->primaryKey();		
			$organisation_pk = $this->organisation->primaryKey();
			$payload = $identity->getPayload();

			if($parent['is_renewable'] =='Y' ){

				 $parent['is_renewable'] ='Y';

			}
			else if($parent['is_renewable'] =='N'){

				 $parent['is_renewable'] ='N';

			}else{
				
				$parent['is_renewable'] ='O';
			}
			
			$parent['contract_no'] = trim($parent['contract_no'])!=''? $parent['contract_no'] : $this->counter->nextCounter('CN');
			$parent[$organisation_pk] = $payload['organisation'][$organisation_pk];
			$code = isset($parent['cycle_no']) && $parent['cycle_no']==6? 'AC' : 'PD';
			$parent[$status_key] = $this->contract_status->fetchColumn($status_key,['code'=>$code]);
			$parent['owner_id'] = $identity->getId();
			$parent['effective_from']= $this->time->dateToTimestamp($data['effective_from']);

			if( is_array($data) && array_key_exists('effective_to',$data)){
				if($this->time->dateToTimestamp($data['effective_to'])){
					$parent['effective_to'] = $this->time->dateToTimestamp($data['effective_to']);
				}else{
					$parent['effective_to'] = null;
				}
			}else{
				$parent['effective_to'] = $this->time->dateToTimestamp(0);
			}
			
			$parent['submitted_on'] = time();
			
		}
		else{
			 //clean parent data. we do this for updates			
			$data = array_merge((array)$this->selectOne($data[$this->primaryKey()]),$data);
			$parent = $this->sanitize($data);
			$parent['last_updated'] = time();
		}		
		
		
		//save the parent record and retrieve process id
		parent::save($parent);
		
		//save secondary records each as a record		
		if($this->recordsAffected()>0)
		{
			$id = $this->lastAffectedId();
			
			//save contract parties
			if($this->isError()!==true)
				$this->saveParties($data, $id);

           //save reminders
				if($this->isError()!==true)
				$this->saveReminders($data, $id);
			
			
			//save contract persons/users
			if($this->isError()!==true)
				$this->saveUsers($data, $id);


		}
				
		/* if NO errors encountered commit transaction*/
		if($this->isError()!==true)
		{
			$this->endTransaction();	
			if($this->enable_mail==true) $this->sendMail($parent,$data);
		}
	
		//finally return no.of records affected
		return ($this->isError()!==true)? $this->recordsAffected() : 0;
	}
	

	/*
	 * @desc save contract parties
	 * @param string $data
	 */
	private function saveParties($data,$id)
	{
		$organisation_pk = $this->organisation->primaryKey();
		
		$parties = isset($data['parties'])? $data['parties'] : [];
		foreach ($parties as $row)
		{
			$row = [$this->primaryKey()=>$id,$organisation_pk=>$row[$organisation_pk]];
			$row = $this->contract_party->sanitize($row);
			$this->contract_party->save($row);
			
			//if error encountered break and throw error
			if($this->contract_party->isError()==true){
				$this->message($this->contract_party->message());
				$this->RollBack();
				break;
			}
		}
	}
/*
	 /*
	 * @method save()
	 * @desc save Reminder dates 
	 * @param array $data contains contract id, and reminder dates 
	 */
	private function saveReminders($data,$id){

		$organisation_pk = $this->organisation->primaryKey();
		$frequencyid = array($data['frequency_id']);
		$unit_id = (array)$this->reminder_frequency->select(['frequency_id'=>$frequencyid],['unit_id']);
		$fre_unit_id=$unit_id[0]['unit_id'];
		$unit = (array)$this->frequency_unit->select(['unit_id'=>$fre_unit_id],['unit']);
		$fre_unit =$unit[0]['unit'];

		switch($fre_unit){
			case "Week":
			$weeklyRange = new WeeklyReminder($data, $this);
			$weeklyRange->seventhRange($id);
			break;

			case "Month":
			$monthlyRange= new MonthlyReminder($data,$this);
			 $monthlyRange->thirthiethRange($id);
			 $monthlyRange->fiftheenthRange($id);
			 $monthlyRange->tenthRange($id);
			 $monthlyRange->fifthRange($id);
			 $monthlyRange->zeroRange($id);
			 $monthlyRange->afterFiveRange($id);
			 break;

			case "Year":
			$yearlyRange = new YearlyReminder($data, $this);
			$yearlyRange->saveYearlyReminder($id);
		}
	}
	
/*
	 * @desc save   contract reminders
	 * @param string $data,$date_created,$id
	 */
   private function storeReminders($data,$date_created,$id){
	$reminders =array($data);
	
		foreach($reminders as $row){
			$row = [$this->primaryKey()=>$id];
		
			$row = $this->reminder->sanitize($row);
			
	        $row['reminder_date']=$date_created;
			
			
			$this->reminder->save($row);
			//if error encountered break and throw error
			if($this->reminder->isError()==true){
				$this->message($this->reminder->message());
				$this->RollBack();
				break;
			}

   }
}
	
	
	/*
	 * @desc save contract users/persons
	 * @param string $data
	 */
	private function saveUsers($data,$id)
	{
		$user_pk = $this->user->primaryKey();
	
		$users = isset($data['users'])? $data['users'] : [];
		$role_pk = $this->role->primaryKey();
		
		foreach ($users as $row)
		{
			$role_ids = isset($row['roles']['id'])? $row['roles']['id'] : [];
			$row = [$this->primaryKey()=>$id,$user_pk=>$row[$user_pk]];			
			$row = $this->contract_user->sanitize($row);
			$this->contract_user->save($row);
				
			/* if error encountered break and throw error */
			/* if error encountered break and throw error */
			if($this->contract_user->isError()==true){
				$this->message($this->contract_user->message());
				$this->RollBack();
				break;
			}
			
			$this->user->saveRoles($role_ids, $row[$user_pk]);
		}
	}
			
	/*
	 * @desc send mail to the administrator on newly created contracts
	 * @param string $parent
	 */
	private function sendMail($parent,$data)
	{
		$users = isset($data['users'])? $data['users'] : [];
		
		$sender = $this->user->filterOne([$this->user->primaryKey()=>$this->identity()->getId()]);
		
		//compose mail body and subject
		$body = 'Title: '.$parent['contract_title'].'<br/>'.
				'Decription: '.$parent['description'].'<br/>'.
			    'Submitted By '.$sender['full_name'].'<br/>'.
				'Date Created :'.date('d-M-Y h:i a',$parent['submitted_on']).'<br/>'.
				'Expiry Date :'.date('d-M-Y h:i a',$parent['effective_to']).'<br/>'.
				
			    'You shall be receiving reminders depending on the subscription you chose.Please login to review the records.<br/>';
		
		$subject = 'New Contract:'.$parent['contract_title'];
		
		//create list of mail recipients
		$recipient_emails = (array)$this->getArrayMap($users, 'primary_email');
		$recipient_emails[] = ADMIN_EMAIL;
		$primary_recipient = array_shift($recipient_emails);
		
		//invoke mailer to send messages		
		$this->mailer()->setSender(ADMIN_EMAIL);		
		$this->mailer()->setRecipient($primary_recipient);
		$this->mailer()->setCc($recipient_emails);
		$this->mailer()->sendEmail($body, $subject,true);		
		
		//invoke notifier to send/save notifications for the stated contract contact persons
		$persons = $this->person->select(['primary_email'=>$recipient_emails],[$this->person->primaryKey()]);		
		$this->saveBatchNotifications($persons, $subject, $subject, '#contracts/list?cycle_no=2');
	}

	/*
	 * @desc Reminder getter function for use by other classes 
	 * 
	 */
	public function getReminder(){
		return $this->reminder;
	}
		
	
				
									
}