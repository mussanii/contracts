<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a contract experience
 */
class ContractExperience extends CustomModel
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='contracts_experience';
		parent::__construct();				
	}	

	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
		return $data;
	}

	public function save($data)
	{
		if(isset($data['effective_from']))
		{
			$data['effective_from'] = $this->time->dateToTimestamp($data['effective_from']);
		}
		return parent::save($data);
	}
}
