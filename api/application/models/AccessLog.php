<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a addresse
 */
class AccessLog extends CustomModel
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='access_logs';
		parent::__construct();	
	}
	
	public function log($data=null)
	{
		$global = $this->globalVar();
		$identity = $this->identity();
		
		$user = User::getInstance();
		$user_pk = $user->primaryKey();
		$username = $user->getUsernameByEmail($global->getPost('username'));
		
		$data[$user_pk] = (is_a($identity,'User') && $identity->getId()>0)?
							$identity->getId() : $user->fetchColumn($user_pk,['username'=>$username]);
		
		$data['ip_address'] = (!isset($data['ip_address']))? $_SERVER['REMOTE_ADDR'] : $data['ip_address'];
		$data['host'] = (!isset($data['url']))? gethostbyaddr($_SERVER['REMOTE_ADDR']) : $data['url'];
		$agent = isset($_SERVER['HTTP_USER_AGENT'])? $_SERVER['HTTP_USER_AGENT'] : '';
		$data['agent'] = (!isset($data['agent']))? $agent : $data['agent'];
		$data['url'] = (!isset($data['url']))? $_SERVER['REQUEST_URI'] : $data['url'];
		
		$u = parse_url($data['url']);
		$data['resource_name'] = @$u['path'];				
		
		$data['remarks'] = (!isset($data['remarks']))? 'No results recorded' : $data['remarks'];
		$data['audit_start'] = (!isset($data['audit_start']))? time() : $data['audit_start'];
		$data['audit_end'] = (!isset($data['audit_end']))? time() : $data['audit_end'];	
		$data['execution_time'] = $data['audit_end'] - $data['audit_start'];
		
		$browser = $this->browser($data['agent']);
		
		$data['browser'] = $browser->getBrowser();
		$data['platform'] = $browser->getPlatform();
		$data['version'] = $browser->getVersion();
		
		parent::save($data);	
	}
	
	public function select($criteria=null, $columns=null, $group_by=null, $order_by=null, $limit=null)
	{
		$user_pk = User::getInstance()->primaryKey();
		$person_pk = Person::getInstance()->primaryKey();
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$data = (array)parent::select($criteria, $columns, $group_by, $order_by, $limit);
		$role_pk = Role::getInstance()->primaryKey();
		
		for($i=0;$i<count($data);$i++)
		{
			$row = $data[$i];
			if(isset($row['agent']))
			{	
				$u = parse_url($row['url']);
				parse_str($u['query'],$query);
				$payload = User::getInstance()->getTokenPayLoad(@$query['access_token']);
				$row['role'] = Role::getInstance()->fetchColumn('role_name',[$role_pk=>@$payload['role']]);
				
				$row['resource_name'] = $u['path'];
				$browser_path = RESOURCES_ROOT.'browsers/'.$row['browser'].'.png';
				$browser_root = MAIN_ROOT.DS.'resources'.DS.'browsers'.DS.$row['browser'].'.png';				
				$row['browser_icon'] = file_exists($browser_root)? $browser_path : RESOURCES_ROOT.'browsers/Default.png';
				
				unset($row['agent']);
				unset($row['url']);				
			}			
			if(isset($row[$user_pk]))
			{
				$columns = ['first_name','last_name','primary_email','phone'];
				$row[$person_pk] = User::getInstance()->fetchColumn($person_pk,[$user_pk=>$row[$user_pk]]);
				$row = array_merge($row,(array)Person::getInstance()->selectOne($row[$person_pk],$columns));				
				$row['full_name'] = isset($row['first_name'])? $row['first_name'].' '.$row['last_name'] : null;
			}
			$data[$i] = $row;
		}
				
		return $data;
	}	
	
}
