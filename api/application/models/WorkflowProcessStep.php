<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a workflow step
 */
class WorkflowProcessStep extends CustomModel
{
	private $wf_comment;
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name = 'workflow_process_steps';
		parent::__construct();	
		$this->wf_comment = WorkflowComment::getInstance();
	}	
	
	public function save($data){
		if(isset($data['due_date']))
		{
			$data['due_date'] = $this->time->dateToTimestamp($data['due_date']);
		}		
		return parent::save($data);
	}
	

	/*
	 * @method select()
	 * @desc fetch detailed steps and comments and documents
	 * @param array|NULL $criteria
	 * @param array|NULL $columns
	 * @return array $data;
	 */
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		//get original array source of parent
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);		
		
		/* populate additional fields */
		if(!$columns || count($columns)>1)
		{
			for ($i=0;$i<count($data);$i++)
			{
				$row = $data[$i];
				
				/* populate comments info for each record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['comments'] = (array)$this->wf_comment->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
					$row['comments_count'] = count($row['comments']);
				}
				
				$data[$i] = $row;
			}
		}
		return $data;
	}
}
