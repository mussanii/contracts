<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for party
 */
class Party extends CustomModel
{
	
	protected $party_id;
	protected $profile_icon;		
	protected $profile_banner;
	
	//associations/compositions
	private $person;
	private $organisation;
	private $party_addresse;
	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{
		$this->table_name='parties';
		$this->person = Person::getInstance();
		$this->organisation = Organisation::getInstance();
		$this->party_addresse = PartyAddresse::getInstance();
		
		parent::__construct();		
	}	
	
	private function resetTable()
	{
		$this->table_name = 'parties';
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$person_table=$this->person->table();
		$person_key=$this->person->primaryKey();
		
		$organisation_table=$this->organisation->table();
		$organisation_key=$this->organisation->primaryKey();
		
		$table = array_filter(explode(' ',trim($this->table_name)));
		$table = (count($table)>0)? $table[0] : $this->table_name;
		
		$this->table("$table a");
		$this->join("$person_table b"," a.$person_key = b.$person_key", " LEFT ");
		$this->join("$organisation_table c"," a.$organisation_key = c.$organisation_key", " LEFT ");
		
		return parent::select($criteria,$columns,$group_by,$order_by,$limit);
	}
	
	public function save($data)
	{
		$this->resetTable();
		
		$address_data = $data;
		$address_data[$this->primaryKey()] = isset($data[$this->primaryKey()])? $data[$this->primaryKey()] : null;
			
		if(isset($data['party_type']) && $data['party_type']=='O')
		{
			$table = $this->organisation;
			$data['is_owner'] = isset($data['is_owner'])? 'Y' : 'N';
			$data = $this->organisation->sanitize($data);			
			$pk = $this->organisation->primaryKey();
		}
		else
		{
			$table = $this->person;
			$data = $this->person->sanitize($data);
			$pk = $this->person->primaryKey();
		}	
		$data['last_updated'] = time();
			
		//save person or organization data here please
		if(($records_affected = $table->save($data))>0 && (int)$table->lastAffectedId()>0)
		{	
			//collect customer data and add other data along
			$data = [$pk => $table->lastAffectedId()];				
			parent::save($data); 
			$address_data[$this->primaryKey()] = ((int)$this->lastAffectedId()>1)? $this->lastAffectedId() : $address_data[$this->primaryKey()];			
			$this->party_addresse->save($address_data);	
		}
		else
		{	
			$this->party_addresse->save($address_data);
		}
		
		if($this->party_addresse->isError()==true)
		{
			$this->message($this->party_addresse->message());
			$this->isError($this->party_addresse->isError());
		}
		
		if($table->isError()==true)
		{
			$this->message($table->message());
			$this->isError($table->isError());
		}
		
		return (!$this->is_error && $records_affected>0 && (int)$this->recordsAffected()>0)? $this->recordsAffected() : 0;
	}	
	
	public function delete($criteria)
	{
		$party_id = $this->fetchColumn($this->primaryKey(), $criteria,false); 
		$person_id = $this->fetchColumn('b.'.$this->person->primaryKey(), $criteria, false, $this->person->primaryKey());
		$organisation_id = $this->fetchColumn('c.'.$this->organisation->primaryKey(), $criteria,false,$this->organisation->primaryKey());
		
		$this->party_addresse->delete([$this->primaryKey()=>$party_id]);
		parent::delete([$this->primaryKey()=>$party_id]);
		$this->organisation->delete([$this->organisation->primaryKey()=>$organisation_id]);
		$this->person->delete([$this->person->primaryKey()=>$person_id]);
		
		return ((int)$this->recordsAffected()>0 && (int)$this->party_addresse->recordsAffected()>0 &&
				((int)$this->organisation->recordsAffected()>0 || (int)$this->person->recordsAffected()>0))? 
				$this->recordsAffected() : 0;
	}
	
}
