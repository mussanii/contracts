<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a  Reminders
 */
class Reminder extends CustomModel
{	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='reminders';
		parent::__construct();		
	}	
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		global $identity;
		$organisation = Organisation::getInstance();	
		$organisation_pk = $organisation->primaryKey();
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
		for ($i=0;$i<count($data);$i++)
		{
			$row = $data[$i];
			/* populate organisation detail info for each record */
			if(array_key_exists($organisation_pk, $row)==true){
				$row = array_merge($row,(array)$organisation->filterOne([$organisation_pk=>$row[$organisation_pk]]));
			}
			$data[$i] = $row;
		}
		return $data;
	}
}
