<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @base class for a monthly Reminder 
 */



class MonthlyReminder implements ReminderRange  {
    protected $data;
    protected $date_created;
    protected $contract;
    public function __construct($data,$contract){
        $this->contract=$contract;
        $this->data=$data;
        

    }

    function thirthiethRange($id){
        if(array_key_exists('effective_to',$this->data)){
            $date_created = date("Y-m-d", strtotime("-30 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);

        }
       

    }
    function fiftheenthRange($id){
        if(array_key_exists('effective_to',$this->data)){
            $date_created = date("Y-m-d", strtotime("-15 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);

        }
    }
    function tenthRange($id){
        if(array_key_exists('effective_to',$this->data)){
            $date_created = date("Y-m-d", strtotime("-10 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);

        }
    }
    function fifthRange($id){
        if(array_key_exists('effective_to',$this->data)){
            $date_created = date("Y-m-d", strtotime("-5 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);

        }
    }
    function zeroRange($id){
        if(array_key_exists('effective_to',$this->data)){
            $date_created = date("Y-m-d", strtotime("-0 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);

        }
    }
    function afterFiveRange($id){
        if(array_key_exists('effective_to',$this->data)){
            $date_created = date("Y-m-d", strtotime("+5 days",strtotime($this->data["effective_to"])));
            $this->storeReminders($id,$date_created);

        }
    }

  protected  function storeReminders($id,$date_created){
        $reminders =array($this->data);
        
        foreach($reminders as $row){
            $row = [$this->contract->primaryKey()=>$id];
            $row = $this->contract->getReminder()->sanitize($row);
            $row['reminder_date']=$date_created;
                
        //save the reminders
        $this->contract->reminder->save($row);
        //if error encountered break and throw error
        if($this->contract->reminder->isError()==true){
            $this->message($this->contract->getReminder()->message());
            $this->RollBack();
            break;
        }

       }
    }
}