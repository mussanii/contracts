<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a cron
 */

class ContractImport extends CustomPhpExcel
{
	protected  $max_rows=1000;
	private $default_currency='KES';
	private $default_country='KENYA';
	private $default_city='NAIROBI';
	
	private $parties_uploaded;
	private $documents_uploaded;
	private $contracts_uploaded;
	
	protected $user;
	protected $contract;
	protected $organisation;
	protected $contract_party;
	protected $document;
	protected $customer;
	
	private $contract_status;
	private $contract_type;
	private $contract_area;
	private $organisation_unit;
	private $contract_location;
	
	/*compulsory columns for contract area columns */
	private $area_columns=[
			'A'=>['id'=>'area_id','label'=>'ID'],
			'B'=>['id'=>'title','label'=>'AREA TITLE']
	];
	
	
	/*compulsory columns for contract location columns */
	private $location_columns=[
	    'A'=>['id'=>'location_id','label'=>'ID'],
	    'B'=>['id'=>'title','label'=>'CONTRACT LOCATION']
	];
	
	/*compulsory columns for contract type columns */
	private $type_columns=[
			'A'=>['id'=>'type_id','label'=>'ID'],
			'B'=>['id'=>'title','label'=>'TYPE TITLE']
	];
	
	/*compulsory columns for departments/organisation units columns */
	private $unit_columns=[
			'A'=>['id'=>'unit_id','label'=>'ID'],
			'B'=>['id'=>'title','label'=>'DEPARTMENT TITLE']
	];
	
	/*compulsory columns for contract party organisations */
	private $party_columns=[
			'A'=>['id'=>'organisation_id','label'=>'NO'],
			'B'=>['id'=>'full_name','label'=>'NAME OF PARTY*'],
			'C'=>['id'=>'primary_email','label'=>'EMAIL*'],
			'D'=>['id'=>'primary_website','label'=>'WEBSITE*'],
			'E'=>['id'=>'phone','label'=>'PHONE*'],
			'F'=>['id'=>'country','label'=>'COUNTRY'],
			'G'=>['id'=>'primary_location','label'=>'LOCATION'],
			'H'=>['id'=>'primary_address','label'=>'POSTAL ADDRESS'],
			'I'=>['id'=>'first_name','label'=>'CONTACT FIRST NAME*'],
	        'J'=>['id'=>'last_name','label'=>'CONTACT LAST NAME*'],
			'K'=>['id'=>'person_email','label'=>'CONTACT EMAIL*'],
			'L'=>['id'=>'person_phone','label'=>'CONTACT PHONE*']
	];

	/*compulsory columns for contract master data */
	private $contract_columns=[
			'A'=>['id'=>'contract_no','label'=>'SNO*'],
			'B'=>['id'=>'contract_title','label'=>'CONTRACT NAME*'],
			'C'=>['id'=>'description','label'=>'DESCRIPTION'],
			'D'=>['id'=>'counter_party','label'=>'COUNTER PARTY*'],
			'E'=>['id'=>'type_id','label'=>'CONTRACT TYPE'],
			'F'=>['id'=>'area_id','label'=>'CONTRACT AREA'],
			'G'=>['id'=>'unit_id','label'=>'DEPARTMENT*'],
	        'H'=>['id'=>'location_id','label'=>'DOCUMENT LOCATION'],
			'I'=>['id'=>'currency','label'=>'CURRENCY'],
			'J'=>['id'=>'contract_value','label'=>'CONTRACT VALUE'],
			'K'=>['id'=>'signature_date','label'=>'SIGNATURE DATE'],
			'L'=>['id'=>'effective_from','label'=>'EFFECTIVE FROM'],
			'M'=>['id'=>'effective_to','label'=>'EFFECTIVE TO']			
	];
	
	//static to help reuse same instance
	private static $_instance=null;	
	
	/* ensures that only one instace exists per class */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	/* construct class and setup a few associations to get us started */
	public function __construct()
	{
		parent::__construct();
		$this->contract = Contract::getInstance();		
		$this->user = User::getInstance();
		$this->organisation = Organisation::getInstance();		
		$this->contract_party = ContractParty::getInstance();
		$this->document = ContractDocument::getInstance();
		$this->customer = Customer::getInstance();
		$this->contract_area = ContractArea::getInstance();
		$this->contract_type = ContractType::getInstance();
		$this->contract_status = ContractStatuse::getInstance();
		$this->contract_location = ContractLocation::getInstance();
		$this->organisation_unit = OrganisationUnit::getInstance();		
	}
	
	/**
	 * @desc get dropdown combo data to add to dropdowns in excel sheet
	 * @param Model $table
	 * @param Array $columns
	 * @param string $criteria
	 * @return array
	 */
	private function getDropdownData($table,$columns,$criteria=null)
	{
		$columns = $this->contract->getArrayMap($columns,'id');
		$pk = $table->primaryKey(); 
		$columns =  [$pk,$table->concat($columns,':').' AS title'];
		$data = (array)$table->select([],$columns,$criteria);
		return $data;
	}

	/**
	 * @method exportWorkBook()
	 * @desc export workbook so that it can be downloaded via browser
	 * @param string $type file type short code e.g. xls,xlsx,ods etc
	 * @return array file attributes and content
	 */
	public function exportWorkBook($type)
	{
		$extension='.'.$type;

		//based on "short file type" code determine the writer and mime type
		$this->setWriterAndType($type,'contracts','application/zip');
		
		//populate statuses/areas/types and departments		
		
		$this->populateSheet(0,'Types',$this->type_columns,$this->getDropdownData($this->contract_type, $this->type_columns),'veryHidden');
		$this->populateSheet(1,'Area',$this->area_columns,$this->getDropdownData($this->contract_area, $this->area_columns),'veryHidden');
		$this->populateSheet(2,'Units',$this->unit_columns,$this->getDropdownData($this->organisation_unit, $this->unit_columns),'veryHidden');
		$this->populateSheet(3,'Locations',$this->location_columns,$this->getDropdownData($this->contract_location, $this->location_columns),'veryHidden');
		
		//create sheets for parties
		$this->populateSheet(4,'Parties',$this->party_columns);
		
		//create contract sheet
		$this->populateSheet(5,'Contracts',$this->contract_columns);
		
		//add dropdowns to contract sheet - parties/type/area/departments
		$this->addDropdown($this->getSheetNames()[0],'E','B');
		$this->addDropdown($this->getSheetNames()[1],'F','B');
		$this->addDropdown($this->getSheetNames()[2],'G','B');
		$this->addDropdown($this->getSheetNames()[3],'H','B');
		$this->addDropdown($this->getSheetNames()[4],'D','B');

		//write to excel and save file
		$this->writeExel();
		
		//set source and destination folder paths
		$src_path = $this->target;
		$des_path = $this->target.DS.$this->zip_name;
		
		//zip content folder 
		Zipper::zipDir($src_path, $des_path);
		
		//obtain file contents
		$content = file_get_contents($des_path);
		
		//return file attributes and content
		return ['content'=>$content,'type'=>$this->zip_mime_type,'name'=>$this->zip_name];
	}
		
	/**
	 * @method importWorkBook()
	 * @desc import workbook uploaded via browser extract and insert records to db
	 * @return array file attributes and error messages
	 */
	public function importWorkBook()
	{
	    global $global;
	    
		$file = $this->getUploadedFileMeta();
		$is_error = false;
		$count = 0;
		
		if($file && $file['error']==false){
			//unzip the uploaded files and extract them to server folders
			Zipper::unzip($file['tmp_name'], $this->target);						
			$excel_path = FileSystem::getPath($this->target, $this->master_file.'.');			
			$docs_path = dirname($excel_path).DS.$this->doc_folder; 
			$excel=\PHPExcel_IOFactory::load($excel_path); 
			
			//loop through rows in excel and insert to database
			foreach ($excel->getWorksheetIterator() as $worksheet) {
				switch($worksheet->getTitle()){
					case 'Parties' : $parties = $this->saveParties($worksheet); $is_error = ($is_error || $this->organisation->isError()); break;
					case 'Contracts' : $count = $this->saveContracts($worksheet,$parties,$docs_path); $is_error = ($is_error || $this->contract->isError()); break;
				}
				if($is_error==true) break;
			}			
			$error_msg = $this->organisation->isError()==true? $this->organisation->message() : $this->contract->message();
		}
		else
		{
			$error_msg = $file['error'];
		}
		
		//send batch email after successfully finishing upload
		if($is_error!==true && $count>0 && $global->getPost('send_email')=='Y') 
		    $this->model->sendMailBatchUpload('Contracts','Uploaded',SITE_URL,$count);
				
		$file['message']= $is_error==true? $error_msg : $this->uploadSuccessMessage();
		$file['title'] = TITLE_UPLOAD_OPERATION;
		$file['status'] = $is_error==true? STATUS_ERROR : STATUS_SUCCESS;
		$file['success'] = $is_error==true? 0 : 1;
		$file['tmp_name'] = null;
				
		return $file;
	}
	
	private function uploadSuccessMessage()
	{
	    $message = 'Success: ';
		if($this->parties_uploaded>0)
			$message .= $this->parties_uploaded.' Partie(s) were successfully uploaded ';
		if($this->contracts_uploaded>0)
			$message .= $this->contracts_uploaded.' Contract(s) were successfully uploaded ';
		if($this->documents_uploaded>0)
			$message .= $this->documents_uploaded.' Document(s) were successfully uploaded ';
		if($this->parties_uploaded==0 && $this->contracts_uploaded==0 && $this->documents_uploaded==0)
			$message = 'No contracts, parties or documents uploaded';
		return $message;
	}
	
	/**
	 * @method saveParties()
	 * @desc save master info on party organisations
	 * @param PHPExcel_Worksheet $worksheet
	 * @return array $inserted details of inserted records
	 */
	private function saveParties(\PHPExcel_Worksheet $worksheet)
	{
		$this->parties_uploaded = 0;
		$parties=[];		
		for ($row = 2; $row <= $worksheet->getHighestRow(); ++ $row) {	
			$row_data = []; 
			foreach ($this->party_columns as $col_key=>$value)
			{
				//get table column name
				$field_name=$value['id'];

				//extract $val and $type
				extract($this->getCellAttributes($worksheet, $row, $col_key));
					
				//assemble values in this array
				$row_data[$field_name] = $val;			
			}
			
			//filter data. if column count is zero, then continue to next row
			$row_data = array_filter($row_data);
			if(count($row_data)==0) continue;
			
			//additional fields here
			$row_data['person'] = $this->extractContactPerson($row_data);
			
			//check if organisation already exists and retrieve its id
			$id = $this->organisation->fetchColumn($this->organisation->primaryKey(),['full_name'=>$row_data['full_name']]);
			
			//save data. If any errors encountered rollback and send messages to caller
			if(intval($id)==0)
			{
				unset($row_data[$this->organisation->primaryKey()]);
				$row_data['country'] = isset($row_data['country'])? $row_data['country'] : 'KE';
				$this->customer->save($row_data);
				if($this->customer->isError()==true)
				{
					$cell_location = '.Please check worksheet "'.$worksheet->getTitle().'" at row '.$row;
					$this->organisation->isError($this->customer->isError());
					$this->organisation->message($this->customer->message().$cell_location);
					break;
				}								
				$this->parties_uploaded = $this->parties_uploaded + $this->customer->recordsAffected();
			}
						
			$parties[] = array_merge([$this->organisation->primaryKey()=>$id],$row_data);
		}
		return $parties;
	}
	
	/**
	 * @method extractContactPerson()
	 * @desc extract contact person from organisation data supplied from excel
	 * @param array $data organisation data from excel
	 */
	private function extractContactPerson($row_data)
	{
		$person = [];
		foreach ($this->party_columns as $col_key=>$value)
		{
			//get table column name
			$field_name=$value['id'];
			if($field_name=='first_name' && isset($row_data['first_name']))
			{
				$person['first_name'] = $row_data['first_name'];
			}
			if($field_name=='last_name' && isset($row_data['last_name']))
			{
			    $person['last_name'] = $row_data['last_name'];
			}
			if($field_name=='person_phone' && isset($row_data['person_phone']))
			{
			    $person['phone'] = $row_data['person_phone'];
			}
			if($field_name=='person_email' && isset($row_data['person_email']))
			{
				$person['primary_email'] = $row_data['person_email'];
			}
		}
		return $person;		
	}
	
	/**
	 * @desc return a list of excel columns and respective foreign keys tables
	 * @return array
	 */
	private function getContractForeignKeys(){
	    return [
	        'E'=>['table'=>$this->contract_type,'title_col'=>'title'],
	        'F'=>['table'=>$this->contract_area,'title_col'=>'title'],
	        'G'=>['table'=>$this->organisation_unit,'title_col'=>'title'],
	        'H'=>['table'=>$this->contract_location,'title_col'=>'title']
	    ];
	}
	
	/**
	 * @desc save foreign key tables
	 * @param string $col_key
	 * @param string $field_data
	 * @return array
	 */
	private function insertIfNotExists($col_key , $field_data)
	{	    
	    $meta = $this->getContractForeignKeys();
	    $table = $meta[$col_key]['table'];
	    $title_col = $meta[$col_key]['title_col'];
	    $pk = $table->primaryKey();
	    
	    $count = count($field_data);
	    $title_val = ($count==1)? $field_data[0] : (($count==2)? $field_data[1] : null);
	    
	    if($title_val)
	    {
	        $pk_val = $table->fetchColumn($pk,[$title_col=>$title_val]);
	        $field_data = [$pk_val,$title_val];
	        if(!$pk_val){ 
	            $table->save(['code'=>strtoupper($this->contract->randomize(3)),$title_col=>$title_val]);
	            $field_data = [$table->lastAffectedId(),$title_val];
	        }	        
	    }
	    
	    return $field_data;
	}
	

	/**
	 * @method saveContracts()
	 * @desc Extract contract master data and save it
	 * @param \PHPExcel_Worksheet $worksheet
	 * @return multitype:multitype:
	 */
	private function saveContracts(\PHPExcel_Worksheet $worksheet,$parties,$docs_path)
	{   
	    $fks = $this->getContractForeignKeys();
		$this->contract->enableMailer(false);		
		$contracts = [];	
		$this->contracts_uploaded = 0;
		$this->documents_uploaded = 0;
		for ($row = 2; $row <= $worksheet->getHighestRow(); $row++) {
			$row_data = [];
			foreach ($this->contract_columns as $col_key=>$value)
			{				
				//get table column name
				$field_name=$value['id'];
				
				//extract $val and $type
				$is_date = ($field_name=='effective_from' || $field_name=='effective_to');
				extract($this->getCellAttributes($worksheet, $row, $col_key,$is_date));
				
				//assemble values in this array
				$row_data[$field_name] = $val;
				
				//extract foreign key data and add to contract master data
				if(in_array($col_key, array_keys($fks))==true){
					$field_data = $this->insertIfNotExists($col_key,array_filter(explode(':',$val)));					
					if(count($field_data)==2){
					    $row_data[$field_name] = $field_data[0];
					}
				}
			}			
			$row_data['cycle_no'] = 6; //this is the lifecycle no. of an active contract
			$row_data['parties'] = $this->getContractParties($parties, $row_data);
			
			//check if organisation already exists and retrieve its id
			$id = $this->contract->fetchColumn($this->contract->primaryKey(),['contract_title'=>$row_data['contract_title']]);
				
			//save data. If any errors encountered rollback and send messages to caller
			if(intval($id)==0)
			{
				//save contract data if errors arise then break and obtain errors and messages 
				$this->contract->save($row_data);
				if($this->contract->isError()==true)
				{
					$this->contract->message('Worksheet '.$worksheet->getTitle().':'.$this->contract->message());
					break;
				}
				$id = $this->contract->lastAffectedId();
				$this->contracts_uploaded += $this->contract->recordsAffected();
			}
			
			//next save contract documents
			$this->documents_uploaded += $this->saveDocuments($docs_path,@$row_data['contract_title'],$id);
		}
	}
	/**
	 * @method getContractParties()
	 * @desc Determine contract parties to link to a contract and detail them
	 * @param Array $parties 2-D list of party information
	 * @param Array $contract 1-D list of contract master information
	 * @return Array 2-D list of contract parties
	 */
	private function getContractParties($parties,$contract)
	{
		$pk = $this->organisation->primaryKey();
		$payload = $this->user->getPayload();
		$organisation_id =isset($_POST[$pk]) && $_POST[$pk]>0? $_POST[$pk] : $payload['organisation'][$pk];
		
		//the logged in entity becomes the first party
		$contract_parties = [[$pk => $organisation_id]];
		
		//loop through saved parties and add them to the list of contract parties
		foreach ($parties as $party)
		{
			//look for matches i.e. remember this data comes from excel
			if($party['full_name'] == $contract['counter_party'])
			{
				$contract_parties[] = [$pk => $this->organisation->fetchColumn($pk,['full_name'=>$party['full_name']])];				
			}
			
		}
		
		return $contract_parties;
	}

	/**
	 * @method saveDocuments()
	 * @desc Loop through contract folders within the target --> extract path 
	 * @desc Save the documents for each contract
	 * @desc Each contract title has folder carrying documents labelled by the title itself
	 * @param unknown $dir
	 */
	public function saveDocuments($docs_path,$contract_title,$id)
	{
		$count = 0;
		$pk = $this->contract->primaryKey();		
		//fetch all documents inside the contract folder --> $contract_title
		$documents = FileSystem::getFiles($docs_path.DS.$contract_title);	
		
		foreach ($documents as $file)
		{
			$name = $file->getFileName();
			if($this->document->count([$pk=>$id,'file_name'=>$name])==0)
			{
				$path = $file->getRealPath();
				$content= file_get_contents($path);
				$size= strlen($content);
				$type= FileSystem::getMimeType($path);			
				$data=[$pk=>$id,'file_name'=>$name,'file_size'=>$size,'mime_type'=>$type,'content'=>$content];
				$this->document->save($data);				
				$count += $this->document->recordsAffected();
			}
		}
		return $count;
	}
	
}
