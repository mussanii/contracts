<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a workflow process
 */
class WorkflowProcesse extends CustomModel
{
	private static $_instance=null;
	private $organisation;
	private $role;
	private $user;
	private $wf_step;
	private $wf_status;
	private $wf_action;
	private $wf_document;
	private $wf_parameter;
	private $reminder_frequency;
	private $frequency_unit;
	public  $reminder;
	private $date_created;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='workflow_process';
		parent::__construct();
		
		//capture instances of object associations		
		$this->wf_action = WorkflowAction::getInstance();
		$this->wf_document = WorkflowDocument::getInstance();
		$this->wf_status = WorkflowStatuse::getInstance();
		$this->wf_step = WorkflowProcessStep::getInstance();
		$this->wf_parameter = WorkflowParameter::getInstance();
		$this->user = User::getInstance();
		$this->organisation = Organisation::getInstance();	
		$this->role = Role::getInstance();
		$this->reminder_frequency= ReminderFrequency::getInstance();
		$this->frequency_unit = FrequencyUnit::getInstance();
		$this->reminder = ProcessReminder::getInstance();

	}
	
	public function update($data,$criteria = null)
	{	
		$pk = $this->primaryKey();
		$response = parent::update($data,$criteria);	
		$process_id = isset($criteria[$pk])? $criteria[$pk] : null;	
		
		if(intval($process_id)>0 && intval($this->recordsAffected())>0)
		{	
			$data = $this->selectOne($process_id);
			$this->reportOnCompletion($data,$data);
		}
		return $response;
	}
	
	public function reportOnCompletion($process)
	{
		if($process[$this->primaryKey()] && in_array($process['status']['code'], ['CNL','CMP'])==true)
		{
			$current = isset($process['current'])? $process['current'] : null;
			$comment = isset($current['comments'][0])? $current['comments'][0] : null;
			$message = isset($comment['comment_title'])? $comment['comment_title'] : null;
			$status = isset($comment['action']['action_effect'])? $comment['action']['action_effect'] : null;
			
			$others= (array)$this->getArrayAssoc((array)$process['parameters'], 'parameter_name', 'parameter_value',false);
			$params = array_merge([
				'nEntity_ID' => $process[$this->organisation->primaryKey()],
				'nWf_Process_ID' => $process[$this->primaryKey()],
				'vStatus' => $status,
				'vMessage' => $message
			],$others);
			
			$end_point = UserSetting::getInstance()->fetchColumn('value',['code'=>'WRU']);
			$response = $this->globalVar()->sendCurlRequest($end_point, http_build_query($params));
		}
	}
	
	/*
	 * @method select()
	 * @desc fetch detailed workflows including status, steps, actioms
	 * @param array|NULL $criteria 
	 * @param array|NULL $columns 
	 * @return array $data;
	 */
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		//get original array source of parent
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
		
		//capture foreign keys of each object
		$step_pk = $this->wf_step->primaryKey();
		$status_pk = $this->wf_status->primaryKey();
		$action_pk = $this->wf_action->primaryKey();		
		$document_pk = $this->wf_document->primaryKey();
		$frequency_pk = $this->reminder_frequency->primaryKey();

		/* populate additional fields */
		if(!$columns || count($columns)>2)
		{
			for ($i=0;$i<count($data);$i++)
			{
				$row = $data[$i];
				
				/* populate steps info for each record */
				/* populate parameters for each record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$row['steps'] = (array)$this->wf_step->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
					$row['current'] = (array)$this->wf_step->filterOne([$this->primaryKey()=>$row[$this->primaryKey()],'is_current'=>'Y']); 
					$row['parameters'] = (array)$this->wf_parameter->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
				}
				 /* populate reminder frequencies info for each record */
				 if(array_key_exists($frequency_pk, $row)==true){
				    $row['frequency'] = (array)$this->reminder_frequency->filterOne([$frequency_pk=>$row[$frequency_pk]]);
				}
				
				
				/* populate status info for each record */
				if(array_key_exists($status_pk, $row)==true){
					$row['status'] = (array)$this->wf_status->filterOne([$status_pk=>$row[$status_pk]]);
				}
				
				/* populate action info for each record */
				if(array_key_exists($action_pk, $row)==true){
					$row['action'] = (array)$this->wf_action->filterOne([$action_pk=>$row[$action_pk]]);
				}
								
				$data[$i] = $row;				
			}
		}
		return $data;
	}
	
	/*
	 * @method save()
	 * @desc save workflow details and their steps
	 * @param array $data contains workflow details and details for step
	 */
	public function save($data)
	{
		global $identity;
		
		//start transaction
		$this->startTransaction();
	
		//clean parent data
		$parent = $this->sanitize($data);
		
		
		/* we do this for new records */
		if(array_key_exists($this->primaryKey(), $parent)==false || (int)$parent[$this->primaryKey()]==0){
			//obtain status id for new record
			$status_key = $this->wf_status->primaryKey();
			$organisation_key = $this->organisation->primaryKey();			
			$payload = $identity->getPayload();
			
			$parent[$organisation_key] = $payload['organisation'][$organisation_key];
			$parent[$status_key] = $this->wf_status->fetchColumn($status_key,['code'=>'NST']);			
			$parent['submitted_on'] = time();
			$parent['due_date'] = (isset($parent['due_date']) && trim($parent['due_date'])!='')? $parent['due_date'] : time();
			$process_title = isset($parent['process_title'])? $parent['process_title'] : null;
			$parent['process_description'] = isset($parent['process_description'])? $parent['process_description'] : $process_title;
		}
		else{
			//clean parent data
			$data = array_merge($this->selectOne($data[$this->primaryKey()]),$data);
			$parent = $this->sanitize($data);
			$parent['last_updated'] = time();
		}
		
		//save the parent record and retrieve process id
		parent::save($parent);


		if($this->recordsAffected()>0)
		{
			$id = $this->lastAffectedId();
		
		$id = $this->lastAffectedId();

		//update path_ref (Url to Process) if originally missing
		if($id && (array_key_exists('path_ref', $parent)==false || trim($parent['path_ref'])=='')){
			$parent['path_ref'] = '/processes/execution/'.$id;
			parent::update(['path_ref'=>$parent['path_ref']],[$this->primaryKey()=>$id]);
		}			
	
		//save process steps - children
		if($id && ($this->isError()!==true))
			$this->saveSteps(isset($data['steps'])? $data['steps'] : [] , $id);
		
		//save process reminders
			if($id && ($this->isError() != true))
			$this->saveProcessReminders($data,$id);


		//save process steps - children
		if($id && ($this->isError()!==true))
			$this->saveParameters(isset($data['parameters'])? $data['parameters'] : [] , $id);
	
		/* if NO errors encountered commit transaction*/
		if($this->isError()!==true)
			$this->endTransaction();
	
		//send emails here to company admin
		if($id && ($this->isError()!==true))
			$this->sendMail($parent,$id);

		}
		
	
		//finally return no.of records affected
		return ($this->isError()!==true)? $this->recordsAffected() : 0;
	}
	
	/*
	 * @method saveSteps()
	 * @desc save each of the steps in a process
	 * @param array $steps
	 * @param int $process_id
	 */
	private function saveSteps($steps,$process_id)
	{
		global $identity;
		$steps = array_filter(is_array($steps)? $steps : (array)json_decode($steps,true));
		$payload = $identity->getPayload();
		$is_current = false;
		
		foreach ($steps as $child)
		{
			$is_current = (isset($child['is_current']) && $child['is_current']=='Y') ? true : $is_current;
			
			$child[$this->primaryKey()] = $process_id;
			$child['min_approvers'] = isset($child['min_approvers'])? $child['min_approvers'] : 1;
			$child['is_mandatory'] = isset($child['is_mandatory'])? $child['is_mandatory'] : 'Y';
						
			$this->wf_step->save($this->wf_step->sanitize($child));
		
			//on meeting errors roll back this transaction
			if($this->wf_step->isError()==true){
				$this->message($this->wf_step->message());
				$this->RollBack();
				break;
			}
		}
		/* if no current step activate first step */
		if(!$is_current){
			$this->move($process_id,true);
		}
	}

	
	 /*
	 * @method save()
	 * @desc save process Reminder dates 
	 * @param array $data contains process_id, and reminder dates 
	 */
	private function saveProcessReminders($data,$id){

		$organisation_pk = $this->organisation->primaryKey();
		$frequencyid = array($data['frequency_id']);
		$unit_id = (array)$this->reminder_frequency->select(['frequency_id'=>$frequencyid],['unit_id']);
		$fre_unit_id=$unit_id[0]['unit_id'];
		$unit = (array)$this->frequency_unit->select(['unit_id'=>$fre_unit_id],['unit']);
		$fre_unit =$unit[0]['unit'];

		switch($fre_unit){
			case "Week":
			$weeklyRange = new WorkflowWeeklyReminder($data, $this);
			$weeklyRange->seventhRange($id);
			break;

			case "Month":
			$monthlyRange= new WorkflowMonthlyReminder($data,$this);
			 $monthlyRange->thirthiethRange($id);
			 $monthlyRange->fiftheenthRange($id);
			 $monthlyRange->tenthRange($id);
			 $monthlyRange->fifthRange($id);
			 $monthlyRange->zeroRange($id);
			 $monthlyRange->afterFiveRange($id);
			 break;

			case "Year":
			$yearlyRange = new WorkflowYearlyReminder($data, $this);
			$yearlyRange->saveYearlyReminder($id);
		}
	}
	
/*
	 * @desc save   contract reminders
	 * @param string $data,$date_created,$id
	 */
   private function storeReminders($data,$date_created,$id){
	$reminders =array($data);
	
		foreach($reminders as $row){
			$row = [$this->primaryKey()=>$id];
		
			$row = $this->reminder->sanitize($row);
			
	        $row['reminder_date']=$date_created;
			
			
			$this->reminder->save($row);
			//if error encountered break and throw error
			if($this->reminder->isError()==true){
				$this->message($this->reminder->message());
				$this->RollBack();
				break;
			}

   }
}
	
	/*
	 * @method saveParameters()
	 * @desc save each of the parameters in a process
	 * @param array $params
	 * @param int $process_id
	 */
	private function saveParameters($parameters,$process_id)
	{
		global $identity;
		$parameters = array_filter(is_array($parameters)? $parameters : (array)json_decode($parameters,true));
		$payload = $identity->getPayload();
		$is_current = false;
	
		foreach ($parameters as $child)
		{
			$child[$this->primaryKey()] = $process_id;
			if(!isset($child['parameter_name']) || trim($child['parameter_name'])=='') continue;
			$child['data_type'] = isset($child['data_type'])? $child['data_type'] : 'string';	
			$this->wf_parameter->save($this->wf_parameter->sanitize($child));		
						
			//on meeting errors roll back this transaction
			if($this->wf_parameter->isError()==true){
				$this->message($this->wf_parameter->message());
				$this->RollBack();
				break;
			}
		}
	}
	
	/*
	 * @desc Copy some details of an existing process to a new one
	 * 
	 */
	public function cloneProcess($id)
	{
		$data = $this->selectOne($id);
		if($data)
		{
			//recapture some keys and knock out some
			$data['id_parent'] = $id;
			unset($data[$this->wf_status->primaryKey()]);
			unset($data[$this->primaryKey()]);
			unset($data['submitted_on']);
			unset($data['path_ref']);
			
			$steps = (array)$data['steps'];
			$parameters = (array)$data['parameters'];
			
			//loop through each steps while knocking some keys
			foreach ($steps as &$step)
			{
				$step[$this->primaryKey()] = $id;
				unset($step[$this->wf_step->primaryKey()]);
				unset($step['due_date']);
				unset($step['is_current']);
			}
			
			//loop through parameters and knock out some keys
			foreach ($parameters as &$parameter)
			{
				$parameter[$this->primaryKey()] = $id;
				unset($parameter[$this->wf_parameter->primaryKey()]);
				unset($parameter['parameter_value']);				
			}
			
			$data['steps'] = $steps;
			$data['parameters'] = $parameters;
		}
		
		return $data;
	}
	
	/**
	 * @desc move process to the next step
	 */
	public function move($process_id,$next=true)
	{
		$step_pk = $this->wf_step->primaryKey();
		$data=$this->wf_step->filterOne([$this->primaryKey()=>$process_id,'is_current'=>'Y']);
		
		$step_no= 1;	
				
		//do something here to get next/previous step depending on content of $next flag
		if(isset($data[$step_pk]) && (int)$data[$step_pk]>0)
		{
			$c_step = $data['step_no'];
			$l_step= $this->wf_step->filterOne([$this->primaryKey()=>$process_id],['step_no'],null,'step_no DESC','LIMIT 1');
			$l_step = isset($l_step['step_no'])? $l_step['step_no'] : 1;
			$step_no = ($next==true)? (($c_step<$l_step)? $c_step + 1 : $l_step) : (($c_step>1)? $c_step - 1 : 1);
		}
	
		$this->startTransaction();
		
		$this->wf_step->update(['is_current'=>'N'],[$this->primaryKey()=>$process_id]);
		$this->wf_step->update(['is_current'=>'Y'],[$this->primaryKey()=>$process_id,'step_no'=>$step_no]);
	
		if($this->wf_step->recordsAffected()>0)
		{
			$this->recordsAffected($this->wf_step->recordsAffected());
		}
		
		$this->endTransaction();
		return $this->recordsAffected();
	}
	
	/*
	 * @desc send mail to the administrator
	 * @param string $parent
	 */
	private function sendMail($parent,$id)
	{
		global $smtp_config;
		
		//table instances that we may need for later record modifications and queries
		$user_role_tb = UserRole::getInstance();
		$process_type = WorkflowProcessType::getInstance();
		$user_pk = $this->user->primaryKey();
		$role_pk = $this->role->primaryKey();
		$type_pk = $process_type->primaryKey();
		
		//get details of recipients at current step
		$cur_step = $this->wf_step->filterOne([$this->primaryKey()=>$id,'is_current'=>'Y']);
		$steps = $this->wf_step->select([$this->primaryKey()=>$id]);
		$user_id = $user_role_tb->fetchColumn($user_pk,[$role_pk=>$cur_step[$role_pk]]);
		$recipients = (array)$this->user->select([$user_pk => $user_id]);
		
		
		//compose email body and subject
		$sender = $this->user->filterOne([$user_pk=>$this->identity()->getId()]);
		
		
		$title = isset($parent[$type_pk])? $process_type->fetchColumn('title',[$type_pk=>$parent[$type_pk]]) : '';
		
		
		if(trim($title)=='')	
		{	
			$body =  'A new workflow has been submitted by.'.$sender['full_name'].'<br/><br/>'.
					'<i>"'.$parent['process_description'].'"</i><br/><br/>'.
					 'Your approval is needed at at step '.
				 '<b>'.$cur_step['step_no'].' of '. count($steps).'</b> -'.$cur_step['step_title'];
		}
		else 
		{
			$body =  'A new '.$title.' has been submitted by.'.$sender['full_name'].'<br/><br/>'.
					'<i>"'.$parent['process_description'].'"</i><br/><br/>';
		}
		
		$subject = $parent['process_title'];
		
		//assemble details of recipients
		$recipient_emails = $this->getArrayMap($recipients,'primary_email');
		
				
		//create one recipient and add the rest of the emails as CCs
		$recipient_emails[] = $smtp_config['ADMIN_EMAIL'];
		$primary_recipient = array_shift($recipient_emails);
		
		//invoke mailer and send mail messages
		$this->mailer()->setSender($sender['primary_email'],$sender['full_name']);
		$this->mailer()->setRecipient($primary_recipient);
		$this->mailer()->setCc($recipient_emails);
		$this->mailer()->sendEmail($body, $subject,true);	

		//invoke notifier and send messages
		$person = Person::getInstance();
		$persons = $person->select(['primary_email'=>$recipient_emails],[$person->primaryKey()]);
		$message = 'New workflow has been created : '.$subject;		
		$this->saveBatchNotifications($persons, $subject, $message, '#'.$parent['path_ref']);
	}

	/*
	 * @desc Reminder getter function for use by other classes 
	 * 
	 */
	public function getReminder(){
		return $this->reminder;
	}	 
	
}