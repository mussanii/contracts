<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a object
 */
class Object extends CustomModel
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='objects';
		parent::__construct();				
	}	
	
	/**
	 * @method addDashboard()
	 * @desc create a default dashboard object for a new role
	 * @param integer $role_id
	 * @return Ambigous <number, string>
	 */
	public function addDashboard($role_id)
	{
		/*obtain some instances to use for later */
		$role = Role::getInstance();
		$role_object = RoleObject::getInstance();
		
		/* obtain some primary keys columns here*/
		$role_pk = $role->primaryKey();
		$pk = $this->primaryKey();
		
		/* lets clone from the default dashboard object -DDB */
		$data = (array)$this->filterOne(['code'=>'DDB']);
		$data = array_merge($data,[$pk=>null,'code'=>strtoupper($this->randomize(3))]);
		
		/* save the dashboard object clone and proceed */
		$results = $this->save($data);
		
		/* link the saved dashboard object to role_id supplied*/
		$id = intval($this->lastAffectedId());
		if($id>0)
		{
			/*remove records matching this object for different roles*/
			$role_object->delete([$pk=>$id]);
			
			/* now lets save the formed role object record */
			$role_object->save([$role_pk=>$role_id,$pk=>$id,'effective_from'=>time(),'active'=>'Y']);
		}
		
		return $results;
	}
}
