<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a person
 */
class Person extends CustomModel
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='persons';
		parent::__construct();			
	}		
	
	public function save($data)
	{
		if(array_key_exists($this->primaryKey(), $data)==true){
			$data = array_merge((array)$this->selectOne($data[$this->primaryKey()]),$data);
		}
		return parent::save($data);
	}	
}
