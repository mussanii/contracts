<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2020 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a case log
 */
class CaseLog extends OrganisationObject
{		
	//associations/compositions	
	private static $_instance=null;
	private $counter;
	private $case_status;
	private $case_person;
	private $person;
	private $user;
	private $document;

	public function __construct()
	{		
		$this->table_name='cases_logs';		
		parent::__construct();	
		
		//capture instances of object associations
		$this->case_person = CasePerson::getInstance();
		$this->case_status = CaseStatuse::getInstance();
		$this->counter = Counter::getInstance();
		$this->person = Person::getInstance();
		$this->user = User::getInstance();
		$this->document = CaseDocument::getInstance();
		
	}	
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
		
	
	
	/*
	 * @method select()
	 * @desc fetch detailed case details
	 * @param array|NULL $criteria
	 * @param array|NULL $columns
	 * @return array $data;
	 */
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
	   //capture foreign keys of each object
		$user_pk = $this->user->primaryKey();
	    $document_pk = $this->document->primaryKey();
	    //get original array source of parent
	    $order_by = $order_by? $order_by : $this->primaryKey().' DESC';
	    $data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
	    
	    /* populate additional fields */
	    if(!$columns || count($columns)>2)
	    {
	        for ($i=0;$i<count($data);$i++)
	        {
	            $row = $data[$i];

	            /* populate case resource persons info for each record */
	            if(array_key_exists($this->primaryKey(), $row)==true){
					$row['persons'] = (array)$this->case_person->select([$this->primaryKey()=>$row[$this->primaryKey()],'active'=>'Y']);
	                $persons_names = $this->getArrayMap($row['persons'], 'full_name');
					$row['persons_names'] = implode(',', $persons_names);
					$row['status'] = (array)$this->case_status->select([$this->primaryKey()=>$row[$this->primaryKey()]]);
					$row['advocates'] = (array)$this->case_person->select([$this->primaryKey()=>$row[$this->primaryKey()],'role_id'=>3]);
					$row['advocate_full_name'] = $row['advocates'][0]['first_name']. ' ' . $row['advocates'][0]['last_name'];
					
					
				
	            
	                /* get owner details here */
	                $person = $this->user->selectOne($row['owner_id']);
	                $row['owner'] = $this->arraySubset($person, [$user_pk,'full_name']);
					
				}
				/* populate documents for each case record */
				if(array_key_exists($this->primaryKey(), $row)==true){
					$columns=[$document_pk,'file_type','file_name','file_size','mime_type','main','effective_from'];
					$document_criteria = ['main'=>'Y',$this->primaryKey()=>$row[$this->primaryKey()]];
					$document_id = $this->document->fetchColumn($this->document->primaryKey(),$document_criteria);
					$row['document_url'] = API_ROOT.'/case-documents/download/'.$document_id.'?access_token=';
					$row['documents'] = (array)$this->document->select([$this->primaryKey()=>$row[$this->primaryKey()]],$columns);
					
				}
	            
	            $data[$i] = $row;	
	        }
	    }
	    
	    return $data;
	}
	
	
	/*
	 * @method save()
	 * @desc save case logs to database
	 * @param array $data contains dictionary of data
	 */
	public function save($data)
	{
	    $identity = $this->identity();
	    
	    //start transaction
	    $this->startTransaction();
	    
	    //clean parent data
	    $parent = $this->sanitize($data);
	    
	    if(array_key_exists($this->primaryKey(), $parent)==false || (int)$parent[$this->primaryKey()]==0){
	        $parent['owner_id'] = $identity->getId();
			$parent['case_no'] = trim($parent['case_no'])!=''? $parent['case_no'] : $this->counter->nextCounter('CS');
			$payload = $identity->getPayload();

	    }
	    
	    //save the parent record and retrieve process id
	    parent::save($parent);
	    
	    //save secondary records each as a record
	    if($this->recordsAffected()>0)
	    {
	        $id = $this->lastAffectedId();
	        
	        //save contract parties
	        if($this->isError()!==true)
	            $this->saveResources($data, $id);
	            
            //save contract persons/users
            if($this->isError()!==true)
				$this->saveStatus($data, $id);
			
				
	    }
	    
	    /* if NO errors encountered commit transaction*/
	    if($this->isError()!==true)
	    {
	        $this->endTransaction();
	    }
	    
	    //finally return no.of records affected
	    return ($this->isError()!==true)? $this->recordsAffected() : 0;	    
	}
	
	public function saveResources($data,$id)
	{  
	    $rows = isset($data['persons'])? array_filter($data['persons']) : []; 
	    foreach ($rows as $row)
	    {
	        if(!isset($row['first_name']) || !isset($row['last_name'])) continue;
	        
	        $row[$this->primaryKey()] = $id;	        
	        $this->case_person->save($row);
	        
	        //if error encountered break and throw error
	        if($this->case_person->isError()==true){
	            $this->message($this->case_person->message());
	            $this->RollBack();
	            break;
	        }
	    }
	}
	
	public function saveStatus($data,$id)
	{
	    $rows = isset($data['status'])? array_filter($data['status']) : []; 
	    foreach ($rows as $row)
	    {
	        $row[$this->primaryKey()] = $id; 
	        $row = $this->case_status->sanitize($row);	
	        
	        if(trim($row['status_text'])=='') continue;
	        $this->case_status->save($row);
	        
	        /* if error encountered break and throw error */
	        if($this->case_status->isError()==true){
	            $this->message($this->case_status->message());
	            $this->RollBack();
	            break;
	        }
	    }
	}

}
