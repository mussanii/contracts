<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for an user role
 */
class UserRole extends CustomModel
{	
	private $role;
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='users_roles';
		$this->role = Role::getInstance();
		parent::__construct();		
	}	
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
        $order_by = ($order_by)? $order_by : $this->role->primaryKey();
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
		$fr_key = $this->role->primaryKey();
		
		for($i=0;$i<count($data);$i++)
		{
			$row=$data[$i];
			if(array_key_exists($fr_key, $row)==true)
			{
				$data[$i]=array_merge($row,$this->role->selectOne($row[$fr_key]));
			}
		}
		return $data;
	}
}
