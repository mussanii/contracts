<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for phone message
 */
class PhoneMessage extends CustomModel
{	
	private static $_instance=null;
	private $box_type;
	
	private $sender;	
	private $recipient;
		
	/*Singlerecipientn Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{
		$this->box_type = MailBoxe::getInstance();
		$this->table_name = 'phone_sms';
		$this->sender = 'eHorizon';
		parent::__construct();
	}
	
	public function setRecipient($recipient)
	{
		$this->recipient = $recipient? $recipient : $this->recipient;
	}
	
	public function parsePhone()
	{
		$phones = str_replace(';', ',', $this->recipient);
		$phones = array_filter(explode(';',$phones));
		$recipient = [];
		foreach ($phones as $phone){
			$recipient[] = ((strpos(trim($phone),'+')!==0)? '+' : '').$phone;
		}
		$this->recipient = implode(';', $recipient);
		return $this->recipient;
	}	

	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$person_pk = Person::getInstance()->primaryKey();
		$person_id = $this->identity()->getPersonId();
	
		/* Fetch sms which apply to this user. If admin then fetch evrything */
		if($this->identity()->isAdmin()!==true)
		{
			$phones = Person::getInstance()->fetchColumn('phone',[$person_pk=>$person_id],true);
			
			$or_predicates = [new Equal('creator_usr_id', $this->identity()->getId(),'creator_usr_id')];
			foreach ($phones as $key=>$phone){
				$or_predicates[] = new Like('recipient','%'.$phone.'%', 'recipient'.$key);
			}
							
			$criteria[] = new NestedOr($or_predicates);
		}
	
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
		$box_pk = $this->box_type->primaryKey();
	
		for($i=0;$i<count($data);$i++)
		{
			$row=$data[$i];
			if(array_key_exists('sender', $row)==true)
			{
				$row['from'] = $row['sender'];
			}
			if(array_key_exists('recipient', $row)==true)
			{
				$row['to'] = $row['recipient'];
			}
			if(array_key_exists('action_time', $row)==true)
			{
				$row['time_ago'] = $this->time->timeAgo($row['action_time']);
			}
			if(array_key_exists($box_pk, $row)==true)
			{
				$row = array_merge($row,$this->box_type->selectOne($row[$box_pk]));
			}
			$data[$i] = $row;
		}	
		return $data;
	}
	
	public function sendSMS($message,$save_copy=false,$recipient=null){
		global $sms_config;
		
		$this->setRecipient($recipient);
		
		/* set timeout to avoid expiry */
		ini_set('max_execution_time', 0);
		
		/* initialize the sms gateway using name and api key*/
		$gateway = new AfricasTalkingGateway($sms_config['SMS_USERNAME'],$sms_config['SMS_APIKEY']);

		/* send sms to phones. separate numbers by commas */
		$phone = $this->parsePhone();
		
		$messages = [];
		
		/* send sms to phones specified save copy of sms if allowed*/
		try{
			$messages = (array)$gateway->sendMessage($phone, $message,$this->sender);
			$is_sent = 'Y';
			$this->message('SMS successfully sent to '.count($messages).' recipient(s)');
		}
		catch(AfricasTalkingGatewayException $e){
			$is_sent = 'N';
			$this->isError(true);
			$this->message('Error occured during sending. Please consult Admin.');
			$this->message($e->getMessage());
		}
		
		/* save results to the database if allowed */
		foreach($messages as $result)
		{
			$fields = ['recipient'=>$result->number,'status'=>$result->status,
					'message_id'=>$result->messageId,'cost'=>$result->cost,'sender'=>$this->sender];
			if($save_copy==true)
			{
				$code = ($is_sent=='Y')? 'ST' : 'OB';
				$box_pk = $this->box_type->fetchColumn($this->box_type->primaryKey(),['code'=>$code]);				
				$this->updateQueue(null,$message,$box_pk,$fields);
			}
		}
		return ($this->isError()==false);
	}
	
	public function updateQueue($sms_id=null,$message,$box_type=null,$fields=null)
	{
		$identity = $this->identity();		
		if($this->exists($sms_id)==false)
		{
			$owner_id = is_object($identity)==true? $identity->getId() : 1;
			$data = array_merge([
					'message'=> $message,
					'sender'=> $this->sender,
					'recipient'=> $this->recipient,					
					'sender_usr_id' => $owner_id,
					'action_time' => time(),
					'creator_usr_id' => $owner_id,
					'owner_id' => $owner_id,
					'created_at' => time(),
					$this->box_type->primaryKey() => $box_type
			],(array)$fields);	
			parent::save($data);						
		}
		else
		{
			$data = array_merge(['sender_usr_id' => $this->identity()->getId(),'action_time' => time()],$fields);
			parent::update($data,[$this->primaryKey()=>$sms_id]);
		}
	}
	
}
