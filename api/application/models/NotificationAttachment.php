<?php
class NotificationAttachment extends Document
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}

	public function __construct()
	{
		$this->table_name = 'notifications_attatchments';
		parent::__construct();
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		global $access_token;
		$identity = $this->identity();
		
		$notification = Notification::getInstance();
		$notification_pk = $notification->primaryKey();
		
		if(!array_key_exists($notification_pk, (array)$criteria)){			
			$criteria[$notification_pk] = $notification->fetchColumn($notification_pk,[]);			
		}
		
		$data= (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
				
		for($i=0;$i<count($data);$i++)
		{
			$row = $data[$i];
			if(isset($row['content']) && !isset($criteria[$this->primaryKey()]))
			{
				unset($row['content']);
				
				$person = Person::getInstance();
				$user = User::getInstance();
				$person_pk = $person->primarykey();
					
				$user_id = $notification->fetchColumn('creator_usr_id',[$notification_pk=>$row[$notification_pk]]);
				$person_id = $user->fetchColumn($person_pk,[$user->primaryKey()=>$user_id]);
					
				$column = $this->concat(['first_name','last_name'],' ').' AS full_name';
				$row['from'] = $person->fetchColumn($column,[$person_pk=>$person_id],false,'full_name');
				$row['person_id'] = $person_id;
			}
			
			if(array_key_exists($this->primaryKey(), $row)==true)
			{
				$row['link'] = API_ROOT.'/notification-attachments/download/'.$row[$this->primaryKey()].'?access_token=';
			}

			$data[$i] = $row;
		}		
		return $data;
	}
}
