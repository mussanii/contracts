<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a contract template
 */
class ContractTemplate extends OrganisationObject
{
	private static $_instance=null;
	
	private $contract_type;
	private $contract_area;
	private $organisation;
	private $organisation_unit;
	private $user;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='contracts_templates';
		parent::__construct();
		
		//capture instances of object associations
		$this->contract_area = ContractArea::getInstance();
		$this->contract_type = ContractType::getInstance();			
		$this->organisation_unit = OrganisationUnit::getInstance();
		$this->organisation = Organisation::getInstance();
		$this->user = User::getInstance();
	}
	
	/*
	 * @method select()
	 * @desc fetch detailed contracts including department, contract type and contract area
	 * @param array|NULL $criteria 
	 * @param array|NULL $columns 
	 * @return array $data;
	 */
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		//get original array source of parent
		$data = (array)parent::select($criteria,$columns,$group_by,$order_by,$limit);
		
		//capture foreign keys of each object
		$unit_pk = $this->organisation_unit->primaryKey();
		$type_pk = $this->contract_type->primaryKey();
		$area_pk = $this->contract_area->primaryKey();
		$user_pk = $this->user->primaryKey();

		/* populate additional fields */
		if(!$columns || count($columns)>1)
		{
			$count = count($data);
			for ($i=0;$i<$count;$i++)
			{	
				$row = $data[$i];
				
				if(isset($criteria[$this->primaryKey()]) && $count>1){
					$row['template_content'] = null;
				}
				
				if(isset($row['template_content'])){
					$match = '/(<title>)(.*?)(<\/title>)/i';
					$row['template_content'] = preg_replace($match, '', $row['template_content']);
				}
				
				/* populate functional unit info for each record */
				if(array_key_exists($unit_pk, $row)==true){
					$row['unit'] = (array)$this->organisation_unit->filterOne([$unit_pk=>$row[$unit_pk]]);
				}
				
				/* populate contract type info for each record */
				if(array_key_exists($type_pk, $row)==true){
					$row['type'] = (array)$this->contract_type->filterOne([$type_pk=>$row[$type_pk]]);
				}
				
				/* populate contract area info for each record */
				if(array_key_exists($area_pk, $row)==true){
					$row['area'] = (array)$this->contract_area->filterOne([$area_pk=>$row[$area_pk]]);
				}
				
				/* populate user detail area info for each record */
				if(array_key_exists('owner_id', $row)==true){
					$row['owner'] = (array)$this->user->filterOne([$user_pk=>$row['owner_id']]);
				}
				
				$data[$i] = $row;				
			}
		}
		return $data;
	}
	
	/*
	 * @method save()
	 * @desc save template details 
	 * @param array $data list, contains template data
	 */
	public function save($data)
	{
		global $identity;
		
		/* start transaction */
		$this->startTransaction();
	
		/* clean parent data */
		$parent = $this->sanitize($data);
		
		/* we do this for new records */
		if(array_key_exists($this->primaryKey(), $parent)==false || (int)$parent[$this->primaryKey()]==0){					
			$parent['owner_id'] = $identity->getId();
			$parent['template_title'] = $parent['template_title'].' '.time();
		}
		else{
			$data = array_merge((array)$this->selectOne($data[$this->primaryKey()]),$data);
			$parent = $this->sanitize($data);
		}
		$parent['created_on'] = time();
	
		/* save the parent record and retrieve process id */
		parent::save($parent);
		
		/* if NO errors encountered commit transaction */
		if($this->isError()!==true)
		{
			$this->endTransaction();
		}
	
		//finally return no.of records affected
		return ($this->isError()!==true)? $this->recordsAffected() : 0;
	}	
}