<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract Interface for a Yearly reminder range
 */


interface YearlyReminderRange {
    function sixthiethRange($id);
    function fourtyFiveRange($id);
}