<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2016 Software Technologies Ltd
 * @license Commercial
 * @version 1.0.0 
 * @final base class for a analytic
 */
class Analytic extends Contract
{
	private static $_instance=null;
	private $colors = ['#84ba5b','#d35e60','#808585','#9067a7','#ab6857','#ccc210','#396ab1','#e1974c','#ff1a75','#804000'];
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	/**
	 * @desc get short descriptive bar/pie or line graph data based on particular field
	 */
	public function simpleGraph($g_model,$criteria=null,$bar=true){
		$criteria = $criteria? $criteria : []; 	
		
	
		$pk = $this->primaryKey();
		$g_pk = $g_model->primaryKey();
		
		
	
		$chart = [];
	
		//fetch graph analytic columns
		foreach ((array)$g_model->select() as $row)
		{
			//count contracts in this status
			$value = intval($this->count(array_merge($criteria,[$g_pk=>$row[$g_pk]])));	
			$title = strtoupper($row['title']);	
			$href = WEB_ROOT.'/#/contracts/list?'.$g_pk.'='.$row[$g_pk];
			
			if(!$row['color_hexa'])
			{
				$i = isset($i)? $i : 0;
				$color = isset($this->colors[$i])? $this->colors[$i] : $this->randomColor();	
				$i++;			
			}
			else 
			{
				$color = $row['color_hexa'];
			}
			
			$chart_row = ($bar==true)? ['label'=>$title,'value'=>$value] : ['key'=>$title,'y'=>$value];	
			
			//compute colors here
			$rgba = $this->hex2rgba($color,true);
			$chart_row['color'] = $color; 
			$chart_row['color_light'] = $this->rgba2hex('rgba('.$rgba[0].','.$rgba[1].','.$rgba[2].')');
			$chart_row['color_dark'] = $this->rgba2hex('rgba('.$rgba[0].','.$rgba[1].','.$rgba[2].')');
			$chart_row['href'] = $href;
			
			$chart[] = $chart_row;
		}		
		return $chart;
	}
}
