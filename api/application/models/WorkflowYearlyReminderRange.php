<?php
/*
 * 
 * @author Kevin K. Musanii
 * @copyright 2019 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract Interface for a Yearly reminder range
 */


interface WorkflowYearlyReminderRange {
    function sixthiethRange($id);
    function fourtyFiveRange($id);
}