<?php
/*
 * 
 * @author kevin K. Musanii
 * @copyright 2020 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a contract location
 */
class Companie extends Modele
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='companies';
		parent::__construct();				
	}		
	
}
