<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for customer
 */
class Customer extends CustomModel
{	
	//associations/compositions
	private static $_instance=null;
	private $party;
	private $counter;
	private $party_addresse;
	private $user_role;
	private $user;
	private $person;
	private $role;
	private $organisation;
	private $companie;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='customers';		
		parent::__construct();		
		
		$this->party = Party::getInstance();
		$this->counter = Counter::getInstance();
		$this->party_addresse = PartyAddresse::getInstance();
		$this->user_role = UserRole::getInstance();
		$this->user = User::getInstance();
		$this->person = Person::getInstance();
		$this->organisation = Organisation::getInstance();
		$this->role = Role::getInstance();
		$this->companie= Model::getSentClientCompanyId();
	
	}	
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{		
		$order_by = $order_by? $order_by : $this->primaryKey().' DESC';
		$criteria=array_merge($criteria,["company_id"=>$this->companie] );

		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
		//die(var_dump($criteria));
		$party_key=$this->party->primaryKey();
		$person_key=$this->person->primaryKey();
		
		//columns exit and at least 2 or not specified at all
		if(($columns && count($columns)>=2) || !$columns)
		{
			for($i=0;$i<count($data);$i++)
			{
				$row=$data[$i];
								
				if(array_key_exists($party_key, $row)==true)
				{
					$row = array_merge($row,$this->party->selectOne($row[$party_key]));
					$row['customer_name'] = ($row['full_name'])? $row['full_name'] : $row['first_name'].' '.$row['last_name'];
					$row['customer_phone'] = $row['phone'];		
					$row['customer_email'] = $row['primary_email'];
					
					$columns = ['b.'.$person_key.' AS '.$person_key,'a.'.$party_key.' AS '.$party_key,'first_name',
								'last_name','gender','b.phone AS phone','b.primary_email AS primary_email'];
					$row['person'] = $this->party->selectOne($row['contact_party_id'],$columns);
					$row['address'] = $this->party_addresse->getAddressList([$this->party->primaryKey()=>$row[$this->party->primaryKey()]]);
				}		
				
				if(array_key_exists($this->organisation->primaryKey(), $row)==true){
					$row['icon'] = API_ROOT.'/organisation-images/download/'.$row[$this->organisation->primaryKey()].'?access_token=';
				}
				
				$data[$i] = $row;
			}	
		}
		return $data;
	}	
	
	public function save($data)
	{
		global $identity;
		//start transaction here
		$this->startTransaction();
		
		//save person and  party data and retrieve party_id	
		$contact_p = $data['person'];	
		$contact_p['party_type'] = 'P';
		$data['party_type'] = 'O';
		$company = $data;
		$contact_party_id = isset($data['contact_party_id'])? $data['contact_party_id'] : null;		
		$contact_p['user_id'] = isset($contact_p['user_id'])? $contact_p['user_id'] : null;
		$contact_p['person_id'] = isset($contact_p['person_id'])? $contact_p['person_id'] : null;
		
		$owner_id = is_object($identity)==true? $identity->getId() : 1;
		
		$records_affected = $this->party->save($data);
				
		if(intval($this->party->lastAffectedId())>0)
		{
			$customer_party_id = $this->party->lastAffectedId();
			
			$party_pk = $this->party->primaryKey();
			
			//save contact person here
			$contact_p[$party_pk] = $contact_party_id;
			$this->party->save($contact_p);	
			
			if($this->party->isError()==true)
			{
				$this->RollBack();
				$this->message($this->party->message());
				return 0;
			}
			$contact_party_id = (int)$this->party->lastAffectedId()>0? $this->party->lastAffectedId() : $contact_party_id;
			
			//collect customer data and some other data along
			$customer_data = $this->sanitize($data);
			
			if(!$customer_data[$this->primaryKey()])
			{
				$customer_data['owner_id'] = $owner_id;
				$customer_data['customer_no'] = $this->counter->nextCounter('CU');
				$customer_data['effective_from'] = time();
				
				//save contact person user details
				$user_detail = $this->user->select([$this->person->primaryKey()=>$contact_p['person_id']]);
				//$user_detail['username'] = isset($contact_p['username'])? $contact_p['username'] : $contact_p['first_name'].$this->randomize(5);
				$firstChar = substr($contact_p['first_name'],0,1);
				$user_detail['username'] = isset($contact_p['username'])? $contact_p['username'] : ucfirst($firstChar).$contact_p['last_name'];
				
				$user_detail[$this->user->primaryKey()] = $contact_p[$this->user->primaryKey()];
				//$user_detail['email'] = $contact_p['primary_email'];
				$user_detail['email'] = isset($contact_p['primary_email'])?$contact_p['primary_email']:null;
				$user_detail['effective_from'] = time();
				$user_detail['password'] = $this->user->getPasswordSalt($user_detail['email'].time());
				
				$party_criteria = [$this->party->primaryKey()=>$contact_party_id];
				$p_column = $this->person->primaryKey();
				$user_detail[$this->person->primaryKey()] = $this->party->fetchColumn('a.'.$p_column,$party_criteria,false,$p_column);
				$this->user->save($this->user->sanitize($user_detail));	
				
				if($this->user->isError()==true || $this->user->recordsAffected()==0)
				{
					$this->RollBack();
					$this->message($this->user->message());
					return 0;
				}
				else 
				{
					//save customer/client role here
					$role_det = [];
					$role_det[$this->role->primaryKey()] = $this->role->fetchColumn($this->role->primaryKey(),['code'=>'CS']);
					$role_det[$this->user->primaryKey()] = $this->user->lastAffectedId();
					$role_det['effective_from'] = time();
					$this->user_role->save($role_det);
					
					//send email on new account created
					$u_criteria = ['username'=>$user_detail['username']];
					$recipient=$this->user->selectOne($this->user->lastAffectedId()); 
					$subject = "New user account at ".SITE_NAME;
					$email=$recipient['primary_email'];
					$name = $recipient['first_name'].' '.$recipient['last_name'];
					
					//save password reset code
					$reset_code = $this->getResetPasswordCode($email);
					$this->user->update(['reset_password'=>$this->user->getPasswordSalt($reset_code)],$u_criteria);
					$body = $this->getEmailNewUserAccount($email, $name, $reset_code);
					
					//send to contact person
					$this->mailer()->setSender();
					if($email){
						$this->mailer()->setRecipient($email);

					}
					if(!empty($company['primary_email'])){
						$this->mailer()->setCc($company['primary_email']);

					}
					
										
					$this->mailer()->sendEmail($body, $subject,true);						
				}			
			}			
			
			$customer_data[$this->user->primaryKey()] = $owner_id;		
			$customer_data['contact_party_id'] = $contact_party_id;
			$customer_data[$party_pk] = $customer_party_id;
			
			//save customer data here
			parent::save($customer_data); 
		}
		
		if($this->party->isError()==true)
		{
			$this->RollBack();
			$this->message($this->party->message());
			return 0;
		}
		
		if((int)$this->recordsAffected()==0 || (int)$this->lastAffectedId()==0)
		{
			//roll back whole transaction if error
			$this->RollBack();
			$this->message($this->user->message());
			return 0;
		}
		else
		{	
			//end transaction here
			$this->endTransaction();					
			return (!$this->is_error && $records_affected>0)? $this->recordsAffected() : 0;
		}
	}
	
	public function delete($criteria)
	{
		$party_id = $this->fetchColumn($this->party->primaryKey(), $criteria);
		$contact_party_id = $this->fetchColumn('contact_party_id', $criteria);
		
		$criteria_org = [$this->party->primaryKey()=>$party_id];
		$organisation_id = $this->party->fetchColumn('a.'.$this->organisation->primaryKey(),$criteria_org,false,$this->organisation->primaryKey());
		
		$criteria_person = [$this->party->primaryKey()=>$contact_party_id];
		$contact_person_id = $this->party->fetchColumn('a.'.$this->person->primaryKey(),$criteria_person,false,$this->person->primaryKey());
		
		$this->startTransaction();
		
		//delete customer record
		parent::delete($criteria);
		
		//delete organisation record
		$this->organisation->delete([$this->organisation->primaryKey()=>$organisation_id]);
		
		if($this->person->isError()==true){
			$this->message($this->person->message());
			$this->RollBack();
			return  $this->recordsAffected(0);			
		}
		
		//delete contact person record
		$this->person->delete([$this->person->primaryKey()=>$contact_person_id]);
		
		if($this->person->isError()==true){
			$this->message($this->person->message());	
			$this->RollBack();
			return $this->recordsAffected(0);
		}
		
		$this->endTransaction();
		
		return ((int)$this->recordsAffected()>0)? $this->recordsAffected() : 0;
	}
	
	public function getOrganisationId($customer_id)
	{
		$party_pk = $this->party->primaryKey();
		$org_pk = $this->organisation->primaryKey();
		$party_id = $this->fetchColumn($party_pk,[$this->primaryKey()=>$customer_id]);
		$data = $this->party->selectOne($party_id,['a.'.$org_pk.' AS '.$org_pk]);
		return isset($data[$org_pk])? $data[$org_pk] : null;
	}
	
}