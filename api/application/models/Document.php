<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract base class for a document
 */
class Document extends File
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}	
	
	public function videoFrame($row)
	{
		$image = getimagesizefromstring($row['content']);
		$type = Helpers::getFileType($image['mime']);
		
		if($type['id']==3) //images
		{
			$row['thumb_url'] = API_ROOT.'/notification-attachments/download/'.$row[$this->primaryKey()].'?access_token=';
		}	
		elseif($type['id']==7) //audio
		{
			$row['thumb_url'] = WEB_ROOT.'/resources/img/audio.png';			
		}	
		elseif($type['id']==8) //video
		{
			$row['thumb_url'] = WEB_ROOT.'/resources/img/video.png';
		}
		else //others
		{
			$row['thumb_url'] = WEB_ROOT.'/resources/img/file.png';
		}
		
		return $row;
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
		//columns exit and at least 2 or not specified at all
		
		if(($columns && count($columns)>=2) || !$columns)
		{
			for($i=0;$i<count($data);$i++)
			{
				$row=$data[$i];
				if(array_key_exists($this->primaryKey(), $row)==true && array_key_exists('content',$row)==true){
					$row = $this->videoFrame($row);
				}
				$data[$i] = $row;
			}
		}
				
		return $data;
	}
}
