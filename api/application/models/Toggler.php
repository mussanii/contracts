<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a toggler
 */
class Toggler extends CustomModel
{		

	protected function toggle($data,$fr_keys=[])
	{
		$all_keys = $fr_keys;
		$all_keys[] = 'active';
		$criteria_sub = $this->arraySubset($data,$all_keys);
						
		/*.....disable when user says so, else enable last record..... */
		if($criteria_sub['active']!='Y')
		{
			/*...update all previous records...*/
			$active_criteria = array_merge($criteria_sub,['active'=>'Y']);
			if($this->exists($active_criteria)==true)
			{
				$update_criteria = $this->arraySubset($data, $fr_keys);
				$data = ['effective_to' => time(),'active'=>'N'];
				return parent::update($data,$update_criteria);
			}
		}
		else
		{
			/*....if someone has already enabled it, just ignore....*/
			if($this->exists($criteria_sub)!=true)
			{
				$data['effective_from'] = time();
				$data['active'] = 'Y';
				return parent::save($data);
			}
		}				
		return 0;
	}
}
