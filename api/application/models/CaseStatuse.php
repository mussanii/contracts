<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a case status
 */
class CaseStatuse extends CustomModel
{		
	//associations/compositions	
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='cases_status';
		parent::__construct();				
	}		
	
	public function formatTime($data)
	{
	    if(isset($data['date_reported']))
	    {
	       $data['date_reported'] = (isset($data['date_reported']) && trim($data['date_reported']))? $this->time()->dateToTimestamp($data['date_reported']) : time();
	    }
	    return $data;
	}
	
	public function update($data,$criteria=null)
	{
	    $data = $this->formatTime($data);
	    return parent::update($data,$criteria);
	}
	
	public function save($data)
	{
	    $data = $this->sanitize($data);
	    $data = $this->formatTime($data);
	    return parent::save($data);
	}
}
