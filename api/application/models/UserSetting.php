<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @final specialized for a user setting
 */
class UserSetting extends CustomModel
{	
	//associations/compositions	
	private $setting;
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
		
	public function __construct()
	{		
		$this->table_name ='users_settings';
		$this->setting = Setting::getInstance();
		parent::__construct();				
	}

	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		global $identity;
		
		$user = User::getInstance();
		$settings_key = $this->setting->primaryKey();
		$columns = $columns? array_merge($columns,[$settings_key]) : $this->setting->getColumns();		
		$data = $this->setting->select($criteria,$columns,$group_by,$order_by,$limit);		
		$user_key = $user->primaryKey();
		$user_id = (is_object($identity)==true && (int)$identity->getId()>0)? $identity->getId() : 1;

		if(!$columns || $columns>1)
		{
			for($i=0;$i<count($data);$i++)
			{
				$row = $data[$i];
				$user_row = parent::select([$settings_key => $row[$settings_key],$user_key=>$user_id]);
				$blank_row = [$user_key=>$user_id];			
				$row = (is_array($user_row)==true)? array_merge($row,$user_row[0]) : array_merge($row,$blank_row);	
				if(array_key_exists('possible_values', $row)==true){
					$row['possible_values'] = array_filter(explode(',', $row['possible_values']));
				}
				$data[$i] = $row;			
			}	
		}		
			
		return $data;
	}
	
	public function reset()
	{
		global $identity;
		$user_key = $identity->primaryKey();
		$user_id = $identity->getId();
		$settings_key = $this->setting->primaryKey();
		$data = $this->setting->select([]);
		for($i=0;$i<count($data);$i++)
		{
			parent::update(['value'=>$data[$i]['value']],[$user_key=>$user_id,$settings_key=>$data[$i][$settings_key]]);
		}
	}
	
	public function save($data)
	{
		global $identity;
		$user_key = $identity->primaryKey();
		$user_id = $identity->getId();
		$settings_key = $this->setting->primaryKey();		
		$pk_val = parent::select([$settings_key => $data[$settings_key],$user_key=>$user_id],[$this->primaryKey()]);
		$pk_val = (is_array($pk_val)==true)? $pk_val[0][$this->primaryKey()] : null;
		$data[$user_key] = $user_id ;
		$data[$this->primaryKey()] = $pk_val;
		return parent::save($data);
	}	
}