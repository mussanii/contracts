<?php

class UserImport extends CustomPhpExcel
{
	//static to help reuse same instance
	private static $_instance=null;
	
	protected $person;
	protected $user;
	protected $role;
	
	protected  $max_rows=1000;
	private $default_currency='KES';
	private $default_country='KENYA';
	private $default_city='NAIROBI';
	
	private $rows_uploaded;
	private $records;

	/*compulsory columns for contract master data */
	private $table_columns=[
			'A'=>['id'=>'username','label'=>'USERNAME*'],
			'B'=>['id'=>'full_name','label'=>'FULL NAME*'],
			'C'=>['id'=>'primary_email','label'=>'EMAIL*'],
			'D'=>['id'=>'primary_phone','label'=>'PHONE*'],
	];
	
	/* ensures that only one instace exists per class */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	/* construct class and setup a few associations to get us started */
	public function __construct()
	{
		parent::__construct();
		$this->person = Person::getInstance();		
		$this->user = User::getInstance();	
		$this->role = Role::getInstance();
	}
	
	/**
	 * @method exportWorkBook()
	 * @desc export workbook so that it can be downloaded via browser
	 * @param string $type file type short code e.g. xls,xlsx,ods etc
	 * @return array file attributes and content
	 */
	public function exportWorkBook($type)
	{
		$extension='.'.$type;

		//based on "short file type" code determine the writer and mime type
		$this->setWriterAndType($type,'users','application/zip');

		//create contract sheet
		$this->populateSheet(0,'Users',$this->table_columns);

		//write to excel and save file
		$this->writeExel();
		
		//obtain file contents
		$content = file_get_contents($this->target.DIRECTORY_SEPARATOR.$this->file_name);
		
		//return file attributes and content
		return ['content'=>$content,'type'=>$this->mime_type,'name'=>$this->file_name];
	}
		
	/**
	 * @method importWorkBook()
	 * @desc import workbook uploaded via browser extract and insert records to db
	 * @return array file attributes and error messages
	 */
	public function importWorkBook()
	{
		$file = $this->getUploadedFileMeta();
		$is_error = false;
		$count = 0;
		
		if($file && $file['error']==false)
		{
			$excel=\PHPExcel_IOFactory::load($file['tmp_name']); 
			
			//loop through rows in excel and insert to database
			$count = $this->saveData($excel->getSheet(0));
			$is_error = $this->user->isError();
			$error_msg = $this->user->isError()==true? $this->user->message() : null;
		}
		else
		{
			$error_msg = $file['error'];
		}
		
		/* send batch emails for successful user accounts created */
		if($is_error!==true && $count>0)
		{
			foreach ($this->records as $user)
			{
				$this->user->sendResetEmail($user['id'],$user['name']);
			}
			
			/* inform admin about the uploads */
			$this->user->sendMailBatchUpload('User','Uploaded',SITE_URL,$count);
		}
		
		$file['message']= $is_error==true? $error_msg : $this->uploadSuccessMessage();
		$file['title'] = TITLE_UPLOAD_OPERATION;
		$file['status'] = $is_error==true? STATUS_ERROR : STATUS_SUCCESS;
		$file['success'] = $is_error==true? 0 : 1;
		$file['tmp_name'] = null;
				
		return $file;
	}
	
	private function uploadSuccessMessage()
	{
		if($this->rows_uploaded>0)
			$message = $this->rows_uploaded.' User(s) were successfully uploaded';
		else 
			$message = ' No user(s) were uploaded. Check Excel File';
		return $message;
	}
	
	/**
	 * @method saveData()
	 * @desc save master info onto user records
	 * @param PHPExcel_Worksheet $worksheet
	 * @return array $inserted details of inserted records
	 */
	private function saveData(\PHPExcel_Worksheet $worksheet)
	{
		$this->records = [];
		$this->rows_uploaded = 0;	
		$this->user->startTransaction();
		
		for ($row = 2; $row <= $worksheet->getHighestRow(); ++ $row) 
		{	
			$row_data = []; 
			foreach ($this->table_columns as $col_key=>$value)
			{
				//get table column name
				$field_name=$value['id'];

				//extract $val and $type
				extract($this->getCellAttributes($worksheet, $row, $col_key));
					
				//assemble values in this array
				$row_data[$field_name] = $val;			
			}
			
			//filter data. if column count is zero, then continue to next row
			$row_data = array_filter($row_data);
			if(count($row_data)==0) continue;
			
			//save data to database and proceed
			$person_data = $this->extractRowData($row_data);	
			$person_data['is_admin'] = 'N';
			$role_id = $this->role->fetchColumn($this->role->primaryKey(),['code'=>'PA']);
			
			//save cloned user data here and check errors
			$clone = $this->user->cloneRecord(['username'=>'sa'],$person_data,$role_id,true);
			if($this->user->isError()==true)
			{
				$this->user->RollBack();
				$this->rows_uploaded = 0;
				$this->user->message('Worksheet '.$worksheet->getTitle().':'.$this->user->message());
				break;
			}	
			else
			{
				$this->records[] = ['id'=>$this->user->lastAffectedId(),'name'=>$clone['username'],'email'=>$clone['primary_email']];
			}		
			$this->rows_uploaded += $this->user->recordsAffected();			
		}
		
		if($this->user->isError()!=true)
		{
			$this->user->endTransaction();
		}
			
		return $this->rows_uploaded;
	}
	
	/**
	 * @method extractRowData()
	 * @desc extract output row from data supplied from excel
	 * @param array $row_data current row as supplied from from excel
	 */
	private function extractRowData($row_data)
	{
		$output = [];
		foreach ($this->table_columns as $col_key=>$value)
		{
			//get table column name
			$field_name=$value['id'];
			if($field_name=='full_name' && isset($row_data['full_name']))
			{
				$name = array_filter(explode(' ', $row_data['full_name']));
				$output['first_name'] = array_shift($name);
				$output['last_name'] = array_pop($name);
				$output['other_name'] = implode(' ',$name);
			}
			$output['username'] = $row_data['username'];
			$output['primary_phone'] = $row_data['primary_phone'];
			$output['primary_email'] = $row_data['primary_email'];
		}
		
		return $output;		
	}
	
}
