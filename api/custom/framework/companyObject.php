<?php
/*
 * 
 * @author Kevin  K Musanii
 * @copyright 2020 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a company object
 * Use this class  on special common classes to all the companies
 */
class CompanyObject extends CustomModel
{		

	public function save($data)
	{
		$identity = $this->identity();
		$organisation = Organisation::getInstance();
			
	

		if(!isset($data[$this->primaryKey()]) || (isset($data[$this->primaryKey()]) && intval($data[$this->primaryKey()])==0))
		{
			$payload = $identity->getPayload();
			
			$data[$organisation->primaryKey()] = $payload['owner'][$organisation->primaryKey()];
			
		}
		return parent::save($data);
		
		echo($data);
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$identity = $this->identity();
		$organisation = Organisation::getInstance();
			

		$payload = $identity->getPayload();
		
		$criteria[$organisation->primaryKey()] = $payload['owner'][$organisation->primaryKey()];
		
		

		
			
					
		return parent::select($criteria,$columns,$group_by,$order_by,$limit);

	}
}
