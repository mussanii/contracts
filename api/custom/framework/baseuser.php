<?php
class BaseUser extends CustomModel
{
    private $password_salt;
    private $token_key;
    private $user_id;
    private $is_super;
    private $username;
    private $full_name;
    private $roles;
    private $role_default;
    private $person_id;
    
    protected $person;
    protected  $user_role;
    protected $role;
    protected $organisation;
    protected $payload;
    
    
    public function __construct()
    {
        $this->table_name='users';
        $this->token_key=TOKEN_KEY;
        $this->person = Person::getInstance();
        $this->user_role = UserRole::getInstance();
        $this->role = Role::getInstance();
        $this->organisation = Organisation::getInstance();
        parent::__construct();
    }
    
    public function getTokenKey()
    {
        return $this->token_key;
    }
    
    public function getId()
    {
        return $this->user_id;
    }
    
    public function getPayload()
    {
        return $this->payload;
    }
    
    public function getPersonId()
    {
        return $this->person_id;
    }
    
    public function isSuper()
    {
        return $this->is_super;
    }
    
    public function getFullName()
    {
        return $this->full_name;
    }
    
    public function getDefaultRole()
    {
        return $this->role_default;
    }
    
    
    /**
     * @method isCustomer()
     * @desc check if user is an customer/client and active
     * @return boolean
     */
    public function isCustomer()
    {
        $identity = $this->identity();
        $role_ids = $this->getMyRoles($identity->getId(),true);
        $exists = $this->role->exists([$this->role->primaryKey()=>$role_ids,'code'=>'CS']);
        return $exists;
    }
    
    /*
     * @method isAdmin()
     * @desc check if user is an admin and active
     * @return boolean
     */
    public function isAdmin()
    {
        global $identity;
        $role_ids = $this->getMyRoles($identity->getId(),true);
        $exists = $this->role->exists([$this->role->primaryKey()=>$role_ids,'code'=>'AM']);
        return $exists;
    }
    
    /**
     * @method getAdmin()
     * @desc get company's admin - client or vendor's admin details
     */
    public function getAdmin()
    {
        return $this->filterOne(['is_admin'=>'Y']);
    }
    
    public function getName()
    {
        return $this->username;
    }
    
    protected function getRoles()
    {
        return $this->roles;
    }
    
    public function getMyRoles($user_id,$ids_only=false,$as_array=false)
    {
        $criteria = [$this->primaryKey()=>$user_id,'active'=>'Y'];
        if($ids_only==true)
        {
            return $this->user_role->fetchColumn($this->role->primaryKey(),$criteria,$as_array);
        }
        else
        {
            return $this->user_role->select($criteria,[$this->role->primaryKey()]);
        }
    }
    
    public function setPayload($username=null)
    {
        if($username)
        {
            $data=$this->filterOne(['username'=>$username]);
            if($data)
            {
                $this->username=$data['username'];
                $this->user_id=$data['user_id'];
                $this->is_super = $data['is_super'];
                $this->person_id=$data['person_id'];
                $this->full_name=$data['first_name'].' '.$data['last_name'];
                $this->roles = $this->getMyRoles($this->user_id);
                $role_pk = $this->role->primaryKey();
                $this->role_default = isset($this->getRoles()[0][$role_pk])? $this->getRoles()[0][$role_pk] : null;
            }
        }
    }
    
    public function save($data)
    {
        $pk = $this->primaryKey();
        if(($this->isCustomer()==true) && (array_key_exists($pk, $data)==false || (int)$data[$pk]==0))
        {
            $party = Party::getInstance();
            $customer = Customer::getInstance();
            
            $party_pk = $party->primaryKey();
            $customer_pk = $customer->primaryKey();
            $organisation_pk = $this->organisation->primaryKey();
            
            $organisation_id = $this->getPayload()['organisation'][$organisation_pk];
            $party_id = $party->fetchColumn($party_pk,['a.'.$organisation_pk=>$organisation_id]);
            $data[$customer_pk] = $customer->fetchColumn($customer_pk,[$party_pk=>$party_id]);
            
            $rows_affected = parent::save($data);
            $role_id = $this->role->fetchColumn($this->role->primaryKey(),['code'=>'CS']);
            $this->saveRoles($role_id, $this->lastAffectedId());
            return $rows_affected;
        }
        else
        {
            return parent::save($data);
        }
    }
    
    public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
    {
        global $access_token;
        global $identity;
        
        /* find out if criteria used referes to currently logged in person and use criteria as is
         * else user has identity select only records that are children of the current user*/
        if($this->identity() && $this->identity()->isAdmin()!==true)
        {
            $person_pk = $this->person->primaryKey();
            
            $u = (isset($criteria[$this->primaryKey()]) && $criteria[$this->primaryKey()]==$this->identity()->getId());
            $p = (isset($criteria[$person_pk]) && $criteria[$person_pk]==$this->identity()->getPersonId());
            $w = ((isset($criteria[$this->primaryKey()]) || isset($criteria[$person_pk])) && count($criteria)==1);
            
            if($u==false && $p==false && $w==false)
            {
                $criteria['id_parent'] = $this->identity()->getId();
            }
        }
        
        $order_by = $order_by? $order_by : $this->primaryKey().' DESC';
        
        $data=parent::select($criteria,$columns,$group_by,$order_by,$limit);
        $person_key=$this->person->primaryKey();
        $count = count($data);
        
        for($i=0;$i<$count;$i++)
        {
            $row=$data[$i];
            
            /* remove session token in record to avoid long records */
            if(isset($row['session_token']) && count($row)>1) unset($row['session_token']);
            
            if(!$columns || count($columns)>2)
            {
                if(array_key_exists($this->primaryKey(), $row)==true)
                {
                    $row['roles'] = ['id' => $this->getMyRoles($row[$this->primaryKey()],true,true)];
                }
                
                if(array_key_exists($person_key, $row)==true)
                {
                    $person_data = $this->person->selectOne($row[$person_key]);
                    $row = array_merge($row,$person_data? $person_data : []);
                    $icon_url = API_ROOT.'/person-images/download/'.$row[$person_key].'?t='.time();
					
					$row['small_url'] = $icon_url.'&dimension=small&access_token=';
					$row['thumb_url'] = $icon_url.'&dimension=thumb&access_token=';
					$row['large_url'] = $icon_url.'&dimension=large&access_token=';
					
					$row['icon'] = $icon_url.'&access_token=';
					$row['signature'] = API_ROOT.'/person-signatures/download/'.$row[$person_key].'?t='.time().'&access_token=';
					$row['first_name'] = isset($row['first_name']) ? $row['first_name']:'';
                    $row['last_name'] = isset($row['last_name']) ? $row['last_name']:'';
					$row['full_name'] = $row['first_name'].' '.$row['last_name'];
					
                    $row = $this->removePasswords($row);
                    
                    $row['organisation'] = $this->getLoggedInEntity($row[$person_key]);
                }
            }
            
            $data[$i]= $row;
        }
        
        return $data;
    }
    
    public function getDashboard($role_id)
    {
        $user = User::getInstance();
        $role = Role::getInstance();
        $object = Object::getInstance();
        $role_object = RoleObject::getInstance();
        $object_type = ObjectType::getInstance();
        
        $object_ids = $role_object->fetchColumn($object->primaryKey(),[$role->primaryKey()=>$role_id,'active'=>'Y']);
        $type_id = $object_type->fetchColumn($object_type->primaryKey(),['code'=>'DB']);
        $result = $object->filterOne([$object_type->primaryKey()=>$type_id,$object->primaryKey()=>$object_ids]);
        
        return $result;
    }
    
    public function getChildUserId($user_id,$role_code=null)
    {
        $user_fk = $this->primaryKey();
        $role_fk = $this->role->primaryKey();
        
        $criteria = ($role_code)? ['code'=>$role_code] : null;
        
        $role_id = $this->role->fetchColumn($role_fk,$criteria);
        
        $user_id = $this->fetchColumn($user_fk,['id_parent'=>$user_id]);
        
        $result = $this->user_role->fetchColumn($user_fk,[$user_fk=>$user_id,$role_fk=>$role_id,'active'=>'Y'],true);
        
        return array_unique(array_filter($result));
    }
	
	
	/**
	 * @method getMyChildren($user_id)
	 * @desc get all user ids for the user id supplied
	 * @param string $user_id unique user's id 
	 * @return array ids or values
	 */
	public function getChildren($user_id)
	{
	    $user_fk = $this->primaryKey();
	    $children = $this->fetchColumn($user_fk,['id_parent'=>$user_id],true);
	    return array_unique(array_filter($children));
	}
	
    
    /**
     * @method getUsernameByEmail()
     * @desc get user by his/her email/username or phone
     * @param string $user_or_email can be a username/email/phone
     * @return string $username
     */
    public function getUsernameByEmail($user_or_email)
    {
        if(!filter_var($user_or_email, FILTER_VALIDATE_EMAIL) === false)
        {
            $id = $this->person->fetchColumn($this->person->primaryKey(),['primary_email'=>$user_or_email]);
            $name = $this->fetchColumn('username',[$this->person->primaryKey()=>$id]);
            $user_or_email = $name? $name : $user_or_email;
        }
        return $user_or_email;
    }
    
    public function removePasswords($data)
    {
        unset($data['password']);
        unset($data['reset_password']);
        unset($data['confirm_password']);
        
        return $data;
    }
    
    public function getPasswordSalt($password)
    {
        return md5($password.PASSWORD_SALT);
    }
    
    public function getLoggedInEntity($person_id=null)
    {
        $person = Person::getInstance();
        $party = Party::getInstance();
        $customer = Customer::getInstance();
        $customer_pk = $customer->primaryKey();
        
        if($person_id)
        {
            //find if contact party by stated person id exists, then find its customer parent record
            $contact_party_id = $party->fetchColumn($party->primaryKey(),['a.'.$this->person->primaryKey()=>$person_id]);
            $customer_id = $this->filterOne([$person->primaryKey()=>$person_id],[$customer_pk]);
            
            if($contact_party_id || $customer_id)
            {
                $criteria = $contact_party_id?  ['contact_party_id'=>$contact_party_id] : [$customer_pk=>$customer_id];
                $organization = $customer->filterOne($criteria);
                if(!$organization){
                    $organization = $this->organisation->filterOne(['is_owner'=>'Y']);
                }
            }
            else
            {
                $organization = $this->organisation->filterOne(['is_owner'=>'Y']);
            }
        }
        else
        {
            $organization = $this->organisation->filterOne(['is_owner'=>'Y']);
        }
        
        //if no contact person specify administrator of organisation
        if(!isset($organization['person']))
        {
            $organization['person'] = $person->selectOne(1);
        }
        return $organization;
    }
    
    public function generateToken()
    {
        $user = $this->selectOne($this->getId());
        
        $payload = [
            'user_id'=>$this->getId(),
            'is_super'=>$user['is_super'],
            'username'=>$user['username'],
            'full_name'=>$user['full_name'],
            'user_role'=>$this->getRoles(),
            'icon' => $user['icon'],
            'first_name' => $user['first_name'],
            'last_name' => $user['last_name'],
            'phone' => $user['phone'],
            'primary_email' => $user['primary_email'],
            'dob' => $user['dob'],
            'gender' => $user['gender'],
            'initials' => $user['initials'],
            'city_town' => $user['city_town'],
            'country' => $user['country'],
            'role' => $this->role_default,
            'dashboard' => $this->getDashboard($this->role_default),
            'person_id'=>$this->getPersonId(),
            'organisation' => $this->getLoggedInEntity($this->getPersonId()),
            'owner' => $this->organisation->filterOne(['is_owner'=>'Y'])
        ];
        
        //generate token
        $token = JWT::encode($payload, $this->getTokenKey(),'HS256');
        
        //save token for this user
        parent::update(['session_token'=>$token],['user_id'=>$this->getId()]);
        
        return $token;
    }
    
    public function getTokenPayLoad($access_token)
    {
        $payload = null;
        try
        {
            $payload = json_encode(JWT::decode($access_token, $this->getTokenKey(),['HS256']));
            $payload = json_decode($payload,true);
        }
        catch (\Exception $ex)
        {
            $this->isError(true);
            $this->message($ex->getMessage());
        }
        $this->payload = $payload;
        return $payload;
    }
    
    public function getAuthorisation($role_id)
    {
        $role = Role::getInstance();
        $object = Object::getInstance();
        $role_object = RoleObject::getInstance();
        $object_type = ObjectType::getInstance();
        $role_permission = RolePermission::getInstance();
        $object_action = ObjectAction::getInstance();
        
        $types = $object_type->select();
        
        $data = [];
        
        $action_ids = $role_permission->fetchColumn($object_action->primaryKey(),[$role->primaryKey()=>$role_id,'active'=>'Y']);
        $object_ids = $role_object->fetchColumn($object->primaryKey(),[$role->primaryKey()=>$role_id,'active'=>'Y']);
        
        //stores all objects as flattened multi-associative array/object
        $all = [];
        $paths = [];
        
        foreach ($types as $type)
        {
            
            $type_key = $object_type->primaryKey();
            $columns = $object->getColumns();
            $columns[] = $object->coalesce('id_parent',0);
            $results = (array)$object->select([$type_key=>$type[$type_key],$object->primaryKey()=>$object_ids],$columns);
            
            //obtain object permissions here
            $objects = [];
            for($i=0;$i<count($results);$i++)
            {
                $row = $results[$i];
                
                $columns = $object_action->getColumns();
                $columns[] = $object_action->coalesce('id_parent',0);
                $criteria = [$object_action->primaryKey()=>$action_ids,$object->primaryKey()=>$row[$object->primaryKey()]];
                $actions = (array)$object_action->select($criteria,$columns);
                $permissions = null;
                foreach($actions as &$action)
                {
                    $permissions[strtolower($action['code'])] = $action;
                    $paths[] = $action['path_ref'];
                }
                
                $paths[] = $row['path_ref'];
                
                $row['actions'] = $permissions? $permissions : new stdClass();
                $objects[strtolower($row['code'])] = $row;
                $all[strtolower($row['code'])] = $row;
            }
            
            $tree = $objects? Helpers::getTree($objects,'id_parent',$object->primaryKey()) : [];
            $data[strtolower($type['code'])] = $tree;
        }
        $data['all'] = $all;
        $data['paths'] = array_values(array_unique(array_filter($paths)));
        return $data? $data : [];
    }
    
    /*
     * @method saveRoles()
     * @desc save roles for a user
     * @param unknown $role_ids_posted list of posted role ids
     * @param unknown $user_id the primary id of the user
     * @return number count of records successfully inserted
     */
    public function saveRoles($role_ids_posted,$user_id)
    {
        $role_ids_posted = is_array($role_ids_posted)==true? $role_ids_posted : [$role_ids_posted];
        $records_affected = 0;
        $criteria = [$this->primaryKey()=>$user_id,'active'=>'Y'];
        $role_ids_db = $this->user_role->fetchColumn($this->role->primaryKey(),$criteria,true);
        
        $role_ids_expired = array_diff($role_ids_db, $role_ids_posted);
        $role_ids_new = array_diff($role_ids_posted,$role_ids_db);
        
        //flag expired roles for user id i.e. active =N, change in 'effective to' to current date
        foreach ($role_ids_expired as $role_id)
        {
            $criteria = [$this->primaryKey()=>$user_id,$this->role->primaryKey()=>$role_id];
            $this->user_role->update(['active'=>'N','effective_to'=>time()],$criteria);
            $records_affected = $records_affected + $this->user_role->recordsAffected();
        }
        
        //save new roles against the user id
        foreach ($role_ids_new as $role_id)
        {
            $data = [$this->primaryKey()=>$user_id,$this->role->primaryKey()=>$role_id,'effective_from'=>time()];
            $this->user_role->save($data);
            $records_affected = $records_affected + $this->user_role->recordsAffected();
        }
        return $records_affected;
    }
    
    public function delete($criteria)
    {
        $this->startTransaction();
        $person_id = $this->fetchColumn($this->person->primaryKey(), $criteria);
        parent::delete($criteria);
        $this->person->delete([$this->person->primaryKey()=>$person_id]);
        if($this->person->isError()==true){
            $this->message($this->person->message());
            $this->isError($this->person->isError());
            $this->recordsAffected(0);
        }
        $this->endTransaction();
        return ((int)$this->recordsAffected()>0)? $this->recordsAffected() : 0;
    }
}
