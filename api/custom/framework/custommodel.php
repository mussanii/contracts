<?php
class CustomModel extends Model 
{
	public function getPasswordLink($email,$reset_password)
	{
		global $COMPANY_CODE;
	
		$user = User::getInstance();
	
		$payload = ['email'=>$email,'code'=>$reset_password,'company_code'=>$COMPANY_CODE];
			
		$token = JWT::encode($payload, $user->getTokenKey(),'HS256');
			
		$link = SITE_URL . '/#/auth/change-password?reset_token='.$token;
	
		return $link;
	}
	
	public function saveBatchNotifications($persons,$title,$message,$action_url,$color_class='label label-primary')
	{
		$person = Person::getInstance();
		$person_pk = $person->primaryKey();
		
		foreach ((array)$persons as $row)
		{
			$data = ['title'=>$title,'message'=>$message,'action_url'=>$action_url,'color_class'=>$color_class];
			$data[$person_pk] = $row[$person_pk];
			$this->notification()->save($data);
		}
	}
	
	/*
	 * @desc send mail and notifications to the administrator and other admins
	 * @param string $object_title name of the affected objects e.g. users
	 * @param string $action_label label to show particular action taken e.g. deleted
	 * @param string $action_url url where an actor/recipient can click to view the stated action
	 * @param integer $count number of affected records in this operation e.g. number of uploaded records
	 * @param array $recipient_emails a list of emails where the message should be sent to
	 */
	public function sendMailBatchUpload($object_title,$action_label,$action_url='',$count,$recipient_emails=[])
	{
		$user = User::getInstance();
		$person = Person::getInstance();
		
		$sender = $user->filterOne([$user->primaryKey()=>$this->identity()->getId()]);
	
		//compose message body
		$body = $count.' '.$object_title.'(s) have been '.strtolower($action_label).' to '.SITE_NAME.'.<br/>'.
				ucwords(strtolower($action_label)).' By: '. $sender['full_name'].'<br/>'.
				'Date '.ucwords(strtolower($action_label)).' :'.date('d-M-Y h:i a',time()).'<br/>'.
				'Please login to review the records at '.'<a href="'.SITE_URL.'">'.SITE_NAME.'</a>';
	
		//compose message. create recipients list.
		$recipient_emails[] = ADMIN_EMAIL;
		$primary_recipient = array_shift($recipient_emails);
		$subject = $count.' New '.$object_title.'(s) '.ucwords(strtolower($action_label)).' to '.SITE_NAME;
	
		//invoke mailer and send emails
		$this->mailer()->setSender($sender['primary_email'],$sender['full_name']);
		$this->mailer()->setRecipient($primary_recipient);
		$this->mailer()->setCc($recipient_emails);
		$this->mailer()->sendEmail($body, $subject,true);
	
		//invoke notifier and save notification messages
		$persons = $person->select(['primary_email'=>$recipient_emails],[$person->primaryKey()]);
		
	
		$this->saveBatchNotifications($persons, $subject, $subject, $action_url);
	}
}