<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Commercial 
 * @version 1.0.0 
 * @abstract specialized for a organisation object
 */
class OrganisationObject extends CustomModel
{		

	public function save($data)
	{
		$identity = $this->identity();
		$organisation = Organisation::getInstance();
			
	

		if(!isset($data[$this->primaryKey()]) || (isset($data[$this->primaryKey()]) && intval($data[$this->primaryKey()])==0))
		{
			$payload = $identity->getPayload();
			;
			$data[$organisation->primaryKey()] = $payload['owner'][$organisation->primaryKey()];
			
		}
		return parent::save($data);
		
		echo($data);
	}
	
	public function select($criteria=null,$columns=null,$group_by=null,$order_by=null,$limit=null)
	{
		$identity = $this->identity();
		$organisation = Organisation::getInstance();
		$selected_company=  Model::getSentClientCompanyId();
	
		

		$payload = null !==($identity->getPayload())? $identity->getPayload():null;
		$cmpctr =array("company_id"=>$selected_company);
		$criteria[$organisation->primaryKey()] = $payload['owner'][$organisation->primaryKey()];
		$criteria= array_merge($criteria,$cmpctr);
		

		
			
					
		return parent::select($criteria,$columns,$group_by,$order_by,$limit);

	}
}
