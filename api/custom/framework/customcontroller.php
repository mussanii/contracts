<?php
class CustomController extends Controller
{	
	protected $import;
	/**
	 * --------------------- IMPORT DATA -------------------------
	 * @api {get} /controller/import import data from excel
	 * @apiName ImportData
	 * @apiGroup Data
	 *
	 */
	public function importAction()
	{
		$excel = $this->import;
		$data = $excel->importWorkBook();
		$this->data = $data;
	}
	
	/**
	 * --------------------- EXPORT DATA ----------------------------
	 * @api {get} /controller/export export data to excel
	 * @apiName ExportData
	 * @apiGroup Data
	 *
	 */
	public function exportAction()
	{
		$excel = $this->import;
		$data = $excel->exportWorkBook($this->getParams('type',null));
		extract($data);
		$this->download($content, $type, $name);
	}
	
}