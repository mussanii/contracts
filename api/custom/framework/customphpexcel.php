<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Adhoc commercial
 * @version 1.0.0 
 * @abstract base class for a excel import and export
 */


class CustomPhpExcel extends PHPExcel
{
	protected  $max_rows=1000;
	protected  $mime_type;
	protected  $writer_type;
	protected  $file_name;
	protected    $master_file;
	protected  $zip_mime_type;
	protected  $zip_name;	
	protected  $exl_folder;
	protected  $doc_folder;
	protected  $target;
	protected  $document_target;
	
	public function __construct($doc_folder='docs',$exl_folder='master')
	{
		//default folder name for documents
		$this->doc_folder = $doc_folder;
		$this->exl_folder = $exl_folder;
		
		$this->master_file = 'master';
			
		//this is basically temporary folder refer to OS directory structure
		$this->target = sys_get_temp_dir().DIRECTORY_SEPARATOR.$exl_folder;
		
		//default documents path
		$this->document_target = $this->target.DIRECTORY_SEPARATOR.$doc_folder;
		
		parent::__construct();
	}
	/**
	 * @desc set writer type, file name, zip name and mime type
	 * @param string $type short code of file type e.g xls, xlsx etc
	 * @param string $file_name
	 * @param string $zip_mime_type e.g. application/zip
	 */
	protected function setWriterAndType($type,$zip_mime_type)
	{
		$extension='.'.$type;
		
		switch ($type){
			case 'xlsx':
				{
					$this->mime_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
					$this->writer_type='Excel2007';
					break;
				}
			case 'xls':
				{
					$this->mime_type='application/vnd.ms-excel';
					$this->writer_type='Excel5';
					break;
				}
			case 'ods':
				{
					$this->mime_type='application/vnd.oasis.opendocument.spreadsheet';
					$this->writer_type='OpenDocument';
					break;
				}
		}
		
		$this->file_name = $this->master_file.$extension;
		$this->zip_mime_type = $zip_mime_type;
		$this->zip_name = $this->master_file.'.zip';		
	}
	
	/**
	 * @method populateSheet()
	 * @desc create a blank sheeet given sheet index,label, columns and optional data
	 * @param $index Excel sheet index
	 * @param $columns sheet columns labeled from A - Z
	 * @param $label textual title/label for the excel sheet
	 * @param $data 2-D list of rows to added to the excel sheet
	 * @param $visibility possible values [visible,hidden,veryHidden]
	 */ 
	protected function populateSheet($index,$label,$columns,$data=[],$visibility='visible',$combine_data=false)
	{
		if($index>0)
		{
			$this->createSheet($index);
		}

		$this->setActiveSheetIndex($index);
		$row = $this->getActiveSheet()->setTitle($label)->getHighestRow();
		$this->getActiveSheet()->setSheetState($visibility);
		
		/* create sheet columns and  set styles for them */
		foreach ($columns as $key=>$value)
		{
			$this->getActiveSheet()->getStyle($key.$row)->getFont()->setBold(true);
				
			$this->getActiveSheet()->SetCellValue($key.$row,$value['label'])->getColumnDimension($key)->setAutoSize(true);
		}
		/* create sheet columns and  set styles for them */

		/*insert data rows to the created sheet */
		$i=2;
		foreach ($data as $row)
		{
			foreach ($columns as $key=>$col_det)
			{
				$col_name=$col_det['id'];
				$value=$row[$col_name];
				$this->getActiveSheet()->SetCellValue($key.$i,$value)->getColumnDimension($key)->setAutoSize(true);
			}
			$i++;
		}
		/*insert data rows to the created sheet */
	}

	/**
	 * @method addDropdown()
	 * @desc add dropdown data to worksheet cells. It uses data from other worksheets
	 * @param string $source_sheet source sheetname
	 * @param char $des_col target column in destination sheet
	 * @param char $src_col source column in source sheet
	 * @param string $default
	 */
	protected function addDropdown($source_sheet,$des_col,$src_col,$default=null)
	{
		$max_rows=$this->max_rows;
		for ($i = 2; $i <= $max_rows; $i++)
		{
			$dropdown_column_range="$$src_col$2:$$src_col$$max_rows";
				
			$cell=$this->getActiveSheet()->getCell($des_col . $i);
			$cell->setValue($default);
			$objValidation2 = $cell->getDataValidation();
			$objValidation2->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
			$objValidation2->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
			$objValidation2->setAllowBlank(true);
			$objValidation2->setShowInputMessage(true);
			$objValidation2->setShowDropDown(true);
			$objValidation2->setPromptTitle('Pick value from list');
			$objValidation2->setPrompt('Please pick a value from the drop-down list.');
			$objValidation2->setErrorTitle('Input error');
			$objValidation2->setError('Value is not in list');
			$formula=$source_sheet.'!'.$dropdown_column_range;			
			$objValidation2->setFormula1($formula);
		}
	}
	
	

	/**
	 * @method writeExel()
	 * @desc start writing to output stream and create the excel file
	 * @param unknown $type
	 */
	protected function writeExel()
	{
		//start output stream bufferring
		ob_clean();
		
		//create excel writer
		$writer = \PHPExcel_IOFactory::createWriter($this,$this->writer_type);
				
		//remove any previous content if existent -- recursive
		FileSystem::rrmdir($this->target);
		
		//recreate this directory with permissions and save content
		mkdir($this->target,0777);
		mkdir($this->document_target,0777);
		ob_clean();
		$writer->save($this->target.DIRECTORY_SEPARATOR.$this->file_name);
		
		//delete any zipped file using the specified zip name
		if(file_exists($this->zip_name)==true)
		{
			unlink($this->zip_name);
		}		
	}
	
	/**
	 * @method getCellAttributes()
	 * @desc get worksheet cell basic attributes e.g. value and type
	 * @param PHPExcel_Worksheet $worksheet
	 * @param int $row i.e. 0,1,2 etc
	 * @param char $col_key i.e. A,B,C etc
	 * @param boolean $is_date
	 * @return multitype:Ambigous <NULL, unknown, number> string
	 */
	protected function getCellAttributes($worksheet,$row,$col_key,$is_date=false)
	{
		//get column index based on key e.g A
		$col=((int)\PHPExcel_Cell::columnIndexFromString($col_key))-1;
	
		//get cell, hence value from excel sheet
		$cell = $worksheet->getCellByColumnAndRow($col, $row);
		$val = trim($cell->getValue());
		$val = ($val=="")? null : $val;
		$type = \PHPExcel_Cell_DataType::dataTypeForValue($val);
	
		if($is_date==true)
		{
			$val = (\PHPExcel_Shared_Date::isDateTime($cell)==true)?
			\PHPExcel_Shared_Date::ExcelToPHP($cell->getValue()) : null;
		}
	
		return ['val'=>$val,'type'=>$type];
	}

	/**
	 * @method getUploadedFileMeta()
	 * @desc get file attributes of uploaded file i.e. file type, name, errors etc
	 * @return array contains file attributes
	 */
	protected function getUploadedFileMeta()
	{
		// Only accept files with these extensions
		$whitelist = ['zip'];
		$name      = null;
		$tmp_name= null;
		$error     = 'File upload failed! Please retry.';
		$extension=null;
			
		if (is_array($_FILES['file'])==true) {
			$tmp_name = $_FILES['file']['tmp_name'];
			$name     = basename($_FILES['file']['name']);
			$error    = $_FILES['file']['error'];
				
			if ($error === UPLOAD_ERR_OK) {

				$extension = pathinfo($name, PATHINFO_EXTENSION);

				if (!in_array($extension, $whitelist)) {
					$error = 'Invalid file type uploaded.Only '.implode(',', $whitelist).' formats are allowed.';
				}
				else{
					$error=false;
				}
			}
		}
		$file=['name'=>$name,'tmp_name'=>$tmp_name,'error'=>$error,'extension'=>$extension];

		return $file;
	}
	
}
