<?php
/*
 * 
 * @author Leiney M. Ogeto
 * @copyright 2017 Software Technologies Ltd
 * @license Adhoc commercial
 * @version 1.0.0 
 * @abstract base class for a addresse
 */
class StlCompany extends CustomModel
{
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{		
		$this->table_name='companies';
		parent::__construct();		
	}	
	
	private function getCompanyCode()
	{
		global $access_token;
		global $global;		
		
		$post = $global->getPost();
		
		//obtain access token if provided
		$access_token = (isset($post['access_token']))? $post['access_token'] : ((isset($_GET['access_token']))? $_GET['access_token'] : null);
		$payload_a = $global->getTokenPayLoad($access_token);
		
		//obtain reset token if provided
		$reset_token = (isset($post['reset_token']))? $post['reset_token'] : null;
		$payload_r = $global->getTokenPayLoad($reset_token);
		
		//obtain company code from $_POST or from $_GET
		$p_code = (isset($post['company_code']))? $post['company_code'] : null;
		$g_code = (isset($payload_a['company_code']))? $payload_a['company_code'] : null;
		$g_code = (isset($payload_r['company_code']))? $payload_r['company_code'] : $g_code;
		
		//reassign access token if invalid. i.e with company_code as parameter key
		$access_token = $g_code? $access_token : JWT::encode(['company_code'=>$p_code],TOKEN_KEY,'HS256');
		
		return $p_code? $p_code : $g_code;
	}
	
	/**
	 * @method setDatabase()
	 * @param $code company code e.g. 010
	 * @desc function to set database given a company code -- used by STL
	 */
	public function setDatabase()
	{
	    global $global;
		global $COMPANY_CODE;
		global $DB_HOST;
		global $DB_PORT;
		global $DB_NAME;
		global $DB_USER;
		global $DB_PASSWORD;
		global $DB_SOURCE;
		global $DB_VENDOR;
		
		$COMPANY_CODE = $this->getCompanyCode();
		
		/* query default stl-controller to fetch database name and some other credentials */
		$company = (array)$this->filterOne(['company_code'=>$COMPANY_CODE]);
			
		if(count($company)>0)
		{
			$DB_HOST = $company['hostname'];
			$DB_NAME = $company['database_name'];
			$DB_USER = $company['login_name'];
			$DB_PASSWORD = $company['password'];
			$DB_PORT = $company['port']? $company['port'] :  $DB_PORT;
			$DB_VENDOR = $company['vendor']? strtolower($company['vendor']) : $DB_VENDOR;
			
			$global->switchDbVendor();			
		}	
		else{
			die(json_encode(['message'=>MESSAGE_COMPANY_CODE_WRONG,'title'=>TITLE_COMPANY_CODE,'status'=>STATUS_ERROR]));
		}
	}
}
