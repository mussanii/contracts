<?php
$DB_HOST = isset($_SERVER['RDS_HOSTNAME'])? $_SERVER['RDS_HOSTNAME'] : 'localhost';
$DB_PORT = isset($_SERVER['RDS_PORT'])? $_SERVER['RDS_PORT'] : '3306';
$DB_NAME = isset($_SERVER['RDS_DB_NAME'])? $_SERVER['RDS_DB_NAME'] : 'stlcontroller';
$DB_USER = isset($_SERVER['RDS_USERNAME'])? $_SERVER['RDS_USERNAME'] : 'root';
$DB_PASSWORD = isset($_SERVER['RDS_PASSWORD'])? $_SERVER['RDS_PASSWORD'] : '';






/* Enable Your Database Vendor Here */
/* Enable Your Database Vendor Here */
//$DB_VENDOR = 'oracle';
$DB_VENDOR = 'mysql';
//$DB_VENDOR = 'mssql';
//$DB_VENDOR = 'postgresql';

$DB_SOURCE = $global->switchDbVendor();

/** Audit settings */
define('AUDIT_ENABLED', true);

/** Company And Product Logos*/
define('PRODUCT_LOGO','/resources/company/logo-128.png');
define('COMPANY_LOGO','/resources/company/vendor.png');

/** Mobile App Parameters */
define('APP_URL','');
define('APP_LOGO','');

/** company code validation messages */
define('TITLE_COMPANY_CODE','Company Code');
define('MESSAGE_COMPANY_CODE_WRONG','The company code entered is wrong');

/** Token & Password Hash Keys */
define('TOKEN_KEY','xyzsfsjkjh86235?hfs7');
define('PASSWORD_SALT','xsfsgh73738kshsg1');

/** Status error flags - in words */
define('STATUS_SUCCESS','success');
define('STATUS_INFO','info');
define('STATUS_ERROR','error');

/** Override database for companies*/
$stl_company = StlCompany::getInstance();
$stl_company->setDatabase();

/** Environment settings */
define ('DEVELOPMENT_ENVIRONMENT',true); 
define('DB_SOURCE',$DB_SOURCE);
define('DB_NAME',$DB_NAME);
define('DB_USER', $DB_USER);
define('DB_PASSWORD', $DB_PASSWORD);


/** Mailer Settings **/
if(in_array($_SERVER['REMOTE_ADDR'],['127.0.0.1','::1','localhost']))
{
    $host = ($_SERVER['SERVER_ADDR']=='::1')? '127.0.0.1' : $_SERVER['SERVER_ADDR'];
    define('SITE_NAME', $host.'.mkataba');
    define('SITE_URL', 'https://'.$host.'/mkataba');
}
else
{
    define('SITE_NAME', 'mkataba.stl-horizon.com');
    define('SITE_URL', 'https://mkataba.stl-horizon.com');
}
define('ADMIN_EMAIL', 'mkatabasupport@stl-horizon.com');
define('ADMIN_NAME', 'Mkataba Admin');
define('FROM_EMAIL', 'mkatabasupport@stl-horizon.com');
define('FROM_NAME', 'Mkataba User');
define('SMTP_PORT', '25');
define('IMAP_PORT_INCOMING', '143');
define('IMAP_PORT_INCOMING_SECURE', '993');
define('POP_PORT_INCOMING', '110');
define('POP_PORT_INCOMING_SECURE', '995');
define('SMTP_SERVER','email-smtp.us-east-1.amazonaws.com');
define('SMTP_SERVER_SECURE','email-smtp.us-east-1.amazonaws.com');
define('SMTP_NAME', 'Admin, Mkataba');
define('SMTP_USERNAME', 'AKIAYHPFZXWXIIVLVR3D');
define('SMTP_PASSWORD', 'BMXAtUB6EmKH63Ggz4J0zUsONyWs5cAulBn5cXJV13u8');
define('SMTP_AUTHED',true);



/** Sms Api Settings */
define('SMS_USERNAME','eboard');
define('SMS_APIKEY','9b75d6d8fb79d5fe9072907537f86565d89618134def3b0ea60ed70ba3095dc3');

/** Push notification settings */
define('PUSH_ENDPOINT','https://android.googleapis.com/gcm/send');
define('PUSH_APIKEY','AIzaSyCLoKrn0IcauPpAeOyW_awkRTUE64wlAj8');

/** Turn on Message queuing model */
define('PRIORITY_QUEUING',false);

/** Hash Keys **/
define('HASH_A', '46?i#gh76278~]w?');
define('HASH_B', '46?i#gh76278~]w?');

/** Message constants **/
define('TITLE_LOGIN_RESPONSE','Login Results');
define('TITLE_PASSWORD_RECOVERY','Password Recovery');
define('TITLE_PASSWORD_CONFIRM','Password Change & Confirmation');
define('TITLE_RECORD_OPERATION','Record Operation');
define('TITLE_SEARCH_OPERATION','Record Search');
define('TITLE_USER_REGISTRATION','User Registration');
define('TITLE_UPLOAD_OPERATION','File Upload Results');
define('TITLE_SETTINGS_RESET','Settings Reset');
define('TITLE_SMS_SEND','Sms Sending');
define('TITLE_SMS_READ','Sms Reading');
define('TITLE_EMAIL_SEND','Email Sending');
define('TITLE_EMAIL_READ','Email Reading');
define('TITLE_GUEST_COLLABORATION','Guest Collaboration');
define('MESSAGE_REGISTRATION_SUCCESS','User Registration Was Successful. Check your email at ');
define('MESSAGE_REGISTRATION_FAILURE','User Registration Failed.Check Errors');
define('MESSAGE_SEND_SUCCESS','Email successfully sent to ');
define('MESSAGE_SEND_FAILURE','Email sending failed, check outbox');
define('MESSAGE_USER_ALREADY_EXISTS','This user already exists!');
define('MESSAGE_ALREADY_TAKEN',' already taken!');
define('MESSAGE_USER_UPDATED','This User account has been successfully updated: ');
define('MESSAGE_LOGIN_SUCCESS','Logged In Successfully.');
define('MESSAGE_LOGIN_FAILURE','Login Failed.');
define('MESSAGE_MISSING_CREDENTIALS','Enter both username and password!');
define('MESSAGE_USER_SUSPENDED','This account has been suspended. Please consult administrator.');
define('MESSAGE_MISSING_INPUT','Enter values for required fields!');
define('MESSAGE_USER_IDENTITY_MISSING', 'A proper user identity could not be established. Fill any missing input.');
define('MESSAGE_WRONG_OLD_PASSWORD','The old password you provided is wrong. Try again!');
define('MESSAGE_PASSWORD_CHANGED','Your password has been changed successfully.');
define('MESSAGE_WRONG_CREDENTIALS','Wrong username or password!');
define('MESSAGE_UNCONFIRMED_PASSWORD','This account has not been activated.');
define('MESSAGE_SAVE_SUCCESS','Record Successfully Saved');
define('MESSAGE_UPDATE_NO_CHANGE','No Changes Made on Record!');
define('MESSAGE_SAVE_FAILURE','Record Save failed.Check Errors!');
define('MESSAGE_DEVICE_REGISTER_SUCCESS','Device successfully registered');
define('MESSAGE_DEVICE_REGISTER_FAILURE','Device registration failed!');
define('MESSAGE_DELETE_SUCCESS','Record Successfully Deleted');
define('MESSAGE_DELETE_FAILURE','Record Deletion Failed!');
define('MESSAGE_DELETE_NO_CHANGE','Record Was Already Deleted!');
define('MESSAGE_RECORD_SEARCH_FAILURE','Record Search Failed. Check Errors');
define('MESSAGE_RECORD_NOT_FOUND','No Record(s) Was Found');
define('MESSAGE_RECORD_FOUND','Record(s) Were Found');
define('MESSAGE_MAIL_READ_SUCCESS','Mail(s) read from inbox ');
define('MESSAGE_MAIL_READ_FAILURE','Mail reading failed ');
define('MESSAGE_MAIL_READ_NONE','No mails found');
define('MESSAGE_PASSWORD_SENT','A new password has been sent to: ');
define('MESSAGE_GUEST_COLLABORATE_SUCCESS','A collaboration link was successfully sent to: ');
define('MESSAGE_EMAIL_INVALID','The email provided doesn\'t exist in our system');
define('MESSAGE_EXPORT_SUCCESS','Document successfully exported!');
define('MESSAGE_IMAGE_UPLOAD_SUCCESS','Image changes successfully saved!');
define('MESSAGE_IMAGE_UPLOAD_FAILURE','Attempt to save image changes failed!');
define('MESSAGE_SETTINGS_RESET','Your settings have been reset!');
define('MESSAGE_ROLE_GRANTED_SUCCESS',' successfully added to ');
define('MESSAGE_ROLE_REVOKE_SUCCESS',' now removed from ');

//visacard constants

//visacard messages
