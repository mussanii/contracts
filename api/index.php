<?php
/*------------dont touch this file unless you know what you are doing-------------!*/

//Enable PHP Errors
//Sometimes it can be pain in the ass and you have no idea where the error started.
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
ini_set('always_populate_raw_post_data',-1);
ini_set("memory_limit","1280M");
ini_set("post_max_size","2000M");
ini_set("upload_max_filesize","5000M");

$default_time_zone = 'Africa/Nairobi';
date_default_timezone_set($default_time_zone);
$audit_start = time();

/*register and deal with fatal errors here * Dont touch unless you know what you are doing * */
function fatal_error_handler() {
	if (@is_array($e = @error_get_last())) 
	{
		if (isset($e['type']) && $e['type']==1) {			
			$error['title'] = 'HTTP/1.0 500 Error';
			$error['message'] = 'An internal server error has occured. Please consult administrator';
			$error['status'] = 'error';
			$error['success'] = 0;	
			//ob_clean();
			//echo json_encode($error);
		}
	}
	exit(); 
}
register_shutdown_function('fatal_error_handler');
/*register and deal with fatal errors here * Dont touch unless you know what you are doing * */


define('DS', DIRECTORY_SEPARATOR);
define('ROOT', dirname(__FILE__));
define('DB_ROOT',ROOT . DS . 'db'.DS);
define('MAIN_ROOT', dirname(dirname(__FILE__)));

//include global files and global required functions *dont touch unless you know what you are doing!*
require_once (ROOT . DS . 'library' . DS . 'globally.php');
$global = new Globally();

//get directory URI *dont touch unless you know what you are doing!*
$dir_uri=explode('/',$_SERVER['PHP_SELF']);
array_pop($dir_uri);
$script_uri=$dir_uri;
array_pop($script_uri);
$script_uri=implode('/',$script_uri);
$dir_uri=implode('/',$dir_uri);

//Retrieving path from url *dont touch unless you know what you are doing!*
$url=str_replace($dir_uri, '', $_SERVER['REQUEST_URI']);
$url=explode("/",$url);
$url=array_filter($url);
$error_message = ['message'=>'Invalid Url.Please Check Url','title'=>'Invalid Url'];
$response = count($url)<2? $global->sendResponse($error_message,true) : false;
$url=implode("/",$url);
$host = '';

//looking for web root *dont touch unless you know what you are doing!*
$protocol = (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://'; 
define('WEB_ROOT', $protocol.$_SERVER['SERVER_NAME'].$script_uri);
define('API_ROOT', $protocol.$_SERVER['SERVER_NAME'].$dir_uri);
define('RESOURCES_ROOT', WEB_ROOT.'/resources/');
 
require_once (ROOT . DS . 'custom' . DS . 'bootstrap.php');
require_once (ROOT . DS . 'library' . DS . 'bootstrap.php'); 