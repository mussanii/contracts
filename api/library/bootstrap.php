<?php 

/** Autoload any classes that are required **/
function Autoloader($className) {
	global $global; 
	
	//add default application paths
	$paths = [
			ROOT . DS . 'library' . DS . strtolower($className) . '.php',
			ROOT.DS.'application'.DS.'controllers' . DS . $className. '.php',
			ROOT.DS.'application'.DS.'models' . DS . $className. '.php'
	];
	
	//if modules present in project auto add their paths
	$modules = FileSystem::getFolders(ROOT.DS.'application'.DS.'modules');	
	if(count($modules)>0)
	{
		foreach ($modules as $module)
		{
			$paths[] = ROOT.DS.'application'.DS.'modules'.DS.$module.DS. 'controllers' . DS . $className. '.php';
			$paths[] = ROOT.DS.'application'.DS.'modules'.DS.$module.DS. 'models' . DS . $className. '.php';
		}
	}
	
	//actual search and determination if file exists, then autoload it
	foreach ($paths as $path)
	{
		if (file_exists($path))
		{
			include_once($path); break;
		}
	}
}

//load third party auto loaders - MUST come first to avoid conflicts
if (file_exists(ROOT . DS . 'library'.DS.'phpexcel' . DS .'Classes'.DS.'PHPExcel.php'))
	include_once(ROOT . DS . 'library'.DS.'phpexcel' . DS .'Classes'.DS.'PHPExcel.php');
if (file_exists(ROOT . DS . 'library'.DS.'phpmailer' . DS.'PHPMailerAutoload.php'))
	include_once(ROOT . DS . 'library'.DS.'phpmailer' . DS.'PHPMailerAutoload.php');
if (file_exists(ROOT . DS . 'library'.DS.'firebase'.DS.'Autoloader.php')) {
	include_once(ROOT . DS . 'library'.DS.'firebase'.DS.'Autoloader.php');
}

//load framework helper autoloaders
if (file_exists(ROOT . DS . 'library'.DS.'helpers' . DS .'Autoloader.php'))
	include_once(ROOT . DS . 'library'.DS.'helpers' . DS .'Autoloader.php');

//load framework auto loaders
spl_autoload_register('Autoloader');

//global objects declared
$post = $global->getPost();
$access_token = (isset($post['access_token']))? $post['access_token'] : ((isset($_GET['access_token']))? $_GET['access_token'] : null);

//load configurations and start routing and bootstrapping
include_once (ROOT . DS . 'config' . DS . 'config.php'); 
include_once (ROOT . DS . 'library' . DS . 'shared.php'); 