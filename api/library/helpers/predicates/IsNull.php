<?php
class IsNull extends PredicateFunction 
{
	public function __construct($value)
	{
		parent::__construct($value);
		$this->expression = ' ISNULL('.$this->getValue().')';
	}
}