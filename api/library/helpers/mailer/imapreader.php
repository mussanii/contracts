<?php
class ImapReader {
	/**
	 *
	 * @var array inbox account details
	 */
	private $account;
	private $is_error=false;
	private $message = null;
	
	//associations/compositions
	private static $_instance=null;
	
	/*Singleton Declaration */
	public static function getInstance()
	{
		$class = get_class();
		if(!($class::$_instance instanceof $class))
		{
			$class::$_instance = new $class;
		}
		return $class::$_instance;
	}
	
	public function __construct()
	{
		$this->setInbox();
	}
	
	public function isError($is_error=null)
	{
		$this->is_error = $is_error? $is_error : $this->is_error;
		return $this->is_error;
	}
	
	public function message($message=null)
	{
		$this->message = $message? $message : $this->message;
		return $this->message;
	}
	
	public function imapErrors()
	{
		return imap_errors();
	}
	
	/**
	 * @desc set inbox account details here
	 * @param string $username
	 * @param string $password
	 */
	public function setInbox($username=null,$password=null, $secure=true, $imap=true)
	{
		global $smtp_config;  
    	  
		$username= $username? $username: $smtp_config['SMTP_USERNAME'];
		$password= $password? $password: $smtp_config['SMTP_PASSWORD'];
    	
		$ssl = ($imap==true)? (($secure==true)? '/ssl' : '/notls') : (($secure==true)? '/ssl' : '');		
		$type = ($imap==true)? 'imap' : 'pop3';	    
	    $server = ($secure==true)? $smtp_config['SMTP_SERVER_SECURE'] : $smtp_config['SMTP_SERVER'];
	    $port = ($imap==true)? (($secure==true)? IMAP_PORT_INCOMING_SECURE : IMAP_PORT_INCOMING) : (($secure==true)? POP_PORT_INCOMING_SECURE : POP_PORT_INCOMING);

		$this->account = [
				'host' 		=> '{'.$server.':'.$port.'/'.$type.$ssl.'}INBOX',
				'username' 	=> $username,
				'password' 	=> $password
		];
	}
	
	private function flattenParts($messageParts, $flattenedParts = array(), $prefix = '', $index = 1, $fullPrefix = true) {
	
		foreach($messageParts as $part) {
			$flattenedParts[$prefix.$index] = $part;
			if(isset($part->parts)) {
				if($part->type == 2) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.', 0, false);
				}
				elseif($fullPrefix) {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix.$index.'.');
				}
				else {
					$flattenedParts = $this->flattenParts($part->parts, $flattenedParts, $prefix);
				}
				unset($flattenedParts[$prefix.$index]->parts);
			}
			$index++;
		}
	
		return $flattenedParts;
				
	}
	
	private function getPart($connection, $msg_no, $partNumber, $encoding) {
	
		$data = imap_fetchbody($connection, $msg_no, $partNumber);
		switch($encoding) {
			case 0: return $data; // 7BIT
			case 1: return $data; // 8BIT
			case 2: return $data; // BINARY
			case 3: return base64_decode($data); // BASE64
			case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
			case 5: return $data; // OTHER
		}
	
	
	}
	
	private  function getFilenameFromPart($part) {
	
		$filename = '';	
		if($part->ifdparameters) {
			foreach($part->dparameters as $object) {
				if(strtolower($object->attribute) == 'filename') {
					$filename = $object->value;
				}
			}
		}
	
		if(!$filename && $part->ifparameters) {
			foreach($part->parameters as $object) {
				if(strtolower($object->attribute) == 'name') {
					$filename = $object->value;
				}
			}
		}
	
		return $filename;
	
	}
	
	private function connect()
	{
		return imap_open($this->account['host'],$this->account['username'],$this->account['password']);
	}
	
	public function getInbox()
	{
		//connect to imap server	
		$imap_stream = $this->connect();
		if(imap_last_error()!==false)
		{
		    $this->setInbox(null,null,true,false);
                    $imap_stream = $this->connect();             
		}		
		return $imap_stream;
	}
	
	public function getMessages($retrieve_attatchments='N',$status='U')
	{
		ini_set('max_execution_time', 0);
		
		$this->message = null;
		
		$this->is_error = false;
		
		$imap_stream = $this->getInbox();
				
		/*retrieve message numbers*/
		$message_nos = [];	
			
		if($imap_stream)
		{
		    $message_nos = imap_search($imap_stream,'ALL');
		}
		/*retrieve message numbers*/
				
		$messages = [];
		
		if($imap_stream && $message_nos)
		{
			rsort($message_nos);
			
			/*Loop through message numbers as we access mails*/
			foreach($message_nos as $msg_no) {
								
				$header = imap_headerinfo($imap_stream, $msg_no);
				
				if(!is_object($header)) continue;
					
				if($header->Unseen!==$status) continue;
				
				/*extract email headers here contents*/
				$subject = $header->subject;	
				$time = strtotime(isset($header->date)? $header->date : 0);
				$date = date('d-M-Y h:i:s a',$time);
				
				$sender = isset($header->fromaddress)? trim($header->fromaddress) : null;
				$recipient = isset($header->toaddress)?  trim($header->toaddress) : null;				
				$cc = isset($header->ccaddress)? trim($header->ccaddress) : null;
				$bcc = isset($header->bccaddress)? trim($header->bccaddress) : null;				
				/*extract email headers here contents*/
				
				if(!$sender || !$recipient) continue;
				
				
				$body = '';
				$attachments = [];
				
				/* retrieve message body and attachments */
				$structure = imap_fetchstructure($imap_stream, $msg_no);
				$flattenedParts = isset($structure->parts)? $this->flattenParts($structure->parts) : [];
				
				foreach($flattenedParts as $partNumber => $part) {
				
					switch($part->type) {
				
						case 0:
							// the HTML or plain text part of the email
							$body = $this->getPart($imap_stream, $msg_no, $partNumber, $part->encoding);
							// now do something with the message, e.g. render it
							break;
				
						case 1:
							// multi-part headers, can ignore
				
							break;
						case 2:
							// attached message headers, can ignore
							break;
				
						case 3: // application
						case 4: // audio
						case 5: // image
						case 6: // video
						case 7: // other
							if($retrieve_attatchments=='Y')
							{
								$filename = $this->getFilenameFromPart($part);
								if($filename) {
									// it's an attachment
									$attachments[$filename] = $this->getPart($imap_stream, $msg_no, $partNumber, $part->encoding);
									// now do something with the attachment, e.g. save it somewhere
								}
								else {
									// don't know what it is
								}
							}
							break;				
					}				
				}
				/* retrieve message body and attachments */
					
				$messages[] = [
						'subject' => $subject,
						'sender' => $sender,
						'recipient' => $recipient,
						'cc' => $cc,
						'bcc' => $bcc,
						'date' => $date,
						'time' => $time,
						'body' => $body,
						'attachments' => $attachments
				];
					
				imap_setflag_full($imap_stream, $msg_no, "\\Seen \\Flagged", ST_UID);				
			}
		}
		
		//capture errors here 
		$this->message = imap_last_error();
		$this->is_error = count(imap_errors()>0)? true : false;		
		$this->alerts = imap_alerts();
			
		/* close imap stream */
		if($imap_stream)
		{
			imap_close($imap_stream);			
		}
			
		return $messages;
	}
	
	public function extractBody($inbox,$message_no,$section)
	{
		$mime_type = imap_fetchmime($inbox, $message_no, $section);
			
		if(strpos($mime_type,'Content-Transfer-Encoding: quoted-printable')>0){
			$body =  quoted_printable_decode(imap_fetchbody($inbox,$message_no,$section));
		}
		else if(strpos($mime_type,'Content-Transfer-Encoding: base64')>0){
			$body =  quoted_printable_decode(imap_fetchbody($inbox,$message_no,1.1));
		}
		else{
			$body =  imap_fetchbody($inbox,$message_no,$section);
		}
		return htmlspecialchars($body);
	}	
	
}
?>