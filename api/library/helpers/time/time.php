<?php
class Time 
{
    public function getTimestampRange($period_span)
    {
    	$ts_begin= null;
    	$ts_end = null;
    	 
    	$period_span = strtoupper(trim($period_span));
    	if($period_span=='D')
    	{    		
    		$ts_begin = new DateTime(date('Y-m-d'));
    		$ts_begin->modify('today')->setTime(0,0,1);
    		$ts_begin = $ts_begin->getTimeStamp();
    		
    		$ts_end = new DateTime(date('Y-m-d'));
    		$ts_end->modify('today')->setTime(23,59,59);
    		$ts_end = $ts_end->getTimeStamp();
    	}
    	elseif ($period_span=='W')
    	{
    		$ts_begin = new DateTime(date('Y-m-d'));
    		$ts_begin->modify('sunday last week')->setTime(0,0,1);
    		$ts_begin = $ts_begin->getTimeStamp();
    		
    		$ts_end = new DateTime(date('Y-m-d'));
    		$ts_end->modify('sunday this week')->setTime(23,59,59);
    		$ts_end = $ts_end->getTimeStamp();    		
    	}
    	elseif ($period_span=='M')
    	{
                $ts_begin = new DateTime(date('Y-m-d'));
                $ts_begin->modify('first day of this month')->setTime(0,0,1);
                $ts_begin = $ts_begin->getTimeStamp();

                $ts_end = new DateTime(date('Y-m-d'));
                $ts_end->modify('last day of this month')->setTime(23,59,59);
                $ts_end = $ts_end->getTimeStamp();
    	}
    	elseif ($period_span=='Y')
    	{
    		$ts_begin = new DateTime(date('Y-m-d'));
    		$ts_begin->modify('first day of january this year')->setTime(0,0,1);
    		$ts_begin = $ts_begin->getTimeStamp();
    		
    		$ts_end = new DateTime(date('Y-m-d'));
    		$ts_end->modify('last day of december this year')->setTime(23,59,59);
    		$ts_end = $ts_end->getTimeStamp();
    		
    	}
    	elseif ($period_span=='Z')
    	{
    	    $ts_begin = new DateTime(date('Y-m-d'));
    	    $ts_begin->modify('first day of january this year')->setTime(0,0,1);
    	    $ts_begin->sub(new DateInterval('P4Y'));
    	    $ts_begin = $ts_begin->getTimeStamp();
    	    
    	    $ts_end = new DateTime(date('Y-m-d'));
    	    $ts_end->modify('last day of december this year')->setTime(23,59,59);
    	    $ts_end = $ts_end->getTimeStamp();
    	    
    	}

    	return ['ts_begin'=>$ts_begin,'ts_end'=>$ts_end];
    }

    public function getTimeSpanValue($period_span)
    {
    	$span_value=0;

    	if($period_span=='D')
    	{    		
    	   $span_value = 60*60;
    	}
    	elseif ($period_span=='W')
      	{
           $span_value = 24*60*60; 		
    	}
    	elseif ($period_span=='M')
    	{
           $span_value = 24*60*60;
    	}
    	elseif ($period_span=='Y')
    	{
           $span_value = 30*24*60*60;  		
    	}

    	return $span_value;
    }
    
    public function getStandardUnit($period_span)
    {
    	if($period_span=='D')
    	{
    		$si = ['unit'=>'H', 'count'=>'6'];
    	}
    	elseif ($period_span=='W')
    	{
    		$si = ['unit'=>'D', 'count'=>'7'];
    	}
    	elseif ($period_span=='M')
    	{
    		$si = ['unit'=>'D', 'count'=>'5'];
    	}
    	elseif ($period_span=='Y')
    	{
    		$si = ['unit'=>'M', 'count'=>'5'];
    	}
    	elseif ($period_span=='Z')
    	{
    		$si = ['unit'=>'Y', 'count'=>'5'];
    	}
    	else
    	{
    		$si = ['unit'=>'N', 'count'=>'0'];
    	}
    
    	return $si;
    }
    
    /*
     * This can be used for comments and other from of communication
     * to tell the time ago instead of the exact time which might
     * not be correct to some one in another time zone.
     * The function only uses unix time stamp like the result of time()
     */
    public static function timeAgo($time,$suffix='ago',$suffix_inverse='to go')
    {
    	$periods = array("sec", "min", "hour", "day", "week", "month", "year", "decade");
    	$lengths = array("60","60","24","7","4.35","12","10");
    
    	$now = time();
    
    	$difference = $now - (int)$time; 
    	$suffix = $difference>0? $suffix : $suffix_inverse;
    	$difference = abs($difference);
    	    
    	for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
    		$difference /= $lengths[$j];
    	}
    
    	$difference = round($difference);
    
    	if($difference != 1) {
    		$periods[$j].= "s";
    	}
    
    	return "$difference $periods[$j] $suffix ";
    }
    
    /**
    * Needs a time() value, and it will tell you how many
    * seconds/minutes/hours/days/years/decades ago.
    */
    public static function timesAgo($tm,$cur_tm=null,$suffix='ago') {    	
        $rcs = 0;
    	$pds = array('second','minute','hour','day','week','month','year','decade');
		$lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
		//$lngh = array(1,60,3600,86400,604800,2592000,31104000,311040000);
		$dif = $tm - $cur_tm;
		$dif = $dif>0? $dif : 0;
		
    	for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); 
    		if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);    
    		$no = ceil($no); 
    		if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
    		if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
    		return $x.' '.$suffix;
    }
    
    /**
     * Needs a time() value, and it will give shortened rounded time 
     */
    public static function secondsToTime($milliseconds) { 
    	$seconds = floor($milliseconds / 1000);
    	$minutes = floor($seconds / 60);
    	$hours = floor($minutes / 60);
    	$milliseconds = $milliseconds % 1000;
    	$seconds = $seconds % 60;
    	$minutes = $minutes % 60;
    	
    	$format = '%u:%02u:%02u.%03u';
    	$time = sprintf($format, $hours, $minutes, $seconds, $milliseconds);
    	return rtrim($time, '0');
    }
    
    /**
     * @method formatDate()
     * @desc format dates
     * @param string $date_string date string 
     * @param string $input_format format of input date string e.g. d-M-Y 
     * @param string $output_format  format of output date string e.g. d-M-Y 
     * @return String|NULL
     */
    public static function formatDate($date_string,$input_format,$output_format)
    {
    	$date_string=(strpos($date_string, '0000-00-00') !== false)? null : $date_string;
    	$input_format=isset($input_format)? $input_format : null;
    	$date = ($date_string)? date_create_from_format($input_format, self::cleanDateString($date_string)) :null; 
    	$date_string = ($date && is_object($date)) ? date_format($date, $output_format) : null;
    	$date_string = ($date_string=="")? null : $date_string;
    	return $date_string;
    }
    
    public static function cleanDateString($date_string)
    {
    	$cmp = array_filter(explode('-',$date_string));
    	foreach ($cmp as &$p){
    		$p = trim(ucwords(strtolower($p)));
    	}
    	$cmp = implode('-',$cmp);
    	return $cmp;
    }
    
    public static function dateToTime($date){
    	$date = strtotime($date);
		return ($date==-2147483648)? null : $date;
    }
    
    public static function dateToTimestamp($date,$input_format='d-M-Y H:i:s')
    {
    	if(is_numeric($date)!=true)
		{	
			$date = DateTime::createFromFormat('Y-m-d H:i:s',self::formatDate($date,$input_format,'Y-m-d H:i:s'));
			$date = $date? $date->getTimestamp() : null;			
		}		
		return ($date==-2147483648)? null : $date;
    }
    
    /**
     * find the diff from 2 values of datetime
     * @param datetime $dt1
     * @param datetime $dt2
     * @return object $dtd (day, hour, min, sec / total)
     */
    public static function datetimeDiff($dt1, $dt2){
    	$t1 = is_int($dt1)==true? $dt1 : strtotime($dt1);
    	$t2 = is_int($dt2)==true? $dt2 : strtotime($dt2);
    
    	$dtd = new stdClass();
    	$dtd->interval = $t2 - $t1;
    	$dtd->total_sec = abs($t2-$t1);
    	$dtd->total_min = floor($dtd->total_sec/60);
    	$dtd->total_hour = floor($dtd->total_min/60);
    	$dtd->total_day = floor($dtd->total_hour/24);
    	$dtd->total_year = floor($dtd->total_day/365);
    
    	$dtd->day = $dtd->total_day;
    	$dtd->hour = $dtd->total_hour -($dtd->total_day*24);
    	$dtd->min = $dtd->total_min -($dtd->total_hour*60);
    	$dtd->sec = $dtd->total_sec -($dtd->total_min*60);
    	return $dtd;
    }
}


