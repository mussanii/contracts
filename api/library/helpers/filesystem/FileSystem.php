<?php 
class FileSystem
{ 
	
  /**
   * @method rrmdir()
   * @desc recursively delete folders within specified directory path
   * @param string $dir directory/folder path
   */
  public static function rrmdir($dir)
  {
  	if(is_dir($dir))
  	{
	  	foreach(glob($dir . '/*') as $file) {
	  		if(is_dir($file))
	  			self::rrmdir($file);
	  		else
	  			unlink($file);
	  	}  	
	  	rmdir($dir);
  	}
  }
  
  /**
   * @desc get path to a folder which contains a particular file
   * @param string $folder location/path to start the search
   * @param string $pattern part of or full file name
   * @return mixed|boolean if found return path else false
   */
  public static function getPath($folder, $pattern)
  {
  	$iti = new RecursiveDirectoryIterator($folder);
  	foreach(new RecursiveIteratorIterator($iti) as $file){
  		if(strpos($file , $pattern) !== false){
  			return $file;
  		}
  	}
  	return false;
  }
  

  /**
   * @desc get files in a folder/directory
   * @param string $folder location/path to start the search
   * @param array $pattern_array parts of or full file name as arrays
   * @return mixed|boolean if found return path else false
   */
  public static function getFiles($folder, $pattern_array=null)
  {
  	$files = array();
  	if(file_exists($folder)==true)
  	{
  		$iti = new RecursiveDirectoryIterator($folder);
  		foreach(new RecursiveIteratorIterator($iti) as $file){
  			if($file->isFile()==true)
  			{
  				if($pattern_array)
  				{
  					if (in_array(strtolower(array_pop(explode('.', $file))), $pattern_array))
  					{
  						$files[] = $file;
  					}
  				}
  				else
  				{
  					$files[] = $file;
  				}
  			}
  		}
  	}
  	return $files;
  }
  
  /**
   * @desc get folders only in a folder/directory
   * @param string $parent location/path to start the search
   * @param array $pattern_array parts of or full file name as arrays
   * @return mixed|boolean if found return path else false
   */
  public static function getFolders($parent, $pattern_array=null) 
  {
  	$folders = array();
  	if(file_exists($parent)==true)
  	{
  		foreach (new DirectoryIterator($parent) as $fileInfo) {
  			if($fileInfo->isDot() || $fileInfo->isFile()) continue;
  			$folders[] = $fileInfo->getFilename();
  		}
  	}
  	return $folders;
  }
  
  /**
   * @desc get file mime type here
   * @param string $filename full path to file including file name
   * @return string mime type e.g. application/pdf
   */
  public static function getMimeType($file)
  {
  		$mtype = false;
		if (function_exists('finfo_open')) 
		{
	 		$finfo = finfo_open(FILEINFO_MIME_TYPE);
	 		$mtype = finfo_file($finfo, $file);
	 		finfo_close($finfo);
	 	} 
	 	elseif (function_exists('mime_content_type')) 
	 	{
	 		$mtype = mime_content_type($file);
	 	} 
 		return $mtype;
  }
  
  public static function cleanFilename($file_name)
  {
  		$chars = '/[\/:*?"<>|]/';
  		return preg_replace($chars,"-",$file_name);
  }
} 
?>