<?php
class DataTransfer{

	private $adapter;
	private $message;
	private $is_error;
	private $clean_tables_sql=[
						"DELETE FROM customers_orders",
						"DELETE FROM mail_messages",
						"DELETE FROM notifications",
						"DELETE FROM orders_shipments",
						"DELETE FROM restock",
						"DELETE FROM stock_transfer",
						"DELETE FROM sms",
						"DELETE FROM stores",
						"DELETE FROM users_settings",
						"DELETE FROM fleet_units",
						"DELETE FROM sales",
						"DELETE FROM cash_book",
						"DELETE FROM items",
						"DELETE FROM categories",
						"DELETE FROM salesperson_targets",
						"UPDATE objects SET `title`='Distributors',`description`='List of distributors registered under this product' WHERE code='VDE'"			
	];
	
	public function message($message=null)
	{
		$this->message = $message? $message : $this->message;
		return $this->message;
	}
	
	public function isError($is_error=null)
	{
		$this->is_error = $is_error? $is_error : $this->is_error;
		return $this->is_error;
	}
	
	public function setAdapter($DB_SOURCE=DB_SOURCE,$DB_USER=DB_USER,$DB_PASSWORD=DB_PASSWORD)
	{
		//instantiate a database adapter here
		$this->adapter = new Adapter($DB_SOURCE, $DB_USER, $DB_PASSWORD);
		$this->adapter->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $this->adapter;		
	}
	
	/**
	 * @method importDatabase()
	 * @desc imports a database from a .sql file script
	 * @param string $db_name name given to database
	 * @param string $db_path absolute/full path to the database above
	 */
	public function importDatabase($db_name,$db_path)
	{		
		$db_name = '`'.str_replace('`','',$db_name).'`'; 
		$this->setAdapter();
		
		$this->isError(false);
		
		if($this->adapter)
		{
			//increase memory to avoid limits due to overusage
			ini_set('memory_limit','128M');
			
			//remove timeout so that the script runs to completion
			ini_set('max_execution_time', 0);
			
			//open database file path and retrieve contents and size
			$fs = fopen ($db_path, 'r');
			$contents = fread ( $fs, filesize($db_path));
			
			// explode dump sql. each sql is separated by semi-colon
			$statements = array_filter(explode ( ";\n", $contents)); 
			$statements = array_merge($statements, $this->clean_tables_sql);
			
			//create database if not existent
			$this->adapter->exec('CREATE DATABASE IF NOT EXISTS '.$db_name);
			
			//change database here
			$this->adapter->exec('USE '.$db_name);
			
			//loop through each statement and execute sql
			foreach ($statements AS $sql )
			{
				$sql = $this->cleanSql($sql);
				if($sql)
				{
					try{
						$this->adapter->exec($sql);
					}
					catch (Exception $ex){
						$this->isError(true);
						$this->message($this->adapter->errorInfo()[2]);
					}
				}
			}				
			
		}		
	}
	
	public function cleanSql($sql)
	{
		$sql = str_replace('DELIMITER', '', $sql);
		$sql = str_replace('$$', '', $sql);		
		
		/* check for comments and make them usable/executable */
		$start = strpos($sql, '/*!');		
		if($start>0)
		{
			/* remove begining of comments block */
			$sql = str_replace('/*!', '', $sql);	
						
			/*remove end of comments block */
			$end = strpos($sql, '*/');
			$sql = str_replace('/*!', '', $sql);
		}
				
		$sql = trim($sql);
		
		return ($sql=='')? null : $sql;		
	}

	/**
	 * @method exportDatabase()
	 * @desc exports a database to a .sql file script
	 * @param string $db_name name given to database
	 * @param string $db_path absolute/full path to the database above
	 */
	public function exportDatabase($db_name,$db_path)
	{
		$db_name = '`'.str_replace('`','',$db_name).'`'; 
		
		$this->setAdapter(); 
		
		if($this->adapter)
		{
			//increase memory to avoid limits due to overusage
			ini_set('memory_limit','128M');
				
			//remove timeout so that the script runs to completion
			ini_set('max_execution_time', 0);
				
			$host = $this->adapter->dsn;
			
			$pass = $this->adapter->passwd;
			
			$user = $this->adapter->username;
			
			$command = "mysqldump --opt -h $host -u $user -p $pass $db_name | gzip > $db_path";
			 
			system($command);
		}
	}
	
}