<?php
class Adapter extends PDO
{
	public $dsn;
	public $username;
	public $passwd;
	public $options;
	private $key;
	
	public function getIndex()
	{
		return $this->key;
	}
	
	public function __construct($dsn, $username, $passwd, $options=null)
	{
		$this->dsn = $dsn;
		$this->username = $username;
		$this->passwd = $passwd;
		$this->options = $options;
		
		$this->key = $this->dsn.$this->username.$this->passwd;
		
		parent::__construct($this->dsn, $this->username, $this->passwd, $this->options);
	}
}