<?php
class Helpers
{
	
	/**
	 * @method getTree()
	 * @desc get tree from 2-d array of table records
	 * @param array $flat
	 * @param string $pidKey
	 * @param string $idKey
	 * @return array
	 */
	public static function getTree($flat, $pidKey, $idKey = null,$text_key=null)
	{
		if(count($flat)==0)
		{
			return $flat;
		}
		 
		$grouped = array();
		foreach ($flat as $sub){
			$grouped[$sub[$pidKey]][] = $sub;
		}
	
		$fnBuilder = function($siblings) use (&$fnBuilder, $grouped, $idKey) {
			foreach ($siblings as $k => $sibling) {
				$id = $sibling[$idKey];
				if(isset($grouped[$id])) {
					$sibling['children'] = $fnBuilder($grouped[$id]);
				}
				$siblings[$k] = $sibling;
			}
			return $siblings;
		};
	
		$tree = $fnBuilder(isset($grouped[0])? $grouped[0] : []);
	
		return $tree;
	}
	
	/**
	 * @desc get paths for items in 2-d dimension array 	 
	 * @param unknown $arr
	 * @param string $pidKey
	 * @param string $idKey
	 * @param string $textKey
	 * @param string $separator
	 * @return Ambigous <multitype:, unknown>
	 */
	public static function getPaths($arr,$pidKey='id_parent', $idKey='id',$textKey='text',$separator='\\')
	{
		$data = [];
		foreach ((array)$arr as $row)
		{
			$data[$row[$idKey]] = $row;
		}
		$arr = $data;
		 
		foreach ($arr as $key => $row)
		{
			$id = $arr[$key][$idKey];
			$arr[$key][$pidKey] = (int)$arr[$key][$pidKey];
			$arr[$key]['path'] = self::getItemPath($arr, $id,$pidKey,$idKey,$textKey,$separator);			
		}
		 
		$data = [];
		foreach ($arr as $key => $row)
		{
			$data[] = $row;
		}
		return $data;
	}
	
	/**
	 * @desc get item path in a 2-d dimensional associative array with parents
	 * @param unknown $arr
	 * @param unknown $id
	 * @param string $pidKey
	 * @param string $idKey
	 * @param string $textKey
	 * @param string $separator
	 * @return string
	 */
	public static function getItemPath($arr,$id,$pidKey='id_parent', $idKey='id',$textKey='text',$separator='\\')
	{
		$id_parent = 0;
		$path = '';
		foreach ($arr as $key => $row)
		{
			if($key==$id)
			{
				$id_parent = (int)$row[$pidKey];
				$path = isset($arr[$id_parent])? $arr[$id_parent][$textKey] : '';
				break;
			}
		}
		if($id_parent==0 || $id==$id_parent)
		{
			return '';
		}
		else
		{ 
			return self::getItemPath($arr, $id_parent,$pidKey, $idKey,$textKey,$separator).$path.$separator;
		}
	}
	
	
	/**
	 * Finds all the ids of the of the nodes in a tree
	 * @param {Array} tree the array tree list
	 * @param {String} key The attribute name
	 * @param {Array} ids The variable that stores found ids
	 */
	public static function getNodes($tree = [], $key, &$descendants=[], $as_array=false) {
	    if(count($tree)){
	        foreach ($tree as $item) {	 
	            if($as_array==true){
	                $desc = $item; unset($desc['children']);
	            }
	            else{
	                $desc = $item[$key];
	            }
	            $descendants[] = $desc;
	            if(isset($item['children']) && count($item['children'])) {
	                self::getNodes($item['children'],$key,$descendants,$as_array);
	            }
	        }
	    }
	}
	
	/**
	 * Finds the first child that has the attribute with the specified value.
	 * Looks recursively down the tree to find the descendants for this child
	 * @param {Array} tree the array tree list
	 * @param {String} key The attribute name
	 * @param {Mixed} id The value to search for
	 * @param {Mixed} child The variable to store search results
	 */
	public static function searchNode($tree = [], $key, $id, &$child=null , &$found=false) {
	    if(count($tree)>0){
	        foreach ($tree as $item) {
	            if(($item[$key]==$id) && $found!=true){
	                $child = $item; $found = true; break;
	            }
	            if ($found!=true && isset($item['children']) && count($item['children'])>0) {
	                self::searchNode($item['children'],$key,$id,$child,$found);
	            }
	        }
	    }
	}
		
	public static function getFileType($type)
	{
		if($type == "Folder")
		{
			return ['id'=>1,'text'=>'Folder','icon'=>'fa fa-folder-o  font-icon-yellow'];
		}
		elseif($type == "application/pdf" || $type == "application/x-pdf")
		{
		    return ['id'=>2,'text'=>'Pdf','icon'=>'fa fa-file-pdf-o font-icon-red'];
		}
		elseif($type=="image/jpeg" || $type=="image/jpg" || $type=="image/png")
		{
		    return ['id'=>3,'text'=>'Image','icon'=>'fa fa-file-image-o  font-icon-green'];
		}
		elseif($type=="application/vnd.openxmlformats-officedocument.wordprocessingml.document"
				||$type=="application/msword"
				|| $type=="application/vnd.openxmlformats-officedocument.wordprocessingml.template")
		{
		    return ['id'=>4,'text'=>'Word','icon'=>'fa fa-file-word-o  font-icon-blue'];
		}
		elseif($type=="application/vnd.ms-powerpoint"
				|| $type=="application/vnd.openxmlformats-officedocument.presentationml.presentation"
				|| $type=="application/vnd.openxmlformats-officedocument.presentationml.slideshow"
				|| $type=="application/vnd.openxmlformats-officedocument.presentationml.template")
		{
		    return ['id'=>5,'text'=>'Powerpoint','icon'=>'fa fa-file-powerpoint-o  font-icon-orange'];
		}
		elseif($type=="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
				|| $type=="application/vnd.ms-excel")
		{
		    return ['id'=>6,'text'=>'Excel','icon'=>'fa fa-file-excel-o  font-icon-green'];
		}
		elseif($type=="audio/mpeg3" || $type=="audio/mpeg" || $type=="audio/x-mpeg-3"
				|| $type=="video/mpeg" | $type=="video/x-mpeg" || $type=='audio/wav' || $type=="audio/x-wav")
		{
		    return ['id'=>7,'text'=>'Audio','icon'=>'fa fa-file-sound-o  font-icon-orange'];
		}
		elseif($type=="video/mkv" || $type=="video/avi" || $type=="video/mp4" || $type=='swf')
		{
		    return ['id'=>8,'text'=>'Video','icon'=>'fa fa-file-film-o  font-icon-orange'];
		}
		elseif($type=="application/rtf")
		{
		    return ['id'=>9,'text'=>'Richtext','icon'=>'fa fa-file-word-o  font-icon-orange'];
		}
		elseif($type=="application/zip")
		{
		    return ['id'=>10,'text'=>'Compressed Zip','icon'=>'fa fa-file-zip-o font-icon-blue'];
		}
		else
		{
		    return ['id'=>0,'text'=>'Others','icon'=>'fa fa-file-o  font-icon-grey'];
		}
	}
	
	public static function phone_format($phone_number) {
		$cleaned = preg_replace('/[^[:digit:]]/', '', $phone_number);
		preg_match('/(\d{3})(\d{3})(\d{3})(\d{3})/', $cleaned, $matches);
		return count($matches)>=5? "({$matches[1]}) {$matches[2]} {$matches[3]} {$matches[4]}" : $phone_number;
	}
	
}