<?php 
	/**
	 * AMORTISATION CALCULATOR
	 * @author PRANEETH NIDARSHAN
	 * @version V1.0
	 */
	class Amortisation
	{
		private $loan_amount;
		private $term_years;
		private $interest;
		private $terms;
		private $period;
		private $currency = "XXX";
		private $principal;
		private $balance;
		private $term_pay;
		private $interest_total = 0;
		private $results;
		private $start_time;

		public function __construct($data)
		{
			if($this->validate($data)) 
			{							
				$this->loan_amount 	= (float) $data['loan_amount'];
				$this->term_years 	= $data['term_years'];
				$this->interest 	= (float) $data['interest'];
				$this->terms 		= (int) $data['terms'];
				
				$this->start_time 	= new DateTime();
				$this->start_time->setTimestamp($data['start_time']);
						
				$this->terms = ($this->terms == 0) ? 1 : $this->terms;

				$this->period = $this->terms * $this->term_years; 
				$this->interest = ($this->interest/100) / $this->terms;
				
				$this->results = [
						'inputs' => $data,
						'summary' => $this->getSummary(),
						'schedule' => $this->getSchedule()
				];
				
				return $this->results;
			}
		}
		
		public function getAmortisedSchedule()
		{
			return $this->results;
		}
		
		private function validate($data) {
			$data_format = [
				'loan_amount' 	=> 0,
				'term_years' 	=> 0,
				'interest' 		=> 0,
				'terms' 		=> 0
			];

			$validate_data = array_diff_key($data_format,$data);
			
			if(empty($validate_data)) 
			{
				return true;
			}
			else
			{
				$errors = [];
				foreach ($validate_data as $key => $value) {
					$errors[] = ":: Value $key is missing ";
				}
				return false;
			}
		}

		private function calculate()
		{
			$deno = 1 - 1 / pow((1+ $this->interest),$this->period);
			$this->term_pay = $deno==0? 0 : ($this->loan_amount * $this->interest) / $deno;
			$interest = $this->loan_amount * $this->interest;
			$this->interest_total = $this->interest_total + $interest;
			
			$this->principal = $this->term_pay - $interest;
			$bal_start = $this->loan_amount;
			$this->balance = $this->loan_amount - $this->principal;

			return [
				'payment' 	=> $this->term_pay,
				'bal_start' => $bal_start,
				'interest' 	=> $interest,
				'principal' => $this->principal,
				'bal_end' 	=> $this->balance,
				'interest_total' => $this->interest_total
			];
		}
		
		public function simpleSchedule()
		{
			$schedule = [];
			$years = $this->term_years;
			$payments_per_year = $this->terms;
			$rate = $this->interest;
			$p = $this->loan_amount;
			
			$number_of_payments = $payments_per_year * $years;
			$interest = ($p*$rate/100)/$number_of_payments;
			$payment = ($p/$number_of_payments) + $interest;
			$bal_start = $payment*$number_of_payments;
			$bal_end = $payment*$number_of_payments;
			$interest_total = 0; $principal = 0; $total_payment = 0; $total_payment=0;
			
			$time_stamp = $this->start_time->getTimestamp();
						
			for ($i=0; $i<$number_of_payments; $i++) {
			
				$total_payment = $total_payment + $payment;
				$principal = ($payment-$interest);
				$bal_end = $bal_end -  $payment;
				$interest_total = $interest_total + $interest;
				
				$date = new DateTime();
				$date->setTimestamp($time_stamp);
				$date->add(new DateInterval('P'.($i+1).'M'));
				
				$row = [
						'instalment' => ($i + 1),
						'payment' => $payment,
						'bal_start' => $bal_start,
						'principal'=> $principal,
						'interest' => $interest,
						'bal_end'=> $bal_end,
						'time'=> $date->getTimestamp(),
						'interest_total'=> $interest_total
				];
				
				$bal_start = $bal_start - $payment;
			
				$schedule[] = $row;
			}
			
			$summary = ['total_payment' => $total_payment,'interest_total' => $interest_total];
			return ['summary' => $summary,'schedule' => $schedule];			
		}

		public function getSummary()
		{
			$this->calculate();
			$total_pay = $this->term_pay *  $this->period;
			$interest_total = $total_pay - $this->loan_amount;

			return [
				'total_payment' => $total_pay,
				'interest_total' => $interest_total,
			];
		}

		public function getSchedule ()
		{
			$schedule = array();
			$this->interest_total = 0;
			
			if($this->interest==0)
			{
				
			}
			else
			{
				$time_stamp = $this->start_time->getTimestamp();
				$end_period = $this->period;
				
				while  ($this->balance >= 0 && $this->period>0) {
					$period = $end_period - ($this->period>0? $this->period : 0);
					
					$date = new DateTime();
					$date->setTimestamp($time_stamp);
					$date->add(new DateInterval('P'.$period.'M'));
					
					$row = array_merge($this->calculate(),['time'=> $date->getTimestamp()]);
					array_push($schedule, $row);
					$this->loan_amount = $this->balance;
					$this->period--;
				}	
			}
			return $schedule;

		}
		
	}
?>