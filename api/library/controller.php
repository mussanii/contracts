<?php
class Controller {
	protected $_model;
	protected $model;
	protected $_controller;
	protected $_action;
	protected $action_name;
	protected $_id;
	protected $_template;
	protected $data;
	protected $sanitize_data = true;
	protected $identity;
	protected $mailer;
	protected $browser;
	protected $sms;
	protected $notification;
	protected $enable_layout;
	protected $render;
	protected $query_array;
	protected $limit;
	protected $pretty;
	protected $after;
	protected $order;
	protected $infinite;
	protected $time;
	protected $advanced;
	protected $search_columns = [];
	
		
	protected function getParams($id, $default = null) {
		if (array_key_exists ( $id, $this->query_array ))
			return $this->query_array [$id];
		else
			return $default;
	}
	
	protected function getActionName() {
		return $this->action_name;
	}
	
	public function __construct($url_array = ['model'=>'','controller'=>'','action'=>'','id'=>''], $query_array = []) {
		global $identity;
		global $browser;
		global $mailer;
		global $sms;
		global $notification;
		global $global;
		global $default_time_zone;
		global $smtp_config;
		global $sms_config;
		global $push_notifier;
		global $access_token;
		global $COMPANY_CODE;
		
		$this->_controller = $url_array ['controller'];
		$this->_action = $url_array ['action'];
		$this->_id = $url_array ['id'];
		$this->_model = $url_array ['model'];
		$this->time = new Time ();
		$this->browser = new Browser(@$_SERVER['HTTP_USER_AGENT']);
		
		$mailer = Mailer::getInstance ();
		$sms = PhoneMessage::getInstance();
		$setting = UserSetting::getInstance ();
		$notification = Notification::getInstance ();
		
		$this->sms = $sms;
		$this->mailer = $mailer;
		$this->notification = $notification;
		
		$this->action_name = $global->getActionName ( $this->_action );
		
		// remove access token from query string
		unset ( $query_array ['access_token'] );
		
		// set record limit
		$this->limit = (isset ( $query_array ['limit'] )) ? ' LIMIT ' . $query_array ['limit'] : null;
		unset ( $query_array ['limit'] );
		
		// set after pagination marker to fetch records after id
		$this->after = (isset ( $query_array ['after'] )) ? intval ( $query_array ['after'] ) : null;
		unset ( $query_array ['after'] );
		
		// order direction. by default we assume a descending or model customized order [ASC/DESC]
		$this->order = (isset ( $query_array ['order'] )) ? strtoupper( $query_array ['order'] ) : null;
		unset ( $query_array ['order'] );
		
		// special flag that tells us if we should limit results in any way [Y/N]
		$this->infinite = (isset ( $query_array ['infinite'] )) ? strtoupper( $query_array ['infinite'] ) : null;
		unset ( $query_array ['infinite'] );
		
		// flag to determine if response content should be formatted with json pretty tree style
		$this->pretty = (isset ( $query_array ['pretty'] )) ? $query_array ['pretty'] : false;
		unset ( $query_array ['pretty'] );
		
		//flag to hold criteria of advanced search
		$this->advanced = (array)json_decode((isset($query_array['advanced'])? $query_array['advanced'] : null),true);		
		unset ( $query_array ['advanced'] );
		
		
		// prepare correct quary array
		$this->query_array = $query_array;
		$this->identity = User::getInstance ();
		$user = User::getInstance();
		
		/* get administrator smtp account credentials */
		$admin_smtp = $user->getAdmin ();
		$smtp_server = $setting->fetchColumn('value', ['code' => 'STS']);
		$smtp_server_secure = $setting->fetchColumn( 'value', ['code' => 'SSE']);
		
		$smtp_config ['ADMIN_EMAIL'] = $setting->fetchColumn ( 'value', ['code' => 'SEA']);
		$smtp_config ['ADMIN_USERNAME'] = $setting->fetchColumn ( 'value', ['code' => 'EAU']);
		$smtp_config ['ADMIN_NAME'] = isset ( $user_det ['full_name'] ) ? $admin_smtp ['full_name'] : ADMIN_NAME;
		$smtp_config ['ADMIN_PASSWORD'] = $setting->fetchColumn ( 'value', ['code' => 'EAP']);
		
		$smtp_config ['SMTP_SERVER_SECURE'] = $smtp_server_secure ? $smtp_server_secure : SMTP_SERVER_SECURE;
		$smtp_config ['SMTP_SERVER'] = $smtp_server ? $smtp_server : SMTP_SERVER;
		
		$sms_config['SMS_USERNAME'] = ($SMS_USERNAME=$setting->fetchColumn('value', ['code' => 'SMU']))? $SMS_USERNAME : SMS_USERNAME;
		$sms_config['SMS_APIKEY'] = ($SMS_APIKEY=$setting->fetchColumn('value', ['code' => 'SMP']))? $SMS_APIKEY : SMS_APIKEY;
		
		$push_notifier['PUSH_ENDPOINT'] = ($PUSH_EPT=$setting->fetchColumn('value', ['code' => 'PAE']))? $PUSH_EPT : PUSH_ENDPOINT;
		$push_notifier['PUSH_APIKEY'] = ($PUSH_AEK=$setting->fetchColumn('value', ['code' => 'PAK']))? $PUSH_AEK : PUSH_APIKEY;
		
		//obtain payload from access token incase any exists
		$payload = $global->getTokenPayLoad ( $access_token );
		
		//capture action name here
		$action_name = $this->getActionName ();

		if (count ( $payload ) > 1 && $action_name != 'login') 
		{
			//validate token and check if it belongs to user identified and allow access else break and return
			$uid = isset($payload[$user->primaryKey()])? $payload[$user->primaryKey()] : null;
			$token_valid =  $uid && $user->exists([$user->primaryKey()=>$uid,'session_token'=>$access_token]);
			
			if($token_valid!=true)
			{
				$this->renderJSON ($global->getExpiredSessionFlags());
			}			
			//validate token and check if it belongs to user identified and allow access else break and return
			
			$payload = $this->identity->getTokenPayLoad ( $access_token );
			$this->identity->setPayload ( $payload ['username'] );
			$identity = $this->identity;
			
			// set default timezone
			$time_zone = $setting->fetchColumn ( 'value', [ 
				'code' => 'TZ' 
			] );
			$time_zone = $time_zone ? $time_zone : $default_time_zone;
			date_default_timezone_set ( $default_time_zone );
			
			/* if no identity found set smtp credentials to those of the logged in user */
			$from_email = $setting->fetchColumn ( 'value', [ 
					'code' => 'SEA' 
			] );
			$smtp_username = $setting->fetchColumn ( 'value', [ 
					'code' => 'EAU' 
			] );
			$smtp_password = $setting->fetchColumn ( 'value', [ 
					'code' => 'EAP' 
			] );
			
			$smtp_config ['SMTP_NAME'] = $smtp_username ? $this->identity->getFullName () : SMTP_NAME;
			$smtp_config ['SMTP_USERNAME'] = $smtp_username ? $smtp_username : SMTP_USERNAME;
			$smtp_config ['SMTP_PASSWORD'] = $smtp_password ? $smtp_password : SMTP_PASSWORD;
			$smtp_config ['FROM_NAME'] = $smtp_username ? $this->identity->getFullName () : FROM_NAME;
			$smtp_config ['FROM_EMAIL'] = $from_email ? $from_email : FROM_EMAIL;
		} 
		else {
			/* Identity not found set smtp credentials to those of the administrator */
			$smtp_config ['SMTP_NAME'] = $smtp_config ['ADMIN_NAME'];
			$smtp_config ['SMTP_USERNAME'] = $smtp_config ['ADMIN_USERNAME'];
			$smtp_config ['SMTP_PASSWORD'] = $smtp_config ['ADMIN_PASSWORD'];
			$smtp_config ['FROM_NAME'] = $smtp_config ['ADMIN_NAME'];
			$smtp_config ['FROM_EMAIL'] = $smtp_config ['ADMIN_EMAIL'];
			
			// if image resource requested then provide access, else normal checks take effect
			if ((stripos ( $this->_controller, 'image' ) >= 0 || stripos ( $this->_controller, 'doc' ) >= 0) && $this->getActionName () == 'download') {
				// do nothing
			} 			// allow customers to register. so we make an exception here
			else if (stripos ( $this->_controller, 'customers' ) >= 0 && $this->getActionName () == 'register') {
			} 			// allow regions listing. so we make an exception here
			else if (stripos ( $this->_controller, 'regions' ) >= 0 && $this->getActionName () == 'list') {
			} 			// else if cron job activity to read mail or whatever, then provide access
			else if (stripos ( $this->_controller, 'cron' ) >= 0 && $this->getActionName () == 'read') {
				// set identity to administrator
				$username = $this->identity->filterOne ( 'username', [ 
						$this->identity->primaryKey () => 1 
				] );
				$this->identity->setPayload ( $username );
			} else {
				// if no session token and action requested isnt login and not change password				
				if ($action_name != 'login' && $action_name != 'changePassword' && $action_name != 'recoverPassword') {
					$this->renderJSON ($global->getExpiredSessionFlags());
				}
			}
		}
		
		$model = $this->_model;
		$controller = $this->_controller;
		$action = $this->_action;
		
		$this->$model = $model::getInstance ();
		
		$this->model = $this->$model;
		
		$this->_template = new Template ( $controller, $action );
		
		$this->set ( 'id', $this->_id );
		
		$this->setRender ( false );
	}
	public function getPost($key = null) {
		$_POST = (isset ( $_POST ) && count ( $_POST ) > 0) ? $_POST : json_decode ( file_get_contents ( "php://input" ), true );
		
		if ($key)
			return isset ( $_POST [$key] ) ? $_POST [$key] : null;
		else
			return isset ( $_POST ) ? $_POST : null;
	}
	public function addPost($data, $override = false) {
		$_POST = $override == true ? array_merge ( ( array ) $this->getPost (), ( array ) $data ) : array_merge ( ( array ) $data, ( array ) $this->getPost () );
		file_put_contents ( "php://input",$_POST);
		return $_POST;
	}
	public function getPostedFiles($key = null) {
		$_FILES = (isset ( $_FILES ) && count ( $_FILES ) > 0) ? $_FILES : json_decode ( file_get_contents ( "php://input" ), true );
		
		if ($key)
			return isset ( $_FILES [$key] ) ? $_FILES [$key] : null;
		else
			return isset ( $_FILES ) ? $_FILES : null;
	}
	public function hasPostedFiles() {
		return isset ( $_FILES ) ? $_FILES : null;
	}
	public function getKeyValue($array, $key) {
		return (isset ( $array [$key] ) && trim ( $array [$key] ) != '') ? $array [$key] : null;
	}
	public function setRender($render = true) {
		$this->render = $render;
	}
	
	/**
	 * JSON renderer
	 *
	 * @param string The original JSON string
	 * @return string
	 */
	public function renderJSON($content = null) 
	{
		global $global;
		$this->setRender (false);
		$content = $content!==null? json_encode ( $content, JSON_NUMERIC_CHECK ) : null;
		$content = ($this->pretty == true) ? '<pre>' .$this->beautifyJson($content) . '<pre>' : $content;		
		$global->sendResponse($content);
	}
	
	/**
	 * JSON beautifier
	 *
	 * @param string The original JSON string
	 * @param string Return string
	 * @param string Tab string
	 * @return string
	 */
	protected function beautifyJson($json, $ret = "\n", $ind = "\t") {
		
		$beauty_json = '';
		$quote_state = FALSE;
		$level = 0;		
		$json_length = strlen ( $json );
		
		for($i = 0; $i < $json_length; $i ++) {
			
			$pre = '';
			$suf = '';
			
			switch ($json [$i]) {
				case '"' :
					$quote_state = ! $quote_state;
					break;
				
				case '[' :
					$level ++;
					break;
				
				case ']' :
					$level --;
					$pre = $ret;
					$pre .= str_repeat ( $ind, $level );
					break;
				
				case '{' :
					
					if ($i - 1 >= 0 && $json [$i - 1] != ',') {
						$pre = $ret;
						$pre .= str_repeat ( $ind, $level );
					}
					
					$level ++;
					$suf = $ret;
					$suf .= str_repeat ( $ind, $level );
					break;
				
				case ':' :
					$suf = ' ';
					break;
				
				case ',' :
					
					if (! $quote_state) {
						$suf = $ret;
						$suf .= str_repeat ( $ind, $level );
					}
					break;
				
				case '}' :
					$level --;
				
				case ']' :
					$pre = $ret;
					$pre .= str_repeat ( $ind, $level );
					break;
			}			
			$beauty_json .= $pre . $json [$i]. $suf;
		}		
		return $beauty_json;
	}
	
	public function enableLayout($enable = true) {
		$this->enable_layout = $enable;
	}
	public function set($name, $value) {
		$this->_template->set ( $name, $value );
	}
	public function setVariables(array $vars) {
		foreach ( $vars as $key => $value ) {
			$this->_template->set ( $key, $value );
		}
	}
	public function isAjaxRequest() {
		/* AJAX check */
		if (! empty ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) && strtolower ( $_SERVER ['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest') {
			return true;
		} else
			return false;
	}
	public function __destruct() {
		if ($this->render == true) {
			$this->_template->render ( $this->enable_layout, $this->render );
		} else {
			$this->renderJSON ( $this->data );
		}
	}
	public function redirect(array $url_string, array $queryString = null) {
		global $global;
		$controller = isset ( $url_string ['controller'] ) ? $url_string ['controller'] : null;
		$action = isset ( $url_string ['action'] ) ? $url_string ['action'] : null;
		$id = isset ( $url_string ['id'] ) ? $url_string ['id'] : null;
		$global->redirect ( MODULE_NAME, $controller, $action, $id, $queryString );
	}
	
	protected function getAdvancedCriteria($model,$criteria)
	{
		$columns = $model->getColumns();
		$index = 0;
		
		foreach ($this->advanced as $key=>$c){
		    $c['value'] = is_array($c['value']) ? htmlentities(implode(',',$c['value'])) : $c['value'] ;
			
			/* set global search here and proceed */
			if($c['type']=='global'){
				$search_columns = $model->concat($this->search_columns);
				$criteria [] = new Like($search_columns, '%'.$c['value'].'%', $key.$index);
			}
	
			/* column missing in set of allowed columns */
			if(!in_array($key, $columns)) continue;
	
			if($c['type']=='range'){
				$criteria [] = new GreaterThanOrEqualTo($key, $c['value'][0], $key.$index);
				$criteria [] = new LessThanOrEqualTo($key, $c['value'][1], $key.($index+1));
			}
			else if($c['type']=='val'){
				$criteria [$key] = $c['value'];
			}
			else if($c['type']=='gt'){
				$criteria [] = new GreaterThan($key, $c['value'], $key.$index);
			}
			else if($c['type']=='lt'){
				$criteria [] = new LessThan($key, $c['value'], key.$index);
			}
	
			$index++;
		}
		
		return $criteria;
	}
	
	public function listAction() {
		/*by default, we let model default 'order by' take  precedence */
		$order_by = null; 
		$criteria = $this->query_array;	
		
		/* compose advanced criteria here */
		$criteria = $this->getAdvancedCriteria($this->model,$criteria);
		
		/* apply infinite counter and scroll */		
		$count = ($this->infinite=='Y')? $this->model->count($criteria) : 0;
		
		if ($this->after > 0) {				
			if ($this->order=='ASC') 
			{
				$order_by = $this->model->primaryKey().' '.$this->order;
				$criteria [] = new GreaterThan($this->model->primaryKey (), $this->after, $this->model->primaryKey ());
			}	
			else
			{
				$criteria [] = new LessThan ( $this->model->primaryKey (), $this->after, $this->model->primaryKey () );
			}
		}				
		/* apply infinite counter and scroll */
		
		$data = (array)$this->model->select( $criteria, null, null, $order_by, $this->limit ); 
		$this->data = ($this->infinite!='Y')? $data : ['count'=>$count,'items'=>$data];		 
	}
	public function oneAction() {
		$this->listAction ();
		$data = $this->data;
		$this->data = count ( $data ) > 0 ? $data [0] : "";
	}
	public function detailsAction() {
		$results = $this->model->selectOne ( $this->_id );
		/* Check if error has been encountered in fetch */
		if ($this->model->isError () == false) {
			if ($results)
				$response = [ 
						'message' => '1 ' . MESSAGE_RECORD_FOUND,
						'status' => STATUS_SUCCESS,
						'success' => 1,
						'data' => $results,
				        'id' => $this->_id
				];
			else
				$response = [ 
						'message' => MESSAGE_RECORD_NOT_FOUND,
						'status' => STATUS_INFO,
						'success' => 2 
				];
		} else {
			$response = [ 
					'message' => MESSAGE_RECORD_SEARCH_FAILURE,
					'status' => STATUS_ERROR,
					'success' => 0 
			];
		}
		
		$response ['title'] = TITLE_RECORD_OPERATION;
		
		$this->data = $response;
	}
	
	/**
	 * get a generalized response during save operation
	 * this method can be called from derived classes
	 * 
	 * @method getSaveResponse()
	 */
	protected function getSaveResponse($count, $success = MESSAGE_SAVE_SUCCESS, $nochange = MESSAGE_UPDATE_NO_CHANGE) {
		/* Check if error has been encountered in saving */
		if ($this->model->isError () == false) {
			if ($count == 0)
				$response = [ 
						'message' => $nochange,
						'status' => STATUS_INFO,
						'success' => 2,
						'id' => $this->model->lastAffectedId () 
				];
			else
				$response = [ 
						'message' => $success,
						'status' => STATUS_SUCCESS,
						'success' => 1,
						'count' => $count,
						'id' => $this->model->lastAffectedId () 
				];
		} else {
			$response = [ 
					'message' => $this->model->message (),
					'status' => STATUS_ERROR,
					'success' => 0 
			];
		}
		
		$response ['title'] = TITLE_RECORD_OPERATION;
		$this->data = $response;
	}
	public function saveAction() {
		$data = ($this->sanitize_data == true) ? $this->model->sanitize ( $this->getPost () ) : $this->getPost ();
		$count = $this->model->save ( $data );
		$this->getSaveResponse ( $count );
	}
	
	/**
	 * edit a single column in database
	 *
	 * @method editFieldAction()
	 */
	public function editFieldAction() {
		$data = $this->getPost ();
		$count = $this->model->update ( $data, [ 
				$this->model->primaryKey () => $this->_id 
		] );
		
		/* Check if error has been encountered in saving */
		if ($this->model->isError () == false) {
			if ($count == 0)
				$response = [ 
						'message' => MESSAGE_UPDATE_NO_CHANGE,
						'status' => STATUS_INFO,
						'success' => 2,
						'id' => $this->model->lastAffectedId () 
				];
			else
				$response = [ 
						'message' => MESSAGE_SAVE_SUCCESS,
						'status' => STATUS_SUCCESS,
						'success' => 1,
						'count' => $count,
						'id' => $this->model->lastAffectedId () 
				];
		} else {
			$response = [ 
					'message' => $this->model->message (),
					'status' => STATUS_ERROR,
					'success' => 0 
			];
		}
		
		$response ['title'] = TITLE_RECORD_OPERATION;
		$this->data = $response;
	}
	public function deleteAction() 
	{		
		$count = $this->_id? $this->model->deleteOne($this->_id) : $this->model->delete($this->query_array);
	
		
		/* Check if error has been encountered in delete */
		if ($this->model->isError () == false) {
			if ($count == 0)
				$response = [ 
						'message' => MESSAGE_DELETE_NO_CHANGE,
						'status' => STATUS_INFO,
						'success' => 2 
				];
			else
				$response = [ 
						'message' => MESSAGE_DELETE_SUCCESS,
						'status' => STATUS_SUCCESS,
						'success' => 1,
						'count' => $count,
						'id' => $this->model->lastAffectedId () 
				];
		} else {
			$response = [ 
					'message' => MESSAGE_DELETE_FAILURE . $this->model->message (),
					'status' => STATUS_ERROR,
					'success' => 0 
			];
		}
		
		$response ['title'] = TITLE_RECORD_OPERATION;
		$this->data = $response;
	}
	
	// calculate daily and previous total for a given date
	public function statAction() {
		$fn = $this->getParams ( 'type' ) . 'Column';
		$date_column = $this->getParams ( 'date_column' );
		$column = $this->model->$fn ( $this->getParams ( 'column' ) );
		
		$today = strtotime ( "midnight", time () );
		$todate = new DateTime ();
		$todate->setTimestamp ( $today );
		$todate->sub ( new DateInterval ( 'P1D' ) );
		$yesterday = $todate->getTimestamp ();
		
		$total_current = $this->model->fetchColumn ( $column, [ 
				new GreaterThanOrEqualTo ( $date_column, $today, $date_column ) 
		], false, $this->getParams ( 'column' ) );
		
		$total_previous = $this->model->fetchColumn ( $column, [ 
				new GreaterThanOrEqualTo ( $date_column, $yesterday, 'date1' ),
				new LessThanOrEqualTo ( $date_column, $today, 'date2' ) 
		], false, $this->getParams ( 'column' ) );
		
		$response ['total'] = $total_current;
		$response ['change'] = $total_current - $total_previous;
		
		$response ['stat'] = $this->getParams ( 'stat' ) ? $this->getParams ( 'stat' ) : $this->getPost ( 'stat' );
		$response ['success'] = 1;
		
		$this->data = $response;
	}
	
	// calculate total for a statistical range of times
	public function statRangeAction() {
		$fn = $this->getParams ( 'type' ) . 'Column';
		$date_column = $this->getParams ( 'date_column' );
		$column = $this->model->$fn ( $this->getParams ( 'column' ) );
		
		// get proper time range to select records from
		$times = $this->getTimestampRange ( $this->getParams ( 'period_span', 'D' ) );
		$ts_begin = $times ['ts_begin'];
		$ts_end = $times ['ts_end'];
		// die($ts_begin.' '.$ts_end);
		// get proper time range to select records from
		
		$total = $this->model->fetchColumn ( $column, [ 
				new GreaterThanOrEqualTo ( $date_column, $ts_begin, 'date1' ),
				new LessThanOrEqualTo ( $date_column, $ts_end, 'date2' ) 
		], false, $this->getParams ( 'column' ) );
		
		$response ['total'] = $total;
		$response ['stat'] = $this->getParams ( 'stat' ) ? $this->getParams ( 'stat' ) : $this->getPost ( 'stat' );
		$response ['success'] = 1;
		
		$this->data = $response;
	}
	
	/**
	 * @desc fetch graph data for a statistical range of times
	 * @param $period_span accepted values [Z-decade,Y-year,M-month, W-week, D-day]
	 * @param $date_column the column to measure against time
	 * @param $type statistic type allowed values [sum,count]
	 * @param $keyed whether to return values as keyed or indexed. accepted values [Y,N], default 'Y'
	 * @param $cumulative whether to cumulate values in the trend. accepted values [Y,N], default 'Y'
	 * @param $title prefered title for the trend
	 */
	public function graphAction() {
		$fn = $this->getParams ( 'type' ) . 'Column';
		$date_column = $this->getParams ( 'date_column' );
		$column = $this->model->$fn ( $this->getParams ( 'column' ) );
		$user = User::getInstance ();
		
		$cumulative = $this->getParams ( 'cumulative','Y');
		$keyed = $this->getParams ('keyed','Y');
		
		// get proper time range to select records from
		$period_span = $this->getParams ( 'period_span', 'D' );
		$times = $this->getTimestampRange ( $period_span );
		$si = $this->time->getStandardUnit($period_span);
		$span_tm = $si['count'];
		$pi = $si['unit'];
		$ts_begin = $times ['ts_begin']; 
		$ts_end = $times ['ts_end'];
		// get proper time range to select records from
		
		$data = [ 
			'values' => [],
			'key' => $this->getParams ('title').' Trends' 
		];
		
		$total = 0;
		$date = new DateTime();
		
		for($i = 1; $i <= $span_tm; $i++) {
			
			$date->setTimestamp($ts_begin);
			$date->add(new DateInterval('P'.($i-1).$pi));
			$begin_ts = $date->getTimestamp();
			
			$date->setTimestamp($ts_begin);			
			$date->add(new DateInterval('P'.$i.$pi));
			$end_ts = $date->getTimestamp();
			
			$criteria = [ 
				new GreaterThanOrEqualTo ( $date_column, $begin_ts, 'date1' ),
				new LessThanOrEqualTo ( $date_column, $end_ts, 'date2' ) 
			];
			
			// filter by user id if specified
			if (( int ) $this->_id > 0) {
				$criteria [$user->primaryKey ()] = $this->_id;
			}
			// filter by user id if specified
			
			$value = $this->model->fetchColumn ( $column, $criteria, false, $this->getParams ( 'column' ) );
			
			$total = ($cumulative=='Y') ? $total + $value : $value;
			
			$data ['values'][] = ($keyed=='Y')? ['x' =>$begin_ts*1000,'y' =>$total] : [$begin_ts*1000,$total];
		}
		
		$response ['data'] = $data;
		$response ['stat'] = $this->getParams ( 'stat' ) ? $this->getParams ( 'stat' ) : $this->getPost ( 'stat' );
		$response ['success'] = 1;
		
		$this->data = $response;
	}
	
	// create proper time range from period span e.g. Day, Week,Month
	protected function getTimestampRange($period_span) {
		return $this->time->getTimestampRange ( $period_span );
	}
	// create proper time range from period span e.g. Day, Week,Month
	
	
	
	/**
	 * get tree from 2-d array of table records
	 * 
	 * @method getTree()
	 * @param array $flat        	
	 * @param string $pidKey        	
	 * @param string $idKey        	
	 * @return array
	 */
	protected function getTree($flat, $pidKey, $idKey = null, $text_key = null) {
		return Helpers::getTree ( $flat, $pidKey, $idKey, $text_key );
	}
	
	// get paths for items in 2-d dimension array
	public function getPaths($arr, $pidKey = 'id_parent', $idKey = 'id', $textKey = 'text', $separator = '\\') {
		return Helpers::getPaths ( $arr, $pidKey, $idKey, $textKey, $separator );
	}
	
	// get item path in a 2-d dimensional associative array with parents
	private function getItemPath($arr, $id, $pidKey = 'id_parent', $idKey = 'id', $textKey = 'text', $separator = '\\') {
		return Helpers::getItemPath ( $arr, $id, $pidKey, $idKey, $textKey, $separator );
	}
	protected function sanitizeAsArray($content) {
		$array = is_array ( $content ) == true ? $content : (($content) ? [ 
				$content 
		] : [ ]);
		return $array;
	}
	protected function getResetPasswordCode($email) {
		return $this->model->getResetPasswordCode ( $email );
	}
	
	/**
	 * generic method to handle email sending
	 * 
	 * @method sendEmail()
	 * @param unknown $email        	
	 * @param unknown $name        	
	 * @param unknown $body        	
	 * @param unknown $subject        	
	 * @param string $from_email        	
	 * @param string $from_name        	
	 * @return boolean @api
	 */
	protected function sendEmail($email, $name, $body, $subject, $save_copy = false, $from_email = null, $from_name = null) {
		global $smtp_config;
		
		$from_email = $from_email ? $from_email : $smtp_config ['FROM_EMAIL'];
		$from_name = $from_name ? $from_name : $smtp_config ['FROM_NAME'];
		
		$this->mailer->setSender ( $from_email, $from_name );
		$this->mailer->setRecipient ( $email );
		$this->mailer->setCc ( $smtp_config ['ADMIN_EMAIL'] );
		
		return $this->mailer->sendEmail ( $body, $subject, $save_copy );
	}
	protected function download($content, $type, $name) {
		global $global;
		
		$size = strlen ( $content );
		header ( "Content-length: $size" );
		header ( "Content-type: $type" );
		header ( "Content-Disposition: inline; filename=\"$name\"" );
		
		$global->sendResponse($content);
	}
}
