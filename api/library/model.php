<?php
abstract class Model
{

	/*Singleton Declaration */
	private $db;
	protected static $db_adapters = [];

	protected $table_name;
	private $recordsAffected;
	private $autocommit = true;
	private $lastAffectedId;
	private $recordsSelected;
	private $join;
	protected $prkey;
	protected $is_error;
	protected $message;
	private $success;
	private $columns;
	private $table_meta;
	protected $identity;
	protected $time;
	protected $mailer;
	protected $notification;
	protected $company_id;
	public function success()
	{
		return $this->success;
	}
	public function message($message = null)
	{
		$this->message = $message ? $message : $this->message;
		return $this->message;
	}

	public function autocommit($autocommit = null)
	{
		$this->autocommit = $autocommit ? $autocommit : $this->autocommit;
		return $this->autocommit;
	}

	/**
	 * @method isError()
	 * @desc returns status flag to show if error occurred during a record operation
	 * @return boolean $is_error 
	 */
	public function isError($is_error = null)
	{
		$this->is_error = $is_error ? $is_error : $this->is_error;
		return $this->is_error;
	}
	public function lastAffectedId($lastAffectedId = null)
	{
		$this->lastAffectedId = $lastAffectedId ? $lastAffectedId : $this->lastAffectedId;
		return $this->lastAffectedId;
	}
	public function recordsAffected($recordsAffected = null)
	{
		$this->recordsAffected = $recordsAffected ? $recordsAffected : $this->recordsAffected;
		return $this->recordsAffected;
	}
	public function recordsSelected()
	{
		return $this->recordsSelected;
	}
	public function setKeys()
	{
		global $DB_VENDOR;

		if ($DB_VENDOR == 'oracle') {
			$sql = "select * from user_cons_columns ucc, user_constraints uc
                where uc.constraint_name=ucc.constraint_name and uc.constraint_type='P' 
                and uc.table_name = '$this->table_name';";
		} else {
			$sql = "SHOW INDEX FROM $this->table_name WHERE Key_name = 'PRIMARY' ";
		}

		$statement = $this->adapter()->prepare($sql);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		$this->primaryKey($result['Column_name']);
	}

	public function setColumns()
	{
		global $DB_VENDOR;

		//get table description i.e. metadata
		$statement = $this->adapter()->prepare("DESCRIBE " . $this->table_name);

		//execute statement
		$statement->execute();

		//get table meta information
		$this->table_meta = $statement->fetchAll();

		switch ($DB_VENDOR) {
			case 'mysql':
				$column = 'Field';
				break;
			case 'oracle':
				$column = 'Name';
				break;
			case 'mssql':
				$column = 'Field';
				break;
			case 'postgresql':
				$column = 'Field';
				break;
			default:
				$column = 'Field';
				break;
		}

		//fetch table columns
		$this->columns = $this->getArrayMap($this->table_meta, $column);
	}

	public function getColumns()
	{
		return $this->columns;
	}

	public function getTableMeta()
	{
		return $this->table_meta;
	}

	public function primaryKey($prkey = null)
	{
		$this->prkey = $prkey ? $prkey : $this->prkey;
		return $this->prkey;
	}
	//the company id sent on the http headers
	public function getSentClientCompanyId()
	{
		return  $this->company_id;
	}
	//the company id sent on the http headers
	public function isSentClientCompanyIdSet()
	{
		return $this->company_id == null ? false : true;
	}
	public function uniqueColumn($column, $alias = null, $remove_alias = false)
	{
		$alias = ($alias) ? $alias : $column;
		return 'DISTINCT(' . $column . ') ' . ($remove_alias == true ? '' : ' AS ' . $alias);
	}

	public function sumColumn($column, $alias = null, $remove_alias = false)
	{
		$alias = ($alias) ? $alias : $column;
		return 'COALESCE(SUM(' . $column . '),0) ' . ($remove_alias == true ? '' : ' AS ' . $alias);
	}

	public function countColumn($column, $alias = null, $remove_alias = false)
	{
		$alias = ($alias) ? $alias : $column;
		return 'COALESCE(COUNT(' . $column . '),0) ' . ($remove_alias == true ? '' : ' AS ' . $alias);
	}

	public function averageColumn($column, $alias = null, $remove_alias = false)
	{
		$alias = ($alias) ? $alias : $column;
		return 'COALESCE(AVG(' . $column . '),0) ' . ($remove_alias == true ? '' : ' AS ' . $alias);
	}

	public function concat($columns, $separator = ' ')
	{
		$columns = is_array($columns) == true ? implode(',', $columns) : $columns;
		return " CONCAT_WS('$separator',$columns) ";
	}

	public function coalesce($column, $default = '', $alias = null)
	{
		$alias = ($alias) ? $alias : $column;
		return 'COALESCE(' . $column . ',\'' . $default . '\') AS ' . $alias;
	}

	public function table($table_name = null)
	{
		if ($table_name != null) {
			$this->table_name = $table_name;
			$this->join = null;
		}
		return $this->table_name;
	}

	protected function addDbAdapter(Adapter $db)
	{
		if (array_key_exists($db->getIndex(), self::$db_adapters) == false) {
			self::$db_adapters[$db->getIndex()] = $db;
		}
		$this->db = self::$db_adapters[$db->getIndex()];
	}

	/*Singleton Declaration */
	public function adapter(PDO $db = null)
	{
		$this->db = $db ? $db : $this->db;
		return $this->db;
	}

	public function mailer()
	{
		global $mailer;
		return $mailer;
	}

	public function sms()
	{
		global $sms;
		return $sms;
	}

	public function browser($agent = null)
	{
		$browser = new Browser($agent);
		return $browser;
	}

	public function notification()
	{
		global $notification;
		return $notification;
	}

	public function identity()
	{
		global $identity;
		return $identity;
	}

	public function globalVar()
	{
		global $global;
		return $global;
	}

	public function smtpConfig()
	{
		global $smtp_config;
		return $smtp_config;
	}

	public function accessToken()
	{
		global $access_token;
		return $access_token;
	}

	public function time()
	{
		return new Time();
	}

	protected function prepareModel()
	{
		global $identity;
		global $mailer;
		global $notification;

		global $DB_USER;
		global $DB_PASSWORD;
		global $DB_SOURCE;

		try {
			$this->addDbAdapter(new Adapter($DB_SOURCE, $DB_USER, $DB_PASSWORD));

			$this->mailer = $mailer;
			$this->notification = $notification;
			$this->time = new Time();
			$this->identity = $identity;

			$this->setKeys();
			$this->setColumns();
			$this->adapter()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (Exception $ex) {
			$title = 'Connection Failed';
			switch ($ex->getCode()) {
				case 2002:
					$message = 'Attempt to Connect to database failed';
					break;
				default:
					$message = $ex->getMessage();
					break;
			}
			$response = json_encode(['message' => $message, 'title' => $title, 'status' => 'error']);
			die($response);
		}
	}

	public function __construct()
	{
		//loop to check company_id header break if found 
		//is value is null check nullity
		foreach (getallheaders() as $name => $value) {

			if ($name == "company_id") {
				$this->company_id = $value;
				break;
			}
		}
		$this->prepareModel();
	}

	/**
	 * @method startTransaction()
	 */
	public function startTransaction()
	{
		if ($this->adapter()->inTransaction() !== true) {
			$this->adapter()->beginTransaction();
		}
	}

	public function endTransaction()
	{
		if ($this->adapter()->inTransaction() == true && $this->autocommit() == true) {
			$this->adapter()->commit();
		}
	}

	public function RollBack()
	{
		if ($this->adapter()->inTransaction() == true) {
			$this->adapter()->rollBack();
		}
		$this->recordsAffected = 0;
		$this->lastAffectedId = null;
		$this->is_error = true;
	}

	protected static function createColumnsParamsValues(array $data, $params_prefix = ":")
	{
		$columns = [];
		$values = [];
		$params = [];
		$columns_equals_params = [];
		foreach ($data as $key => $value) {
			/* iterator here to loop through criteria objects as we build an expression */
			$expBuilder = function ($value, $column) use (&$expBuilder, &$columns, &$values, &$params, &$columns_equals_params, $params_prefix) {

				/* the recursive section.. we obtain the expression by repeating several predicates*/
				if (($value instanceof NestedOr) || ($value instanceof NestedAnd)) {
					$expression = [];
					foreach ($value->getValue() as $k => $v) {
						$expression[] = is_object($v) ? $expBuilder($v, $column) : $expBuilder($v, $k);
						
					}
					$expression = '(' . implode(($value instanceof NestedOr ? ' OR ' : ' AND '), $expression) . ')';
					
				} elseif ($value instanceof PredicateFunction) {
					$expression = $value->getExpression();
				} else {
					/*if the argument supplied is an object we obtain values, expression from the object accessor methods */
					if (is_object($value)) {
						$column = str_replace('.', '', $value->getAlias());
						$alias = $column;
						$columns[] = $column;
						//array_push($columns,'company_id');
						
						$param = $params_prefix . $alias;
						$params[] = $param;
						$bound_val = $value->getValue();
						$bound_exp = $value->getExpression($params_prefix);

						$values[] = is_array($bound_val) == true ? implode(',', $bound_val) : $bound_val;
						$not = preg_match('/NOT/i', get_class($value));
						$formula = $not ? "ISNULL($column) OR FIND_IN_SET($column,$param)=0" : "FIND_IN_SET($column,$param)>0";
						$expression = is_array($bound_val) == true ? $formula : $bound_exp;
					} else {
						/*if not object, we constuct an expression i.e. =(single value) OR FIND_INSET(for many values) */
						$columns[] = $column;
						//array_push($columns,'company_id');
						$alias = str_replace('.', '', $column);
						$param = $params_prefix . $alias;
						$params[] = $param;

						$values[] = is_array($value) ? implode(',', $value) : ((trim($value) == "") ? null : $value);
						$expression = is_array($value) == true ? "FIND_IN_SET($column,$param)>0" :  $column . '=' . $param;
					}
				}
				return $expression;
			};

			$columns_equals_params[] = $expBuilder($value, $key);

		}
		
		return ['columns' => $columns, 'params' => $params, 'values' => $values, 'columns_equals_params' => $columns_equals_params];
		
	}

	public function createStatement($sql, array $params, array $values)
	{
		$statement = $this->adapter()->prepare($sql);

		$this->bindStatementParams($statement, $params, $values);

		return $statement;

		
	}

	private function bindStatementParams($statement, $params, $values)
	{
		for ($i = 0; $i < count($params); $i++) {
			$statement->bindParam($params[$i], $values[$i]);
		}
	}

	protected function executeStatement(PDOStatement $statement)
	{
		try {
			$this->success = (int) $statement->execute();
			$this->recordsAffected = (int) $statement->rowCount();
			$this->lastAffectedId = $this->adapter()->lastInsertId();
			$this->is_error = false;
			$this->message = 'success';
		} catch (\Exception $ex) {
			$this->recordsAffected = 0;
			$this->lastAffectedId = null;
			$this->is_error = true;
			$this->message = $statement->errorInfo()[2];
		}
		return $this->recordsAffected;
	}

	public function insert(array $data)
	{

		foreach ($data as $key => $val) {
			if ($key == 'company_id') {
				$data['company_id'] = $this->company_id;
			}
		}
		$cpv = self::createColumnsParamsValues($data);

		$sql = "INSERT INTO `$this->table_name` (" . implode(",", $cpv['columns']) . ") 
					VALUES(" . implode(',', $cpv['params']) . ") ";
					//die(var_dump($sql));

					
		$statement = $this->createStatement($sql, $cpv['params'], $cpv['values']);

		return $this->executeStatement($statement);
	}

	public function delete($criteria)
	{
		$cpv = self::createColumnsParamsValues($criteria, $params_prefix = ":", $this->company_id);
		

		array_push($cpv['columns_equals_params'],'company_id=:company_id');

		$sql = "DELETE FROM `$this->table_name` WHERE " .
			implode(" AND ", $cpv['columns_equals_params']);

			array_push($cpv['params'],':company_id');
			array_push($cpv['values'],$this->company_id);

		
			
		
		$statement = $this->createStatement($sql, $cpv['params'], $cpv['values']);
		
		
		return $this->executeStatement($statement);
	}

	public function deleteOne($id)
	{
		return $this->delete([$this->prkey => $id]);
	}

	public function deleteAll()
	{
		$this->delete($this->id);
	}

	public function update($data, $criteria = null)
	{
		$criteria = ($criteria) ? $criteria : [$this->prkey => $data[$this->prkey]];
		
		
	
		
		foreach ($data as $key => $val) {
			if ($key == 'company_id') {
				$data['company_id'] = $this->company_id;
			}
		}
		

		//assemble parameters,columns and values 
		$cpv = self::createColumnsParamsValues($data, ':c');
		

		$upc = self::createColumnsParamsValues($criteria, ':p');
		

		//assemble criteria parameters in where clause
		$where = " WHERE " . ((count($upc['columns']) > 0) ? implode(" AND ", $upc['columns_equals_params']) : "1");
		
		//assemble update statement
		$sql = "UPDATE `$this->table_name` SET " . implode(",", $cpv['columns_equals_params']) . " " . $where;

		
	
		//bind data columns
		$statement = $this->createStatement($sql, $cpv['params'], $cpv['values']);
		
	
		//bind criteria columns
		$this->bindStatementParams($statement, $upc['params'], $upc['values']);
		

		$response = $this->executeStatement($statement);


		$this->lastAffectedId = isset($data[$this->prkey]) ?  $data[$this->prkey] : (isset($criteria[$this->prkey]) ? $criteria[$this->prkey] : $this->lastAffectedId);

		return $response;
	}

	public function selectOne($id, $columns = null)
	{
		$result = $this->select([$this->prkey => $id], $columns);
		//die(var_dump($result));
		
		return ($result && count($result) > 0) ? $result[0] : null;
	}

	public function filterOne($criteria, $columns = null, $group_by = null, $order_by = null, $limit = null)
	{
		$result = $this->select($criteria, $columns, $group_by, $order_by, $limit);
		
	
		
		return ($result && count($result) > 0) ? $result[0] : null;
	}

	public function exists($criteria)
	
	{  	
	
		//array_push($criteria,["company_id"=>$this->company_id]);
		//die(var_dump($criteria ));
		$criteria = (is_array($criteria)) ? $criteria : [$this->prkey => $criteria];
		
		$result = $this->select($criteria, [$this->countColumn($this->prkey, $this->prkey)]);
		
	
	
		return ($result && count($result) > 0 && (int) $result[0][$this->prkey] > 0) ? true : false;
	}

	public function count($criteria)
	{
		$criteria = (is_numeric($criteria)) ? [$this->prkey => $criteria] : $criteria;
		$result = $this->select($criteria, [$this->countColumn($this->prkey, $this->prkey)]);
		
		return ($result && count($result) > 0 && (int) $result[0][$this->prkey] > 0) ? (int) $result[0][$this->prkey] : 0;
		
	
	}

	/**
	 * @method fetchColumn()
	 * @desc find values for a given column based on supplied criteria
	 * @param string $col column to select
	 * @param array $criteria filter criteria columns and values
	 * @return array|null $values or null 
	 */
	public function fetchColumn($col, $criteria = [], $as_array = false, $alias = null, $unique_only = null)
	{
	
		$criteria = (is_numeric($criteria)) ? [$this->prkey => $criteria] : $criteria;
			$result = $this->select($criteria, [$col]);
		$col = ($alias) ? $alias : $col;
		
		$values = null;
		if ($result && count($result) > 0) {
			$values = $result[0][$col];
			if (count($result) > 1) {
				$values = [];
				foreach ($result as $row) {
					$values[] = $row[$col];
				}
			}
		}
		$values = ($as_array == true) ? ($values ? (is_array($values) == true ? $values : [$values]) : []) : $values;
		$values = $unique_only == true ? array_unique($values) : $values;
			

		return $values;
	}

	public function save($data)
	{
		if (isset($data[$this->prkey]) && ((int) $data[$this->prkey] > 0))
			return $this->update($data);
		else
			return $this->insert($data);
	}

	public function select($criteria =null, $columns = null, $group_by = null, $order_by = null, $limit = null)
	{
		$columns = (is_array($columns) && count($columns) > 0) ?  implode(',', $columns) : '*';
		

		$group_by = (is_array($group_by) && count($group_by) > 0) ?  implode(',', $group_by) : $group_by;
		

		$group_by = (strlen(trim($group_by)) == 0) ? null : " GROUP BY " . $group_by;
		

		$order_by = (is_array($order_by) && count($order_by) > 0) ?  implode(',', $order_by) : $order_by;

		$order_by = (strlen(trim($order_by)) == 0) ? null : " ORDER BY " . $order_by;
		
		if (is_array($criteria) && count($criteria) > 0) {
			$cpv = self::createColumnsParamsValues($criteria);
				
			$params = $cpv['params'];
			$values = $cpv['values'];

				$criteria = implode(' AND ', $cpv['columns_equals_params']);
				
			
			
		} else {
			$criteria = "1";
			
			$params = [];
			$values = [];
		}

		$sql = "SELECT $columns FROM $this->table_name $this->join WHERE $criteria $group_by $order_by $limit ";
		
	
		$statement = $this->createStatement($sql, $params, $values);
		
		
		$statement->execute();

		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		
		$this->recordsSelected = $statement->rowCount();
		
		return ($this->recordsSelected > 0) ? $result : null;
	}

	public function join($table, $on = null, $type = null)
	{
		$default_on = " $this->table_name.$this->prkey=$table.$this->prkey ";
		$on = " ON " . (($on == null) ? $default_on : $on);
		$type = strtoupper($type . ' JOIN ');
		$this->join = $this->join . " $type $table $on ";
		

	}

	//other functions
	public function getArrayCopy()
	{
		return get_object_vars($this);
	}

	public function getClassVars($class = null)
	{
		return get_class_vars($class);
	}

	public function arrayKeys()
	{
		return array_keys(get_class_vars(get_class($this)));
	}

	public function listKeys()
	{
		return implode(',', $this->arrayKeys());
	}

	public function sanitize($data, $use_defaults = true)
	{
		$default = $this->getArrayMap($this->getTableMeta(), 'Default', false);
		$result = [];
		for ($i = 0; $i < count($this->columns); $i++) {
			$default_value = ($use_defaults == true) ? $default[$i] : null;
			$key = $this->columns[$i];
			$value = isset($data[$key]) && !is_array($data[$key]) ? trim($data[$key]) : $default_value;
			$result[$key] = trim($value);
		}
		
		return $result;
	}

	public function getClassName()
	{
		return get_class($this);
	}

	public static function getInitials($name, $separator = false)
	{
		//split name using spaces
		$words = array_filter(explode(" ", $name));
		$inits = '';
		//loop through array extracting initial letters
		foreach ($words as $word) {
			$inits .= strtoupper(substr($word, 0, 1)) . (($separator == false) ? '' : $separator);
		}
		return trim($inits);
	}

	public function getFromArray($data, $key, $as_number = false, $default = null)
	{
		$value =  isset($data[$key]) ? $data[$key] : null;
		return ($as_number == true && intval($value) == 0) ? $default : $value;
	}

	public function getArrayMap($data, $key, $unique = true)
	{
		$results = [];
		$data = is_array($data) == true ? $data : [];
		foreach ($data as $row) {
			if (array_key_exists($key, $row) == true) {
				$results[] = $row[$key];
			}
		}
		$results = ($unique == true) ? array_unique($results) : $results;
		return $results;
	}

	public function objectifyArray($data, $key)
	{
		$results = [];
		$data = is_array($data) == true ? $data : [];
		foreach ($data as $row) {
			$results[$row[$key]] = $row;
		}
		return $results;
	}

	public function getArrayAssoc($data, $key_col, $val_col, $unique = true)
	{
		$results = [];
		$data = is_array($data) == true ? $data : [];
		foreach ($data as $row) {
			if (array_key_exists($key_col, $row) == true) {
				$results[$row[$key_col]] = $row[$val_col];
			}
		}
		$results = ($unique == true) ? array_unique($results) : $results;
		$results = count($results) > 0 ? $results : null;
		return $results;
	}

	public function arraySubset($source, $keys, $twoD = false)
	{
		$source = (array) $source;
		$keys = (array) $keys;
		$result = [];

		if ($twoD == true) {
			foreach ($source as $row) {
				$fields = [];
				foreach ($keys as $key) {
					if (array_key_exists($key, $row) == true) {
						$fields[$key] = $row[$key];
					}
				}
				$result[] = $fields;
			}
		} else {
			foreach ($keys as $key) {
				if (array_key_exists($key, $source) == true) {
					$result[$key] = $source[$key];
				}
			}
		}
		return $result;
	}

	public function getFiltered($source, $key, $val)
	{
		$source = (array) $source;
		$result = [];

		foreach ($source as $row) {
			if ($row[$key] == $val) {
				$result[] = $row;
			}
		}

		return $result;
	}

	public function sumValueIn2DArray($source, $key)
	{
		$source = (array) $source;
		$sum = 0;

		foreach ($source as $row) {
			if (isset($row[$key])) {
				$sum = $sum + doubleval($row[$key]);
			}
		}

		return $sum;
	}

	public function getResetPasswordCode($email)
	{
		return substr(md5($email . SITE_NAME . HASH_A), 0, 10);
	}

	public function fromRGBToHexa($R, $G, $B)
	{
		$R = dechex($R);
		if (strlen($R) < 2)
			$R = '0' . $R;

		$G = dechex($G);
		if (strlen($G) < 2)
			$G = '0' . $G;

		$B = dechex($B);
		if (strlen($B) < 2)
			$B = '0' . $B;

		return '#' . $R . $G . $B;
	}

	public function hex2rgba($hex, $as_array = true)
	{
		$hex = str_replace("#", "", $hex);
		$r = 0;
		$g = 0;
		$b = 0;
		$a = 0;

		switch (strlen($hex)) {
			case 3:
				$r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
				$g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
				$b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
				$a = 1;
				break;
			case 6:
				$r = hexdec(substr($hex, 0, 2));
				$g = hexdec(substr($hex, 2, 2));
				$b = hexdec(substr($hex, 4, 2));
				$a = 1;
				break;
			case 8:
				$a = hexdec(substr($hex, 0, 2)) / 255;
				$r = hexdec(substr($hex, 2, 2));
				$g = hexdec(substr($hex, 4, 2));
				$b = hexdec(substr($hex, 6, 2));
				break;
		}
		$rgba = array($r, $g, $b, $a);

		return ($as_array == true) ? $rgba : 'rgba(' . implode(', ', $rgba) . ')';
	}

	public function rgba2hex($string)
	{
		$rgba  = array();
		$hex   = '';
		$regex = '#\((([^()]+|(?R))*)\)#';
		if (preg_match_all($regex, $string, $matches)) {
			$rgba = explode(',', implode(' ', $matches[1]));
		} else {
			$rgba = explode(',', $string);
		}

		$rr = dechex($rgba['0']);
		$gg = dechex($rgba['1']);
		$bb = dechex($rgba['2']);
		$aa = '';

		if (array_key_exists('3', $rgba)) {
			$aa = dechex($rgba['3'] * 255);
		}

		return strtoupper("#$aa$rr$gg$bb");
	}

	public function getPasswordLink($email, $reset_password)
	{
		$user = User::getInstance();

		$payload = ['email' => $email, 'code' => $reset_password];

		$token = JWT::encode($payload, $user->getTokenKey(), 'HS256');

		$link = SITE_URL . '/#/auth/change-password?reset_token=' . $token;

		return $link;
	}

	public function getEmailNewUserAccount($email, $name, $reset_code, $message = "A user account has been created for this email at")
	{
		$link = $this->getPasswordLink($email, $reset_code);

		$link = '<a href="' . $link . '">' . $link . '</a>';

		$body = "Hello " . $name . ",<br/><br/>" . $message . " " . SITE_NAME . "<br/>" . "
				<a href='" . APP_URL . "'><img height='50' src='" . APP_LOGO . "' /></a><br/>
				Please use the following credentials if prompted on mobile: <br/>
				Username: " . $email . " <br/>Verification Code: " . $reset_code . " <br/><br/>
				Or <br/>
				Please click the link below to complete the registration process on web: <br/>" . $link . "<br/><br/>" .
			"Regards,<br/>" . "Administrator,<br/>" . SITE_NAME;

		return $body;
	}

	/**
	 * @desc join 2 arrays of 2-dimension where a key in each row matches
	 * @param array $arrayA 
	 * @param array $arrayB
	 * @param string $key 
	 * @return array combined result  
	 */
	public function join2DArrays($arrayA, $arrayB, $key)
	{
		$results = [];
		foreach ($arrayA as $a) {
			foreach ($arrayB as $b) {
				if ($a[$key] == $b[$key]) {
					$results[] = array_merge($a, $b);
				}
			}
		}
		return $results;
	}

	/**
	 * @desc Rename keys with another set of keys in a 2-D array
	 * @param array $keys
	 * @param array $array
	 * @param boolean $return_unmatches
	 * @return multitype:multitype:NULL
	 */
	public function renameKeys2D($keys, $array, $return_unmatches = false)
	{
		$rows = [];
		for ($i = 0; $i < count($array); $i++) {
			$values = [];
			foreach ($keys as $o_key => $n_key) {
				$values[$n_key] =  $array[$i][$o_key];
			}
			$rows[] = $values;
		}
		return $rows;
	}

	/**
	 * @method randomize()
	 * @desc Create a random string from alphanumeric characters
	 * @param string $length number of characters in the result defaults to 8
	 * @return string the random string
	 */
	public function randomize($length = 8, $num_only = false, $use_symbols = false)
	{
		if ($num_only == true)
			$chars = "0123456789";
		else
			$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

		if ($use_symbols == true)
			$chars = $chars . "!@#$%^&*()_-=+;:,.?";

		$result = substr(str_shuffle($chars), 0, $length);
		return $result;
	}

	/**
	 * @method randomColor()
	 * @desc create random and return it as hexa decimal or rgb
	 * @return string
	 */
	public function randomColor()
	{
		return '#' . dechex(rand(0x000000, 0xFFFFFF));
	}
}
