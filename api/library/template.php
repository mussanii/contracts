<?php
class Template {
     
    protected $variables = array();
    protected $_controller;
    protected $_action;
     
    function __construct($controller,$action) {
        $this->_controller = $controller;
        $this->_action = $action;
    }
 
    /** Set Variables **/
 
    function set($name,$value) {
        $this->variables[$name] = $value;
    }
 
    
    
    /** Display Template **/
     
    function render($enable_layout=true,$render=true) {
    	if($render==true)
    	{
	    	global $global;
	    	
			$controller = $global->removeCapsPutHyphen($this->_controller);
			$action = $global->removeCapsPutHyphen($global->getActionName($this->_action));
			
	    	extract($this->variables);
	
	    	if ($enable_layout == true) {
	    	    if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $controller . DS . 'layout/header.php')) {
	                include (ROOT . DS . 'application' . DS . 'views' . DS . $controller . DS . 'layout/header.php');
	            } else {
	                include (ROOT . DS . 'application' . DS . 'views' . DS . 'layout/header.php');
	            }
	    	}
	        
	    	include (ROOT . DS . 'application' . DS . 'views' . DS . $controller . DS . $action . '.php');
	               
	    	if ($enable_layout == true) {
	            if (file_exists(ROOT . DS . 'application' . DS . 'views' . DS . $controller . DS . 'layout/footer.php')) {
	                include (ROOT . DS . 'application' . DS . 'views' . DS . $controller . DS . 'layout/footer.php');
	            } else {
	                include (ROOT . DS . 'application' . DS . 'views' . DS . 'layout/footer.php');
	            }
	        }    
    	}
    	else
    	{
    		exit();
    	}
    } 
}
