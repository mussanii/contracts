<?php 
/** Check if environment is development and display errors **/
/** dont touch unless you know what you are doing! */
 
function setReporting() {
	if (DEVELOPMENT_ENVIRONMENT == true) {
	    error_reporting(E_ALL);
	    ini_set('display_errors','On');
	} else {
	    error_reporting(E_ALL);
	    ini_set('display_errors','Off');
	    ini_set('log_errors', 'On');
	    ini_set('error_log', ROOT.DS.'tmp'.DS.'logs'.DS.'error.log');
	}
}
 
/** Check for Magic Quotes and remove them **/
 
function stripSlashesDeep($value) {
    $value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
    return $value;
}
 
function removeMagicQuotes() {
if ( get_magic_quotes_gpc() ) {
    $_GET    = stripSlashesDeep($_GET   );
    $_POST   = stripSlashesDeep($_POST  );
    $_COOKIE = stripSlashesDeep($_COOKIE);
}
}
 
/** Check register globals and remove them **/
 
function unregisterGlobals() {
    if (ini_get('register_globals')) {
        $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $var) {
                if ($var === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}

/** Main Call Function **/
 

function callHook() {
    global $url;
    global $global;
    global $audit_start;
 
    $url_components = explode("?",$url); 
    $query_array=[];    
    
    if(count($url_components)>1){
    	parse_str($url_components[1],$query_array);    	
    }    
    
    $urlArray = array();
    $urlArray = array_filter(explode("/",$url_components[0]));
    $count = count($urlArray);
    
    //check if modules requested for
    if(($count==3 && is_numeric($urlArray[2])==false) || ($count==4 && is_numeric($urlArray[3])==true)){ 
    	$module = $global->removeHyphenPutCaps($urlArray[0]);
    	$moduleName = $global->removeHyphenPutCaps($urlArray[0]);    	
    	define('__MODULE__', $moduleName);
    	array_shift($urlArray);
    }
     
    $controller = $urlArray[0];
    array_shift($urlArray);
    $action = lcfirst($global->removeHyphenPutCaps($global->createActionName($urlArray[0])));
    array_shift($urlArray);
    $id = array_shift($urlArray);
 
    $controllerName = $global->removeHyphenPutCaps($controller);
    $controller = $global->removeHyphenPutCaps($controller);
        
    $model = rtrim($controller, 's');    
    $controller .= 'Controller';
    
    $url_array=['model'=>$model,'controller'=>$controllerName,'action'=>$action,'id'=>$id];
    
    $dispatch = new $controller($url_array,$query_array);
    
    if ((int)method_exists($controller, $action)) {
    	call_user_func_array(array($dispatch,$action),[]);          

        //perform audit here if enabled *dont touch unless you know what you are doing!*
        if(AUDIT_ENABLED==true)
        {
        	AccessLog::getInstance()->log(['audit_start'=>$audit_start]);
        }              
    } 
    else 
    {    	
        /* Error Generation Code Here */
    }
}

setReporting();
removeMagicQuotes();
unregisterGlobals();
callHook();
