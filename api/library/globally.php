<?php 
class Globally
{
	private $expired_session_flags = ['message' => 'Session has expired. Please login.','title' => 'Session Expired','status'=>'error','success'=>0,'session'=>'N'];
	
	function getExpiredSessionFlags()
	{
		return $this->expired_session_flags;
	}
	
	//creating module/controller/action names
	function removeHyphenPutCaps($string)
	{
		$array = explode("-", $string);
		for($i=0; $i<count($array); $i++)
		{
			$array[$i] = ucwords($array[$i]);
		}
		return implode("",$array);
	}
	
	function createActionName($action){
		return $action."Action";
	}
	
	function removeCapsPutHyphen($string)
	{
		$array=preg_split('#([A-Z][^A-Z]*)#', $string,
				null, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
		 
		for($i=0; $i<count($array); $i++)
		{
			$word=ucwords($array[$i]);
			$word=($i>0)? "-".$word : $word;
			$array[$i] = $word;
		}
		return strtolower(implode("",$array));
	}
	
	function getActionName($action){
		return preg_replace('/Action$/', '', $action);
	}
	
	function redirect($module_name='application',$controller,$action,$id=null,$queryString)
	{
				 		 
		$controller = $this->removeCapsPutHyphen($controller);
		
		$action = $this->removeCapsPutHyphen($this->getActionName($action));
		 
		$location = BASE_URL . DS . $controller . DS . $action;
	
		$id=(($id)? DS.$id : ''); 
		 
		$queryString= (count($queryString)>0)? '?'.implode('&', $queryString) : ''; 
		
		$location = $location.$id.$queryString;
	
		header("Location: " . $location);
				 
		exit;
	}
	
	public function getPost($key=null)
	{
		$_POST= (isset($_POST) && count($_POST)>0) ? $_POST : json_decode(file_get_contents("php://input"),true);
		 
		if($key)
			return isset($_POST[$key])? $_POST[$key] : null;
		else
			return isset($_POST)? $_POST : null;
	}
			
	public function getServerVars($key=null)
	{
		$array = (isset($_SERVER) && count($_SERVER)>0) ? $_SERVER : [];
			
		if($key)
			return isset($array[$key])? $array[$key] : null;
		else
			return isset($array)? $array : null;
	}
	
	public function getTokenPayLoad($access_token)
	{
		$payload = null;
		try
		{
			$payload = json_encode(JWT::decode($access_token, TOKEN_KEY,['HS256']));
			$payload = json_decode($payload,true);	
		}
		catch (\Exception $ex)
		{
			
		}	
		return $payload;
	}
	

	public function queryToArray($str)
	{
		$rows = explode('&', $str);
		$results = [];
		foreach ($rows as $row)
		{
			$single = explode('=', $row);
			$key = $single[0];
			$value = isset($single[1])? $single[1] : null;
			$results[$key] = $value;
		}
		return $results;
	}
	
	public function arrayToQuery($array)
	{
		$result = [];
		foreach ($array as $key=>$value)
		{
			$result[] = $key.'='.$value;
		}
		return implode('&', $result);
	}
	
	public function sendResponse($content,$json=false)
	{
		if (!headers_sent() && $content) {
						
		}
		$content = ($json==true)? json_encode($content,JSON_NUMERIC_CHECK) : $content;
		echo $content;
		exit(0);
	}
	
	/**
	 * @method sendCurlRequest()
	 * @param $end_point uri/url where request will be sent to
	 * @param $body request body to be sent along could be a json string
	 * @desc silently run a remote request given uri/url
	 * @return mixed $output
	 */
	public function sendCurlRequest($end_point,$body,$headers=null,$as_json=true)
	{
		$output = null;
		try
		{
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,$end_point);
			curl_setopt($ch, CURLOPT_HEADER, 0);
				
			curl_setopt($ch, CURLOPT_VERBOSE, '0');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
				
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, '0');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0');
			
			if($headers) curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
				
			$output = curl_exec($ch); 
			curl_close($ch);
				
			if(is_object($ch) && curl_errno($ch))
			{
				$this->message('Request failed. Please try again.');
				$this->isError(true);
			}
		}
		catch(Exception $ex)
		{
			$this->message($ex->getMessage());
			$this->isError(true);
		}
		return ($as_json==true)? json_decode($output,true) : $output;		
	}
	
	public function addUriQueryParam($url,$param_name,$param_value)
	{
	    $url_components = explode("?",$url);	    
	    $query_array=[];
	    if(count($url_components)>1){
	        parse_str($url_components[1],$query_array);
	    }
	    $query_array[$param_name] = $param_value;
	    return $url_components[0].'?'.http_build_query($query_array);
	}	
	
	public function switchDbVendor()
	{
	    global $DB_HOST;
	    global $DB_PORT;
	    global $DB_NAME;
	    global $DB_SOURCE;
	    global $DB_VENDOR;
	    
	    
	    switch ($DB_VENDOR){
	        case 'mysql': $DB_SOURCE = 'mysql:host='.$DB_HOST.';port='.$DB_PORT.';dbname='.$DB_NAME; break;
	        case 'oracle': $DB_SOURCE = 'oci:dbname='." (DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = $DB_HOST
	           )(PORT = $DB_PORT))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = $DB_NAME)))"; break;
	        case 'mssql': $DB_SOURCE = 'odbc:Driver={SQL Server};Server='.$DB_HOST.';Database='.$DB_NAME.';'; break;
	        case 'postgresql': $DB_SOURCE = 'pgsql:host='.$DB_HOST.';port='.$DB_PORT.';dbname='.$DB_NAME; break;
	        default: $DB_SOURCE = 'mysql:host='.$DB_HOST.';port='.$DB_PORT.';dbname='.$DB_NAME; break;
	    }	
	    
	    return $DB_SOURCE;
	}
}