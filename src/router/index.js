import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import ForgotPassword from '../views/ForgotPassword.vue'

Vue.use(VueRouter)
// const originalPush = VueRouter.prototype.push;
// VueRouter.prototype.push = function push(location) {
//   return originalPush.call(this, location).catch(err => {
//     if (err.name !== 'NavigationDuplicated') throw err
//   });
// }

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/forgot-password',
    name: 'ForgotPassword',
    component: ForgotPassword
  },
  {
    path: '/',
    component:()=> import('../views/Layout/Main.vue'),
    children: [
      {
        path:'/dashboard',
        component:()=> import('../views/Dashboard.vue'),
   
      },
      {
        path: '/customers/list',
        component:()=> import('../views/Customers.vue') ,
      },
      {
        path: '/contracts/templates',
        component:()=> import('../views/Templates.vue') ,
      },
      {
        path: 'contracts/drafting',
        component:()=> import('../views/Contracts.draft.vue') ,
      },
      {
        path: 'contracts/negotiation',
        component:()=> import('../views/Contracts.negotiate.vue') ,
        
      },
      {
        path: 'contracts/approval',
        component:()=> import('../views/Contracts.approve.vue') ,
      },
      {
        path: 'contracts/publication',
        component:()=> import('../views/Contracts.publish.vue') ,
      },
      {
        path: 'contracts/execution',
        component:()=> import('../views/Contracts.execute.vue') ,
      },
      {
        path: 'contracts/expiry',
        component:()=> import('../views/Contracts.expired.vue') ,
      },
      {
        path: 'messaging/mails-list',
        component:()=> import('../views/Emails.vue') ,
      },
      {
        path: 'messaging/sms-list',
        component:()=> import('../views/Sms.vue') ,
      },
      {
        path: 'messaging/notifications-list',
        component:()=> import('../views/Notifications.vue') ,
      },
      {
        path: 'process/list',
        component:()=> import('../views/Contracts.expired.vue') ,
      },
      {
        path: '/cases/logs',
        component:()=> import('../views/CaseLogs.vue') ,
      },
    ]
  }
]

const router = new VueRouter({
 //mode: 'history',
  base: process.env.BASE_URL,
  routes
})
/**control auth in all routes */
router.beforeEach((to,from,next)=>{
  const publicPages = ['/login','/register'];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem('token');
 
  

  /**
   * trying to access a restricted page + not authenticated
   * redirects to login page
   */
  if(authRequired && !loggedIn){
    next('/login');
  }else{
    next();
  }
});

export default router
