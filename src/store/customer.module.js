import CustomerService from '../services/customer.service'
//import AuthService from '../services/auth-service'

const initialState = {
    selected: {},
    allCustomers: {}

};

export const customer = {
    namespaced: true,
    state: initialState,
    actions: {
        list({ commit }){
            return CustomerService.listAll().then(
                response => {
                    console.log()
                    commit('loginSuccess', response.data)
                    return Promise.resolve(response.data)
                },
                error => {
                    commit('loginFailure', error)
                    return Promise.reject(error)
                }

            )
        },
        setSelected({ commit }, item){
            commit('setSelected', item)
        },
    },
    mutations:{
         // handler: () => { },
         loginSuccess(state, response) {
            state.status.loggedIn = true
            state.snackbar.show = true
            state.snackbar.color = 'success'
            state.snackbar.message = 'User login successful!'
            localStorage.setItem('user', JSON.stringify(response.data.user))
            localStorage.setItem('token', response.data.access_token)
            state.user = response.data.user
        },
        loginFailure(state,error) {
            console.log(error)
            state.status.loggedIn = false
            state.user = null
            state.snackbar.show = true
            state.snackbar.color = 'error'
            state.snackbar.message = error.response.data.message ||
              (error.response && error.response.data) ||
              error.mesage || 
              error.toString();
        },
        setSelected(state, item) {
            state.selected = item
        },

    },
    getters: {
        user: (state) => state.user
    }
}