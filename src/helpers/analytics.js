import D3Funnel from 'd3-funnel'
import * as CanvasJS from 'canvasjs'
import Vue from 'vue';

var ChartsEvents = {
	Click: function(){
		return {
			elementClick: function(e) {
				if(e.data.href){
					window.location.href = e.data.href;
				}
			}   
		};
	},
};

var StatisticFunctions = function(){	
	return {
		formatLargeNumber: function(d,decimals){
			var decimals = (typeof decimals !=='undefined')? decimals : 2;
			var array = ['','K','M','G','T','P'];
		    var i=0;
		    while (d > 1000)
		    {
		        i++;
		        d = d/1000;
		    }
		    var p = (decimals==0)?  '.' : '.0'; 
		    d = d3.format(',' + p + decimals.toString() + 'f')(d) + ' ' + array[i];
		    return d;
		}
	}	
}();

var Analytics = function($scope,Data){
	var DiscreteVerticalBarChartOptions = function(height,xAxis,yAxis){
		return {
		    chart: {
		        type: 'discreteBarChart',
		        height: height,
		        margin : {
		            top: 10,
		            right: 0,
		            bottom: 20,
		            left: 10
		        },
		        fill:false,
		        showControls: false, 
				showValues: true, 
				showLegend: false, 
				showXAxis: true, 
				showYAxis: false, 
		        x: function(d){ return d.label; },
		        y: function(d){ return d.value; },
		        showValues: true,
		        valueFormat: function(d){
		            return StatisticFunctions.formatLargeNumber(d);
		        },
		        transitionDuration: 200,
		        xAxis: xAxis,
		        yAxis: yAxis
		    }
		};
		
	};
	
	var PieChartOptions = function(height,donut,semi,chart_options,decimals){
		var decimals = (typeof decimals !=='undefined')? decimals : 0;
		var chart_options = (typeof chart_options !=='undefined')? chart_options : {};
		
		var semi_config = {
			startAngle: function(d) { return d.startAngle/2 -Math.PI/2 },			
			endAngle: function(d) { return d.endAngle/2 -Math.PI/2},
		};
		 
		var pie  =  (semi==true)? semi_config: {};
					
		Vue.extend(pie,{dispatch:ChartsEvents.Click()});		
		
		var options = {
			  chart: {
			    type: 'pieChart',
			    height: height,
			    donut : donut,
			    margin : {
		            top: 10,
		            right: 0,
		            bottom: 0,
		            left: 0
		        },		        
		        pie : pie,
		        transitionDuration: 200,
			    x: function(d) { return d.key; },
			    y: function(d) { return d.y; },			    
			    valueFormat: function(d){
			    	return StatisticFunctions.formatLargeNumber(d,decimals);
		        }
			  }
		};
		
		Vue.extend(options.chart,chart_options)
		return options;
	};
	
	var TrendGraph = function(height,xAxis,yAxis,decimals, timeFormat,chartType){		
		return {
            chart: {
                type: (chartType=='L'? 'lineChart' : 'multiBarChart'),
                height: height,
                margin : {
                    top: 20,
                    right: 30,
                    bottom: 60,
                    left: 60
                },         
                color: d3.scale.category10().range(),
                showLegend: true,
                showControls: false,
                useVoronoi: true,
                clipEdge: false,
                transitionDuration: 500,
                useInteractiveGuideline: true,
                yAxis: {
                    tickFormat: function(d) {
                    	return StatisticFunctions.formatLargeNumber(d,decimals);
                    }
                },
                xAxis: {
                    axisLabelDistance: 50,
                    showMaxMin: true,
                    staggerLabels: false,
                    tickFormat: function(d) {
                    	return d3.time.format(timeFormat)(new Date(d));
                    }
                }
            }
        };
	}
	
	 FunnelChartOptions2 = function(height,extra_options,data,selector,reversed,showLegend,showLabel){
		var chart = new CanvasJS.Chart(selector, {
			animationEnabled: true,
			exportEnabled: false,
			data: [{
				type: "funnel",
				reversed: reversed,
				showInLegend: showLegend,
				animationEnabled: true,
				animationDuration: 1000,
				legendText: "{label}",
				indexLabel: (showLabel? "{label} - {y}" : "{label} - {y}"),
				toolTipContent: "<b>{label}</b>: {y} <b>({percentage}%)</b>",
				indexLabelFontColor: "black",
				dataPoints: data
			}]
		});
		CalculatePercentage(chart);
		chart.render();
	};
	
	var FunnelChartOptions = function(height,extra_options,results,selector){
		
		/*organise data here */
		var data = [];
  		for(var i in results){
  			var row = results[i]; 
  			if(row.y && row.y>0){
  				data.push([row.label,row.y]);
  			}  				
  		}
  		/*organise data here */
  		
		const options = {
			   block: {
				   dynamicHeight: false,
				   minHeight: 0,
				   fill: {
					   type: 'gradient'
				   },
				   highlight: true,
			   },
			   tooltip: {enabled:true,format:'{v}'},
			   chart: {
				   bottomWidth: 1,
				   animate: 300,
				   curve: {
					   enabled: true,
					   height: 50,
				   },
				   bottomPinch: 1,
				   inverted: true,
				   height: height
			   },
			   events: {
				   click: {
					   block: function(data) { 
							if(data.href){
								window.location.href = data.href;
							}
						}   
				   }
			   }
			};
		
		if(options.chart)
			angular.extend(options.chart,extra_options.chart);	
		if(options.block)
			angular.extend(options.block,extra_options.block);	
		
		const chart = new D3Funnel(selector);
		chart.draw(data, options);
	}
}
