export default class User {
    constructor(username, password, company_code){
        this.username = username;
        this.password = password;
        this.company_code = company_code;
    }
}