//Data service 
import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost/contracts/api/';
let user = JSON.parse(localStorage.getItem('user'));

 
 

class CustomerService {
    getAllCaseLogs() {

 
        return axios.get(API_URL + 'case-logs/list?access_token='+user.data.token, { headers: authHeader() })
    }

    saveCaseLog(resource) {
        return axios.post(API_URL + 'save', resource, {headers: authHeader() })

    }
}

export default new CustomerService()