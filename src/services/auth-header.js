export default function authHeader(){
    let user = JSON.parse(localStorage.getItem('user'));
 
 


    if(user && user.data.token){
        
        return { 
            
            Accept: 'application/json',
            'Content-Type':'application/json',
            "Access-Control-Allow-Origin": "*",
            'company_id': 1
    };
    }else{
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'company_id': 1
        }
    }
}