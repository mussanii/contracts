//Data service 
import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost/contracts/api/';
let user = JSON.parse(localStorage.getItem('user'));
 
 

class ContractService {
    getAllContractDrafts() {
        return axios.get(API_URL + 'contracts/list?cycle_no=2&access_token='+user.data.token, { headers: authHeader() })
    }
    getAllContractNegotiations() {
        return axios.get(API_URL + 'contracts/list?cycle_no=3&access_token='+user.data.token, { headers: authHeader() })
    }
    getAllContractApprovals() {
        return axios.get(API_URL + 'contracts/list?cycle_no=4&access_token='+user.data.token, { headers: authHeader() })
    }
    getAllContractPublications() {
        return axios.get(API_URL + 'contracts/list?cycle_no=5&access_token='+user.data.token, { headers: authHeader() })
    }
    getAllContractExecutions() {
        return axios.get(API_URL + 'contracts/list?cycle_no=6&access_token='+user.data.token, { headers: authHeader() })
    }
    getAllContractExpired() {
        return axios.get(API_URL + 'contracts/list?cycle_no=7&access_token='+user.data.token, { headers: authHeader() })
    }

    saveContract(resource) {
        return axios.post(API_URL + 'save', resource, {headers: authHeader() })

    }
}

export default new ContractService()