//Data service 
import axios from 'axios';
import authHeader from './auth-header';




const API_URL = 'http://localhost/contracts/api/';

let access_token = JSON.parse(localStorage.getItem('token'));

  class AnalyticsService {
        getQuickStats() {
            var period_span = 'Y';
            return axios.post(API_URL + 'analytics/quick-stats?period_span='+period_span+'&access_token='+access_token, { headers: authHeader() })
        }

        getPieAnalytics(){
           // var chart_options = {showLegend:true,showLabels:false,labelType:'value'}
           //  Vue.pie_analytics_options = PieChartOptions(Vue.graph_height_c, true,false,chart_options);
            var analytics = ['area','department','status','type'];
            for(var i=0; i<analytics.length;  i++)
            {
                var key = analytics[i];
                var period_span = 'Y';
                return axios.post(API_URL + 'analytics/pie-stats?period_span=' +period_span+'&analytic='+key+'&access_token=' + access_token, { headers: authHeader() })
             }



         }
        // getContractRenewals() {
            
        //     var analytics = ['area','department','status','type'];
        //     for(var i=0;i<analytics.length;i++)
        //     {
        //         var key = analytics[i];
        //         var period_span = 'Y';

        //         return axios.post(API_URL + 'analytics/bar-stats?period_span=' +period_span+'&analytic='+key+'&code=RN'+'&access_token=' + user.data.token, { headers: authHeader() })
        //     }

        // }
        // getContractExpiries() {
        //     var analytics = ['area','department','status','type'];
        //     for(var i=0;i<analytics.length;i++)
        //     {
        //         var key = analytics[i];
        //         var period_span = 'Y';

        //         return axios.post(API_URL + 'analytics/bar-stats?period_span=' +period_span+'&analytic='+key+'&code=PE'+'&access_token=' + user.data.token, { headers: authHeader() })
        //     }

        // }


    }

export default new AnalyticsService();