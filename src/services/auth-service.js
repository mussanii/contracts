import axios from "axios";
const API_URL = 'http://localhost/contracts/api/users/';


class AuthService {
    login(user) {
        return axios
        .post(API_URL + 'login', {
            username: user.username,
            password: user.password,
            company_code: user.company_code
        })
        .then(response =>{
            if(response.data.token){
                localStorage.setItem('user', JSON.stringify(response.data));
                localStorage.setItem('token', JSON.stringify(response.data.token));
            }

            return response
        })
    }

    logout(){
        localStorage.removeItem('user');
        localStorage.removeItem('token');
    }

    // register(user){
    //     return axios.post(API_URL + 'register',{

    //     })
    // }
}
export default new AuthService();