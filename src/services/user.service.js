import axios from 'axios';
import authHeader  from './auth-header';

const API_URL = 'http://localhost/contracts/api/';

class UserService{
    getAllUsers(){
        return axios.get(API_URL +'users/list',{headers:authHeader()});
    }
    getUserRoles(){
        return axios.get(API_URL + 'roles/list',{headers:authHeader()});
    }
}
export default new UserService();