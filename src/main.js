import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'bootstrap'
import VueD3 from 'vue-d3'
import VueNVD3 from 'vue-nvd3'
import 'bootstrap/dist/css/bootstrap.min.css'
import VeeValidate from 'vee-validate'
import { library } from '@fortawesome/fontawesome-svg-core'
import VueGoogleCharts from 'vue-google-charts';
import vuetify from './plugins/vuetify'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'
import {
  faHome,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt
} from '@fortawesome/free-solid-svg-icons'
 library.add(faHome,faUser,faUserPlus, faSignInAlt,faSignOutAlt)
Vue.config.productionTip = false
Vue.use(VueGoogleCharts)
Vue.use(VeeValidate)
Vue.use(VueD3)
Vue.use(VueNVD3)

Vue.component('font-awesome-icon', FontAwesomeIcon)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
